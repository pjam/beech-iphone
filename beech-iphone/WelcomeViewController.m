//
//  WelcomeViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 09/02/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "WelcomeViewController.h"
#import "SlideItemViewController.h"

@interface WelcomeViewController ()
{
    __weak IBOutlet UIPageControl *pageControl;
    __weak IBOutlet UIScrollView *_scroller;
    __weak IBOutlet UIScrollView *_secondScroller;
    int numberOfViews;
    __weak IBOutlet UIButton *prevButton;
    __weak IBOutlet UIButton *nextButton;
    __weak IBOutlet UIButton *submitButton;
    __weak IBOutlet UIView *pageView;
}
@end

@implementation WelcomeViewController

@synthesize viewControllers, delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= [viewControllers count])
        return;
    if ([viewControllers count] == 0)
        return;

}

- (SlideItemViewController*)buildScrollViewWithTitle:(NSString*)title andContent:(NSString*)content forPage:(int)page
{
    
    SlideItemViewController *controller = [[SlideItemViewController alloc] initWithTitle:title andContent:content];
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil)
    {
        CGRect frame = _scroller.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;

        if (self.view.frame.size.height > 460)
        {
            controller.view.frame = CGRectMake(frame.size.width * page, 0, 320, 568);
            controller.bottomText.frame = CGRectMake(controller.bottomText.frame.origin.x, controller.bottomText.frame.origin.y + 88, controller.bottomText.frame.size.width, controller.bottomText.frame.size.height);
            controller.slideImage.frame = CGRectMake(controller.slideImage.frame.origin.x, controller.slideImage.frame.origin.y, controller.slideImage.frame.size.width, controller.slideImage.frame.size.height + 88);
            
            controller.slideImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"help-screen-%d-5", (page+1)]];
        } else
        {
            controller.slideImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"help-screen-%d", (page+1)]];
        }
        [_scroller addSubview:controller.view];
    }
    return controller;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *arrowImage = [UIImage imageNamed:@"list-arrow"];
    UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
    CGRect rect = CGRectMake(10, 0, arrowImage.size.width, arrowImage.size.height);
    arrowImageView.frame = rect;
    [prevButton addSubview:arrowImageView];
    UIImageView *arrowImageViewNext = [[UIImageView alloc] initWithImage:arrowImage];
    arrowImageViewNext.frame = rect;
    [nextButton addSubview:arrowImageViewNext];

    numberOfViews = 4;
    prevButton.transform = CGAffineTransformMakeRotation(3.142);
    [prevButton setHidden:YES];
    [submitButton setHidden:YES];
    submitButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:15.0];
    submitButton.titleLabel.textColor = [UIColor whiteColor];
    UIImage *buttonImage = [[UIImage imageNamed:@"help-button-close"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(2, 5, 2, 5)];
    [submitButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
//    submitButton setFrame:CGRectMake(submitButton.frame.origin.x, submitButton.frame.origin.x, submitButton.frame.size.width, <#CGFloat height#>)
    
    UIImage *panImage = [UIImage imageNamed:@"pan"];
    UIImageView *panImageView = [[UIImageView alloc] initWithImage:panImage];
    panImageView.frame = CGRectMake(0, 0, 400, 282);
    
    [_secondScroller setContentSize:CGSizeMake(400, 282)];
    [_secondScroller addSubview:panImageView];
    
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    [controllers addObject:[self buildScrollViewWithTitle:NSLocalizedString(@"Welcome on Beech !", @"") andContent:NSLocalizedString(@"You can now share beers you drink with your friends and earn badges", @"") forPage:0]];
    
    [controllers addObject:[self buildScrollViewWithTitle:NSLocalizedString(@"It's beeching !", @"") andContent:NSLocalizedString(@"Share a beer with a single touch, it's easy. You just beeched you first beer.", @"") forPage:1]];
    [controllers addObject:[self buildScrollViewWithTitle:NSLocalizedString(@"Buddies, beechs.", @"") andContent:NSLocalizedString(@"Follow your friends and discover new beers. They're not on beech ? Get to your mails, texts, pigeons.", @"") forPage:2]];
    [controllers addObject:[self buildScrollViewWithTitle:NSLocalizedString(@"Beechs champion", @"") andContent:NSLocalizedString(@"Become a beer expert and gather beech badges. Start by beeching your first beer !", @"") forPage:3]];

    _scroller.contentSize = CGSizeMake(_scroller.frame.size.width * numberOfViews, _scroller.frame.size.height);
    
    self.viewControllers = controllers;
    
    if (self.view.frame.size.height > 460)
    {
        NSLog(@"iphone 5");
        _scroller.frame = CGRectMake(0, 0, _scroller.frame.size.width, _scroller.frame.size.height + 88);
        _secondScroller.frame = CGRectMake(0, 0, _secondScroller.frame.size.width, _secondScroller.frame.size.height + 88);
        [panImageView setImage:[UIImage imageNamed:@"pan-5"]];
        panImageView.frame = CGRectMake(0, 0, panImageView.frame.size.width, panImageView.frame.size.height + 88);

    } else
    {
        NSLog(@"others");
    }
    
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender == _scroller)
    {
        
        float speedFactorsecond = (_secondScroller.contentSize.width - _secondScroller.frame.size.width) / _scroller.contentSize.width;
        
        // setting the x value of the contentOffset of the underlying scrollviews
        [_secondScroller setContentOffset:CGPointMake(speedFactorsecond * _scroller.contentOffset.x, 0)];
        
        // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
        // which a scroll event generated from the user hitting the page control triggers updates from
        // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
        if (pageControlUsed)
        {
            // do nothing - the scroll was initiated from the page control, not the user dragging
            return;
        }
        
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = _scroller.frame.size.width;
        int page = floor((_scroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageControl.currentPage = page;
        
        if (page > 0)
        {
            [prevButton setHidden:NO];
            if (page == [viewControllers count] - 1 )
            {
                [nextButton setHidden:YES];
                [submitButton setHidden:NO];
            } else
            {
                [nextButton setHidden:NO];
                [submitButton setHidden:YES];
            }
        } else if (page == 0)
        {
            [prevButton setHidden:YES];
        }
        
        // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
        
        // A possible optimization would be to unload the views+controllers which are no longer visible
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (IBAction)changePage:(id)sender
{
    int page = pageControl.currentPage;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    
	// update the scroll view to the appropriate page
    CGRect frame = _scroller.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [_scroller scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (void)viewDidUnload {
    _scroller = nil;
    pageControl = nil;
    nextButton = nil;
    nextButton = nil;
    prevButton = nil;
    submitButton = nil;
    _secondScroller = nil;
    pageView = nil;
    [super viewDidUnload];
}
- (IBAction)prevButtonTapped:(id)sender
{
    if (pageControl.currentPage > 0)
    {
        int xOffset = _scroller.frame.size.width * (pageControl.currentPage - 1);
        [_scroller setContentOffset:CGPointMake(xOffset, 0) animated:YES];
    }
}

- (IBAction)nextButtonTapped:(id)sender
{
    if (pageControl.currentPage < [viewControllers count] - 1 )
    {
        int xOffset = _scroller.frame.size.width * (pageControl.currentPage + 1);
        [_scroller setContentOffset:CGPointMake(xOffset, 0) animated:YES];

    }
}
- (IBAction)submitButtonTapped:(id)sender
{
    if (pageControl.currentPage == [viewControllers count] - 1)
    {
        if (delegate)
        {
            [self dismissViewControllerAnimated:NO completion:^{
                [delegate dismissViewControllerAnimated:YES completion:nil];
            }];
        } else
        {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }

}

@end
