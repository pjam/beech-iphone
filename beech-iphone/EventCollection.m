//
//  EventCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "EventCollection.h"
#import "Comment.h"
#import "Like.h"
#import "User.h"
#import "Beer.h"
#import "UserCollection.h"

@implementation EventCollection
- (Event*)eventForId:(NSNumber *)event_id
{
    NSUInteger eventIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id event, NSUInteger idx,BOOL *stop)
     {
         return [((Event*)event).event_id intValue] == [event_id intValue];
     }];
    return [self.collection objectAtIndex:eventIndex];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Event *newEvent = [MTLJSONAdapter modelOfClass:Event.class fromJSONDictionary:item error:nil];
        [self addObject:newEvent];
    }
}

- (BOOL)addObject:(id)anObject
{
    NSUInteger eventIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id event, NSUInteger idx,BOOL *stop)
     {
         return [((Event*)event).event_id intValue] == [((Event*)anObject).event_id intValue];
     }];

    if (eventIndex == NSNotFound) {
        NSUInteger newIndex = [self.collection indexOfObject:anObject
                                               inSortedRange:(NSRange){0, [self.collection count]}
                                                     options:NSBinarySearchingInsertionIndex
                                             usingComparator:^NSComparisonResult(id a, id b) {
                                                 NSNumber *first = [(Event*)a created_at];
                                                 NSNumber *second = [(Event*)b created_at];
                                                 return [second compare:first];
                                             }];
        [self.collection insertObject:anObject atIndex:newIndex];
        return YES;
    } else {
        return NO;
    }
}

- (void)sort
{
    self.collection = [[self.collection sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSNumber *first = [(Event*)a created_at];
        NSNumber *second = [(Event*)b created_at];
        return [second compare:first];
    }] mutableCopy];
}

- (void)save:(NSNumber *)limit
{
    // This assumes that the collection is sorted
    FMDatabase *database = [SqliteDB DB];
    [database open];
    long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);

    int i = 0;
    while (i < [self.collection count] && i < [limit intValue]) {
        Event *event = [self.collection objectAtIndex:i];
        [event saveWithDB:database];
        i++;
    }

    // Remove too old elements
    FMResultSet *results = [database executeQuery:@"SELECT * FROM events ORDER BY created_at DESC LIMIT 1 OFFSET ?", limit];

    NSMutableArray *eventIds = [[NSMutableArray alloc] init];
    NSNumber *eventId;
    while([results next]) {
        [eventIds addObject:[results stringForColumn:@"id"]];
    }
    if ([eventIds count] > 0) {
        eventId = (NSNumber*)eventIds[0];

        [database executeUpdate:@"DELETE FROM events WHERE id <= ?", eventId];
    }
    // TODO : Remove obsolete users;

    [database close];
    long long milliseconds2 = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);

    NSLog(@"This lasted : %lld", milliseconds2 - milliseconds);
}

- (Event *)objectAtIndex:(NSUInteger)index
{
    return (Event*)[self.collection objectAtIndex:index];
}

- (void)updateEventWithId:(NSNumber *)eventId andData:(NSDictionary *)jsonDict
{
    NSLog(@"%@", eventId);
    NSLog(@"%@", jsonDict);

    NSDictionary *eventDict = [jsonDict objectForKey:@"event"];
    Event *event = [MTLJSONAdapter modelOfClass:Event.class fromJSONDictionary:eventDict error:nil];
    event.likes = [[NSMutableArray alloc] init];
    event.comments = [[NSMutableArray alloc] init];

    // User
    UserCollection *users = [[UserCollection alloc] init];
    for (NSDictionary *userDict in [jsonDict objectForKey:@"users"]) {
        User *user = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:userDict error:nil];
        [users addObject:user];
    }

    event.user = [users userForId:event.user_id];

    // Comments
    [event.comments removeAllObjects];
    for (NSDictionary* commentDict in [jsonDict objectForKey:@"comments"]) {
        Comment *newComment = [MTLJSONAdapter modelOfClass:Comment.class fromJSONDictionary:commentDict error:nil];
        newComment.user = [users userForId:newComment.user_id];
        [event.comments addObject:newComment];
    }
    // Likes
    [event.likes removeAllObjects];
    for (NSDictionary* likeDict in [jsonDict objectForKey:@"likes"]) {
        Like *newLike = [MTLJSONAdapter modelOfClass:Like.class fromJSONDictionary:likeDict error:nil];
        newLike.user = [users userForId:newLike.user_id];
        [event.likes addObject:newLike];
    }
    // Beer Color
    // TODO
    // Beer
    NSDictionary *beerDict = [[jsonDict objectForKey:@"beers"] objectAtIndex:0];
    Beer *beer = [MTLJSONAdapter modelOfClass:Beer.class fromJSONDictionary:beerDict error:nil];

    // Check
    NSDictionary *checkDict = [[jsonDict objectForKey:@"checks"] objectAtIndex:0];
    Check *check = [MTLJSONAdapter modelOfClass:Check.class fromJSONDictionary:checkDict error:nil];

    check.beer = beer;
    event.check = check;

    // TODO award and badges

    // Replace the old event
    Event *oldEvent = [self eventForId:eventId];
    NSUInteger eventIndex = [self.collection indexOfObject:oldEvent];
    [event saveWithDB:nil];
    [self.collection replaceObjectAtIndex:eventIndex withObject:event];
}

- (void)removeAllObjects
{
    [super removeAllObjects];
    FMDatabase *db = [SqliteDB DB];
    [db open];

    [db executeUpdate:@"DELETE FROM events"];


    [db close];
}

@end
