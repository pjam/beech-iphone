//
//  SearchDelegateViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 5/8/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SearchDelegateViewController.h"
#import "BeerCell.h"

@interface SearchDelegateViewController ()

@end

@implementation SearchDelegateViewController

@synthesize searchResults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BeerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BeerSearchCell"];
    if (cell == nil) {
        cell = [[BeerCell alloc] init];
        [cell initContent];
    }

    cell.name.text = [[searchResults objectAtIndex:indexPath.row ] name];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_parent.tableView setHidden:NO];
    [_parent dismissKeyboard];
}

@end
