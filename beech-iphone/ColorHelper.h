//
//  ColorHelper.h
//  beech-iphone
//
//  Created by Pierre Jambet on 15/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorHelper : NSObject
+(UIColor*) greyColor;
+(UIColor*) darkColor;
+(UIColor*) blackColor;
+(UIColor*) blueColor;
@end
