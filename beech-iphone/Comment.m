//
//  Comment.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Comment.h"

@implementation Comment

@synthesize comment_id, event, user, content, user_id, event_id;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"comment_id": @"id",
             @"event_id": @"event_id",
             @"user_id": @"user_id",
             @"content": @"content",
             @"created_at": @"created_at",
             };
}
@end
