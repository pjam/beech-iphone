//
//  CommentCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "CommentCell.h"
#import "ColorHelper.h"

@implementation CommentCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Additional setup goes HERE

        int yOffset = 8;
        int xOffset = 8;
        int imageWidth = 38;
        int paddingLeft = 10;
        int nameHeight = 20;
        int textWidth = 260;
        int descHeight = nameHeight;

        self.name = [[UILabel alloc] initWithFrame:CGRectMake(imageWidth + xOffset + paddingLeft, yOffset, textWidth, nameHeight)];
        self.name.font = [UIFont fontWithName:@"ProximaNova-Bold" size:16.0];

        self.desc = [[UILabel alloc] initWithFrame:CGRectMake(imageWidth + xOffset + paddingLeft, yOffset + nameHeight, textWidth, descHeight)];
        [self.desc setTextColor:[UIColor colorWithRed:0.40f green:0.40f blue:0.40f alpha:1.00f]];
        self.desc.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16.0];
        self.desc.lineBreakMode = NSLineBreakByWordWrapping;
        self.desc.numberOfLines = 0;

        int timeWidth = 40;
        int timeHeight = 20;
        self.time = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - timeWidth - xOffset, yOffset, timeWidth, timeHeight)];
        self.time.textAlignment = UITextAlignmentRight;
        [self.time setTextColor:[ColorHelper greyColor]];
        [self.time setFont: [UIFont fontWithName:@"ProximaNova-Regular" size:14]];
        [self.time setHighlighted:NO];


        [self.contentView addSubview:self.name];
        [self.contentView addSubview:self.desc];
        self.accessoryView = nil;
    }
    return self;
}


- (int)regularHeight
{
    return 60;
}

- (void) computeDescLabelHeight
{
    CGSize maximumLabelSize = CGSizeMake(self.desc.frame.size.width, FLT_MAX);

    CGSize expectedLabelSize = [self.desc.text sizeWithFont:self.desc.font constrainedToSize:maximumLabelSize lineBreakMode:self.desc.lineBreakMode];

    CGRect newFrame = self.desc.frame;
    newFrame.size.height = expectedLabelSize.height;
    self.desc.frame = newFrame;
}

@end
