//
//  Beer.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Beer.h"
#import "SqliteDB.h"

@implementation Beer
{
    FMDatabase *database;
}
@synthesize name, beer_color, country;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"beer_id": @"id",
             @"name": @"name",
             @"country": @"country",
             @"color_pattern": @"color_pattern",
             @"font_color": @"font_color",
             @"background_color": @"background_color",
             };
}

- (FMDatabase*) getDatabase
{
    if (database == nil) {
        database = [SqliteDB DB];
        [database open];
    }
    return database;
}

- (id)init
{
    self = [super init];
    return self;
}


- (void)dealloc
{
    [database close];
    database = nil;
}

- (BOOL)saveWithDB:(FMDatabase *)db forceInsert:(BOOL)isForce
{
    if (db == nil) {
        db = [self getDatabase];
    }

    NSString *countryName;
    if (self.country) {
        countryName = self.country;
    } else {
        countryName = @"";
    }
    NSDictionary *argsDict = @{@"beer_id": self.beer_id,
                               @"beer_name": self.name,
                               @"font_color": self.font_color,
                               @"background_color": self.background_color,
                               @"country": countryName};
    // Insert no matter what
    if (isForce) {
        [db executeUpdate:@"INSERT INTO beers (id, name, country, font_color, background_color) VALUES (:beer_id, :beer_name, :country, :font_color, :background_color)" withParameterDictionary:argsDict];
        return YES;
    } else {
        NSDictionary *argsDict = @{@"beer_id": self.beer_id};
        FMResultSet *results = [db executeQuery:@"SELECT * FROM beers WHERE id = :beer_id" withParameterDictionary:argsDict];

        Beer *beerInDb = [[Beer alloc] init];
        while([results next]) {
            beerInDb.beer_id = (NSNumber*)[results stringForColumn:@"id"];
        }

        if(beerInDb.beer_id > 0) {
            // Should we update the existing record ?
            NSLog(@"%@", beerInDb);
            NSDictionary *args = @{@"name": self.name,
                                   @"font_color": self.font_color,
                                   @"background_color": self.background_color,
                                   @"beer_id": self.beer_id,
                                   @"country": countryName};
            [db executeUpdate:@"UPDATE beers SET name = :name, font_color = :font_color, background_color = :background_color, country = :country where id = :beer_id" withParameterDictionary:args];
            return NO;
        } else {
            [db executeUpdate:@"INSERT INTO beers (id, name, country, font_color, background_coor) VALUES (:beer_id, :beer_name, :country, :font_color, :background_color)" withParameterDictionary:argsDict];
            return YES;
        }
    }
}

+ (Beer *)find:(NSNumber *)beer_id withDB:(FMDatabase *)db
{
    BOOL dbWasNil = NO;
    if (db == nil) {
        db = [SqliteDB DB];
        dbWasNil = YES;
    }
    Beer* beer = [[Beer alloc] init];
    [beer setBeer_id:beer_id];

    FMResultSet *results = [db executeQuery:@"SELECT * from beers where id = ?", beer_id];

    while ([results next]) {
        [beer setBackground_color:[results stringForColumn:@"background_color"]];
        [beer setFont_color:[results stringForColumn:@"font_color"]];
        [beer setCountry:[results stringForColumn:@"country"]];
        [beer setName:[results stringForColumn:@"name"]];
    }
    

    if (dbWasNil) {
        [db close];
    }
    return beer;
}

@end
