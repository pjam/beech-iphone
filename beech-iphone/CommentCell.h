//
//  CommentCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "LikeCell.h"

@interface CommentCell : LikeCell
-(int) regularHeight;
- (void) computeDescLabelHeight;
@end
