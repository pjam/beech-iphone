//
//  Event.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "Eventable.h"
#import "Beer.h"
#import "Badge.h"
#import "Check.h"
#import "Award.h"
#import "User.h"
#import "BeechModel.h"

@interface Event : BeechModel
@property(nonatomic) NSNumber *event_id;
@property(nonatomic) NSNumber *user_id;
@property(nonatomic) NSNumber *created_at;
@property(nonatomic) NSDictionary *eventable;
@property(nonatomic) BOOL is_liked;
@property(nonatomic) Check *check;
@property(nonatomic) Award *award;
@property(nonatomic) User *user;
@property(nonatomic) NSMutableArray *comments;
@property(nonatomic) NSMutableArray *likes;

- (BOOL) isEqualToEvent:(Event*) event;
- (BOOL) isAward;
- (BOOL) isCheck;
- (NSString*) eventType;
@end
