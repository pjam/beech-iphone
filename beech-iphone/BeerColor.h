//
//  BeerColor.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface BeerColor : MTLModel
@property(nonatomic) NSString *name;
@end
