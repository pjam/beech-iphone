//
//  UserCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "User.h"

@interface UserCollection : Collection
-(User*) userForId:(NSNumber*) user_id;

@end
