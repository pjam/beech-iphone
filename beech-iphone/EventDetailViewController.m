//
//  EventDetailViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/19/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "EventDetailViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    int paddingVert = 10;
    int paddingHor = 10;
    [self.refreshButton setFrame:CGRectMake(paddingHor, paddingVert, self.view.frame.size.width - (2 * paddingHor), 44)];
    self.tableHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.refreshButton.frame.size.height + (2 * paddingVert))];
    [self.tableHeader setBackgroundColor:[UIColor whiteColor]];
    [self.refreshButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16]];
    self.refreshButton.layer.borderColor = [[UIColor colorWithRed:0.83f green:0.83f blue:0.83f alpha:1.00f] CGColor];
    self.refreshButton.layer.borderWidth = 1.0f;
    self.refreshButton.layer.cornerRadius = 3.0f;
    [self.refreshButton setTitleColor:[UIColor colorWithRed:0.40f green:0.40f blue:0.40f alpha:1.00f] forState:UIControlStateNormal];

    [self.refreshButton setBackgroundColor:[UIColor colorWithRed:0.92f green:0.92f blue:0.92f alpha:1.00f]];

    [self.refreshButton setTitle:NSLocalizedString(@"Refresh comments", nil) forState:UIControlStateNormal];
    [self.tableHeader addSubview:self.refreshButton];

    [self.refreshButton addTarget:self action:@selector(refreshButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
