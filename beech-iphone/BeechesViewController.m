//
//  BeechesViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechesViewController.h"
#import "ASIHTTPRequest.h"
#import "ProfileViewController.h"
#import "TopBeerViewController.h"
#import "BeerDetailCell.h"
#import "RequestHelper.h"
#import "BeerDetailCell.h"
#import <QuartzCore/QuartzCore.h>

@interface BeechesViewController ()
{
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UIView *_titleBackground;
    NSArray* beers;
    __weak IBOutlet UIPageControl *pageViewControl;
    __weak IBOutlet UILabel *topBeechCount;
    __weak IBOutlet UILabel *topBeechName;
    __weak IBOutlet UILabel *topBeechIndex;
    UIScrollView *topScroller;
    __weak IBOutlet UIView *topWrapper;
    __weak IBOutlet UILabel *wallOfBeeches;
    NSInteger numberOfViews;
}

@end

@implementation BeechesViewController
@synthesize forceRefresh, viewControllers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= 3)
        return;
    if (page >= [beers count])
        return;
    if ([beers count] == 0)
        return;
    
    NSDictionary *beer = [beers objectAtIndex:page];
    // replace the placeholder if necessary
    TopBeerViewController *controller = [viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null])
    {
        controller = [[TopBeerViewController alloc] initWithBeerCount:[beer objectForKey:@"checks_count"] beerName:[beer objectForKey:@"name"] beerIndex:[[beer objectForKey:@"id"]intValue] ];
        [viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil)
    {
        CGRect frame = topScroller.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;
        [topScroller addSubview:controller.view];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    forceRefresh = YES;
    
    numberOfViews = 3;
    
    topScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 88)];
    topScroller.scrollsToTop = NO;
    [self.view sendSubviewToBack:topScroller];
    
    topScroller.pagingEnabled = YES;
    topScroller.contentSize = CGSizeMake(topScroller.frame.size.width * numberOfViews, topScroller.frame.size.height);
    topScroller.showsHorizontalScrollIndicator = NO;
    topScroller.showsVerticalScrollIndicator = NO;
    topScroller.scrollsToTop = NO;
    topScroller.delegate = self;
    

    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < numberOfViews; i++)
    {
		[controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;

    [topWrapper addSubview:topScroller];
    [self fetchContent];
    
//    topScroller.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"panorama"]];

    [wallOfBeeches setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:10.0]];
    
    self.navigationItem.title = @"Beeches";
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [header setBackgroundColor:[UIColor clearColor]];
    UIImage *imageHeader = [UIImage imageNamed:@"heading-list-wip"];
    UIImageView *tableHeaderView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    tableHeaderView.image = imageHeader;
    [header addSubview:tableHeaderView];
    [_tableView setTableHeaderView:header];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [header setBackgroundColor:[UIColor clearColor]];
    UIImage *imageFooter = [UIImage imageNamed:@"bg-list-bottom-index"];
    UIImageView *tableFooterView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    tableFooterView.image = imageFooter;
    [footer addSubview:tableFooterView];
    [_tableView setTableFooterView:footer];
    
	// Do any additional setup after loading the view.
    _titleBackground.layer.masksToBounds = NO;
    _titleBackground.layer.shadowOffset = CGSizeMake(0, 2);
    _titleBackground.layer.shadowRadius = 2;
    _titleBackground.layer.shadowOpacity = 0.1;
    
}

-(void) fetchContent
{
    NSURL *url;
    NSUInteger arraySize = [self.navigationController.viewControllers count];
    ProfileViewController* prevController = [self.navigationController.viewControllers objectAtIndex:arraySize-2];
    if ([prevController onCurrentUserProfile])
    {
        url = [RequestHelper createURL:@"/my/beers" withPrefix:@"/api"];
    } else
    {
        url = [RequestHelper createURL:[NSString stringWithFormat:@"/users/%@/beers",[prevController user_id]] withPrefix:@"/api"];
    }
    
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSMutableDictionary *feed = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        beers = [[feed objectForKey:@"beers"] mutableCopy];

        [_tableView reloadData];
        [self loadScrollViewWithPage:0];
        [self loadScrollViewWithPage:1];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (YES)
    {
        forceRefresh = NO;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [beers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BeerDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BeerDetailCell"];
    
    cell.name.text = [[beers objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.count.text = [NSString stringWithFormat:@"%@", [[beers objectAtIndex:indexPath.row] objectForKey:@"checks_count"]];
    if (indexPath.row >= 3)
    {
        [cell setCountColor:[BeerDetailCell greyFontColor]];
    } else {
        [cell setCountColor:[BeerDetailCell blueFontColor]];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender == topScroller)
    {
        // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
        // which a scroll event generated from the user hitting the page control triggers updates from
        // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
        if (pageControlUsed)
        {
            // do nothing - the scroll was initiated from the page control, not the user dragging
            return;
        }
        
        // Switch the indicator when more than 50% of the previous/next page is visible
        CGFloat pageWidth = topScroller.frame.size.width;
        int page = floor((topScroller.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        pageViewControl.currentPage = page;
        
        // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
        
        // A possible optimization would be to unload the views+controllers which are no longer visible
    }
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (IBAction)changePage:(id)sender
{
    int page = pageViewControl.currentPage;
	
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
	// update the scroll view to the appropriate page
    CGRect frame = topScroller.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [topScroller scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}

- (void)viewDidUnload {
    _titleBackground = nil;
    _tableView = nil;
    pageViewControl = nil;
    topBeechCount = nil;
    topBeechName = nil;
    topBeechIndex = nil;
    topScroller = nil;
    topWrapper = nil;
    wallOfBeeches = nil;
    [super viewDidUnload];
}
@end
