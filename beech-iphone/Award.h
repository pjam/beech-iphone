//
//  Award.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "Badge.h"

@interface Award : MTLModel <MTLJSONSerializing>
@property(nonatomic) NSNumber *award_id;
@property(nonatomic) NSNumber *user_id;
@property(nonatomic) NSNumber *badge_id;
@property(nonatomic) NSNumber *created_at;
@property(nonatomic) Badge *badge;
@end
