//
//  BeechUIBarButtonItem.h
//  beech-iphone
//
//  Created by Pierre Jambet on 16/02/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeechUIBarButtonItem : UIBarButtonItem

@end
