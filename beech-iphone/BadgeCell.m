//
//  FeedCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BadgeCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation BadgeCell

@synthesize badgeCount, badgeDesc, badgeName, badgeImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIImage *badgeBackgroundImage = [UIImage imageNamed:@"bg-badges.png"];
//        UIImageView *badgeBackgroundImageView = [[UIImageView alloc] initWithImage:badgeBackgroundImage];

        UIImage *ribbonImage = [UIImage imageNamed:@"yay-ribbon"];
        UIImageView *ribbon = [[UIImageView alloc] initWithImage:ribbonImage];
        badgeImageView.frame = CGRectMake(10, 20, 38, 38);

        badgeImageView = [[UIImageView alloc] initWithImage:nil];
        badgeImageView.frame = CGRectMake(10, 20, 57, 57);
        //    [badgeImageView setBackgroundColor:[UIColor blueColor]];


        //        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
        //        label.backgroundColor = [UIColor clearColor];
        //        label.textAlignment = UITextAlignmentCenter;
        //        label.textColor = [UIColor whiteColor];
        //        label.text = @"beech";
        //        label.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:18.0];

        badgeName = [[UILabel alloc] initWithFrame:CGRectMake(80, 20, 150, 15)];
        //    badgeName.text = @"Gros Viking";
        badgeName.textColor = [UIColor colorWithRed:0.93f green:0.39f blue:0.13f alpha:1.00f];
        badgeName.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:16.0];
        badgeName.backgroundColor = [UIColor clearColor];

        //    badgeCount = [[UILabel alloc] initWithFrame:CGRectMake(75, 24, 100, 15)];
        //        badgeCount.text = @"BADGE #4";
        //    badgeCount.textColor = [UIColor whiteColor];
        //    badgeCount.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0];
        //    badgeCount.textColor = [UIColor colorWithRed:134.0/255.0 green:134.0/255.0 blue:134.0/255.0 alpha:1];
        //    badgeCount.backgroundColor = [UIColor clearColor];


        self.badgeDesc = [[UILabel alloc] initWithFrame:CGRectMake(80, 29, 162, 40)];
        self.badgeDesc.text = @"Description ...";
        self.badgeDesc.backgroundColor = [UIColor clearColor];
        self.badgeDesc.textColor = [UIColor colorWithRed:0.43f green:0.43f blue:0.35f alpha:1.00f];
        self.badgeDesc.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
        self.badgeDesc.numberOfLines = 2;
        NSLog(@"%@", badgeDesc);

        self.cartouche = [[UIView alloc] initWithFrame:CGRectMake(10, 70, 286, 84)];
        [self.cartouche setBackgroundColor:[UIColor colorWithRed:0.99f green:0.99f blue:0.96f alpha:1.00f]];
        self.cartouche.layer.cornerRadius = 2.0f;
        [self.cartouche addSubview:self.beer];
        
        //    [self.cartouche addSubview:badgeCount];
        [self.cartouche addSubview:badgeName];
        [self.cartouche addSubview:self.badgeDesc];
        [self.cartouche addSubview:ribbon];
        [self.cartouche addSubview:badgeImageView];
        [self.contentView addSubview:self.cartouche];

    }
    return self;
}

- (void)initContent
{

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


@end
