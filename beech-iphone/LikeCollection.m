//
//  LikeCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "LikeCollection.h"

@implementation LikeCollection
- (Like*)likeForId:(NSNumber *)like_id
{
    NSUInteger likeIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id like, NSUInteger idx,BOOL *stop)
     {
         return [((Like*)like).like_id intValue] == [like_id intValue];
     }];
    return [self.collection objectAtIndex:likeIndex];
}

- (NSMutableArray *)likesForEventId:(NSNumber *)event_id
{
    NSIndexSet *likesIndexes = [self.collection indexesOfObjectsPassingTest:^BOOL(id like, NSUInteger idx, BOOL *stop)
    {
        return [((Like*)like).event_id intValue] == [event_id intValue];
    }];
    return [[self.collection objectsAtIndexes:likesIndexes] mutableCopy];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Like *newLike = [MTLJSONAdapter modelOfClass:Like.class fromJSONDictionary:item error:nil];
        [self addObject:newLike];
    }
}

- (BOOL)addObject:(id)anObject
{
//    Like *newLike = [MTLJSONAdapter modelOfClass:Like.class fromJSONDictionary:anObject error:nil];

    NSUInteger likeIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id like, NSUInteger idx,BOOL *stop)
     {
         return [((Like*)like).like_id intValue] == [((Like*)anObject).like_id intValue];
     }];
    if (likeIndex == NSNotFound) {
        NSUInteger newIndex = [self.collection indexOfObject:anObject
                                               inSortedRange:(NSRange){0, [self.collection count]}
                                                     options:NSBinarySearchingInsertionIndex
                                             usingComparator:^NSComparisonResult(id a, id b) {
                                                 NSNumber *first = [(Event*)a created_at];
                                                 NSNumber *second = [(Event*)b created_at];
                                                 return [first compare:second];
                                             }];
        [self.collection insertObject:anObject atIndex:newIndex];
        return YES;
    } else {
        return NO;
    }

}
@end
