//
//  ProfileViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"
#import "FeedCell.h"
#import "EventsViewController.h"

@interface ProfileViewController : EventsViewController <UITableViewDelegate, UITableViewDataSource, PullToRefreshViewDelegate, FeedCellDelegate>

@property(nonatomic) NSNumber *user_id;
@property(nonatomic) BOOL forceRefresh;
@property(nonatomic) BOOL loadingAnteriorData;
- (BOOL) onCurrentUserProfile;
- (void) clear;
@end
