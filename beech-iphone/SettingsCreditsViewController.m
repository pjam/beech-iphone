//
//  SettingsCreditsViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 09/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SettingsCreditsViewController.h"
#import "ColorHelper.h"

@interface SettingsCreditsViewController ()
{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIImageView *imageView;
}
@end

@implementation SettingsCreditsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Credits", @"");
    scrollView.contentSize = CGSizeMake(320, 409);
    if (self.view.frame.size.height > 460)
    {
        scrollView.frame = CGRectMake(0, 0, 320, scrollView.frame.size.height + 88);

    } else
    {
        scrollView.frame = CGRectMake(0, 0, 320, 365);
    }

    NSString *imageBackground = @"credits-bg";
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];    
    if ([language isEqualToString:@"en"]) {
        imageBackground = [NSString stringWithFormat:@"%@-EN", imageBackground];
    }
    imageView.image = [UIImage imageNamed:imageBackground];
    
    // Append the version number
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10, imageView.frame.origin.y + imageView.frame.size.height - 20, 200, 20)];
    [lab setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:12.0]];
    [lab setText:[NSString stringWithFormat:@"%@(%@)", version, build]];
    [lab setTextColor:[ColorHelper greyColor]];
    [lab setBackgroundColor:[UIColor clearColor]];
    [scrollView addSubview:lab];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    scrollView = nil;
    imageView = nil;
    [super viewDidUnload];
}
@end
