//
//  LikeButton.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/14/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "LikeButton.h"

@implementation LikeButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initLikeButton
{
    self = [super init];
//    [self setTitle:@"<3" forState:UIControlStateNormal];
    // TODO : Use that when Ill have the real images
    //    [likeButton setBackgroundImage:unselectedImage forState:UIControlStateSelected];
    //    [likeButton setBackgroundImage:selectedImage forState:UIControlStateSelected];
    //    [likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    [self setTitleColor:[UIColor redColor] forState:UIControlStateSelected|UIControlStateHighlighted];
    UIImage *defaultImage = [UIImage imageNamed:@"widget-button-default"];
    UIImage *selectedImage = [UIImage imageNamed:@"widget-button-pressed"];
    defaultImage = [defaultImage stretchableImageWithLeftCapWidth:4.0f topCapHeight:8.0f];
    selectedImage = [selectedImage stretchableImageWithLeftCapWidth:4.0f topCapHeight:9.0f];

    [self setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    [self.titleLabel setFont: [UIFont fontWithName:@"ProximaNova-Regular" size:14]];
//    self.highlighted = NO;
//    [self setHighlighted:NO];
    self.adjustsImageWhenDisabled = NO;

    return self;
}


@end
