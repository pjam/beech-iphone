//
//  WelcomeViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 09/02/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController <UIScrollViewDelegate>
{
    BOOL pageControlUsed;
}
@property (nonatomic, retain) NSMutableArray *viewControllers;
@property (nonatomic, assign) id delegate;
@end
