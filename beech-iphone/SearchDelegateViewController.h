//
//  SearchDelegateViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 5/8/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeechViewController.h"

@interface SearchDelegateViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) NSArray *searchResults;
@property (nonatomic, retain) BeechViewController *parent;
@end
