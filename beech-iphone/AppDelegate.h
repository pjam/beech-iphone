//
//  AppDelegate.h
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kServerURL   @"http://beech-server-staging.herokuapp.com"
//#define kServerURL   @"http://beech-server.herokuapp.com"
//#define kServerURL   @"http://192.168.0.17:3000" // @luc
//#define kServerURL   @"http://192.168.2.2:3000" // @pjam
#define kURLPathPrefix   @"/api"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
