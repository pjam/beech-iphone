//
//  LikesViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "LikesViewController.h"
#import "LikeCollection.h"
#import "UserCollection.h"
#import "User.h"
#import "RequestHelper.h"
#import "LikeCell.h"
#import "BeechHelper.h"
#import "ProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface LikesViewController ()
{
    LikeCollection *likes;
    UserCollection *users;
    UIView *emptyView;
    NSNumber* selectedUserId;
    __weak IBOutlet UIButton *refreshButton;
    __weak IBOutlet UITableView *_tableView;
}
@end

@implementation LikesViewController

@synthesize event;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_tableView setTableHeaderView:self.tableHeader];

    [self.navigationItem setTitle:NSLocalizedString(@"Likes", @"")];
    likes = [[LikeCollection alloc] init];
    users = [[UserCollection alloc] init];
    [_tableView setHidden:YES];
    [self setLoadingGlobal:YES];
    [self loadContent];

    emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 120, 320, 100)];
    UIImage *emptyImage = [UIImage imageNamed:@"icon-like-empty"];
    UIImageView *emptyIcon = [[UIImageView alloc] initWithImage:emptyImage];
    emptyIcon.frame = CGRectMake(124.5, 0, 76, 71);

    UILabel *emptyText = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 320, 50)];
    emptyText.text = NSLocalizedString(@"Come on, you wanna like that !", nil);
    emptyText.textAlignment = UITextAlignmentCenter;
    [emptyText setBackgroundColor:[UIColor clearColor]];
    [emptyText setTextColor:[UIColor colorWithRed:0.56f green:0.56f blue:0.56f alpha:1.00f]];
    [emptyText setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:16.0]];

    [emptyView addSubview:emptyIcon];
    [emptyView addSubview:emptyText];
    emptyView.hidden = YES;

    [self.view addSubview:emptyView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadContent
{
    NSURL *url = [RequestHelper createURL:[NSString stringWithFormat:@"/events/%@/likes", event.event_id] withPrefix:@"/api"];

    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", likes);
        NSNumber *total = [[json objectForKey:@"meta"] objectForKey:@"total"];
        [self loadDataFromDictionary:json];
        if ([total intValue] <= [likes count]) {
            [_tableView setTableHeaderView:nil];
        }
        [_tableView reloadData];
        [_tableView setHidden:NO];
        [self goToLastRow];
        [self.loadingView setHidden:YES];
        if ([likes count] == 0) {
            [emptyView setHidden:NO];
        }

    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [self.loadingView setHidden:YES];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];

}

- (void) refresh
{

    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO];
    NSArray *sortedArray=[likes sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];

    NSNumber *timestamp = [[sortedArray lastObject] created_at];
    NSURL *url = [RequestHelper createURL:[NSString stringWithFormat:@"/events/%@/likes", event.event_id] withPrefix:@"/api"];

    NSDictionary *args = @{@"before": timestamp};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [self.refreshButton setHidden:YES];
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        [self loadDataFromDictionary:json];
        [_tableView setTableHeaderView:nil];
        [_tableView reloadData];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [self.loadingView setHidden:YES];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
    
    
}

- (void) goToLastRow
{
    if ([likes count] > 0) {
        NSIndexPath* ip = [NSIndexPath indexPathForRow:[likes count] - 1 inSection:0];
        [_tableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

- (void) loadDataFromDictionary:(NSDictionary*) dict
{
    NSLog(@"%@", dict);
    for (NSDictionary* u in [dict objectForKey:@"users"]) {
        NSError *err = nil;
        User *user = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:u error:&err];

        [users addObject:user];
    }


    for (NSDictionary* l in [dict objectForKey:@"likes"]) {
        NSError *err = nil;
        Like *like = [MTLJSONAdapter modelOfClass:Like.class fromJSONDictionary:l error:&err];
        [like setUser:[users userForId:like.user_id]];
        [likes addObject:like];
    }

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [likes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LikeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LikeCell"];

    Like *like = [likes objectAtIndex:indexPath.row];

    [cell.name setText:like.user.nickname];
    NSURL *imageUrl = [NSURL URLWithString:[[like.user.avatar_url objectForKey:@"thumb"] objectForKey:@"url"]];
//    NSURL *imageUrl = [NSURL URLWithString:[[[user objectForKey:@"avatar_url" ] objectForKey:@"thumb" ] objectForKey:@"url" ]];

    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];

    cell.time.text = [BeechHelper stringForTimestamp:like.created_at serverTime:[NSDate date]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedUserId = [[likes objectAtIndex:indexPath.row] user_id];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];

    if ([[defaults valueForKey:@"user_id"] isEqualToNumber:selectedUserId])
    {
        // GO to my profile
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    } else
    {
        [self performSegueWithIdentifier:@"LikeProfileSegue" sender:self];
    }

}

- (void)viewDidUnload {
    refreshButton = nil;
    _tableView = nil;
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LikeProfileSegue"])
    {
        // Get reference to the destination view controller
        ProfileViewController *vc = [segue destinationViewController];

        NSLog(@"Hei : %@", vc);
        // Pass any objects to the view controller here, like...
        Like *like = [likes objectAtIndex:[[_tableView indexPathForSelectedRow] row]];
        [vc setUser_id:like.user_id];
    }
}

-(void) setLoadingGlobal:(BOOL)glob
{
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }

    if (glob) {
        self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 130, 308, [_tableView tableHeaderView].frame.size.height)];
        [self.view addSubview:self.loadingView];
    } else {
        self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, [_tableView tableHeaderView].frame.size.height)];
        [_tableView.tableHeaderView addSubview:self.loadingView];
    }

    [self.loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width / 2) - 21, 10,42,42)];
    [self.loadingView addSubview:anImageView];

    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
}

- (IBAction)refreshButtonTapped:(id)sender {
    NSLog(@"Tapped");
    [self setLoadingGlobal:NO];
    [self refresh];
}
@end
