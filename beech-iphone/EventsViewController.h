//
//  EventsViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/14/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Badge.h"
#import "BadgeCollection.h"
#import "Event.h"
#import "EventCollection.h"
#import "Check.h"
#import "CheckCollection.h"
#import "Award.h"
#import "AwardCollection.h"
#import "Beer.h"
#import "BeerCollection.h"
#import "User.h"
#import "UserCollection.h"
#import "Like.h"
#import "LikeCollection.h"
#import "Comment.h"
#import "CommentCollection.h"
#import "FeedCell.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface EventsViewController : UIViewController <FeedCellDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
@property CheckCollection *checks;
@property BeerCollection *beers;
@property UserCollection *users;
@property AwardCollection *awards;
@property BadgeCollection *badges;
@property EventCollection *events;
@property LikeCollection *likes;
@property CommentCollection *comments;
@property NSNumber *selectedEventId;
@property BOOL likingEvent;

- (int) loadDataWithDictionary:(NSDictionary*) dict Reversed:(BOOL)reversed;
- (void) setRegularFooter;
- (void) clear;
@end
