//
//  SecondViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FollowingsViewController.h"
#import "ProfileViewController.h"
#import "UserCell.h"
#import "RequestHelper.h"
#import "BeechHelper.h"
#import "ColorHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface FollowingsViewController ()
{
    UIView *loadingView;    
    NSMutableArray *users;
    NSNumber *selectedUserId;
    IBOutlet UITableView *_tableView;
}
@end

@implementation FollowingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@", [self parentViewController]);
	// Do any additional setup after loading the view, typically from a nib.
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [header setBackgroundColor:[UIColor clearColor]];
    UIImage *imageHeader = [UIImage imageNamed:@"heading-list-wip"];
    UIImageView *tableHeaderView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    tableHeaderView.image = imageHeader;
    [header addSubview:tableHeaderView];
    [_tableView setTableHeaderView:header];

    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [header setBackgroundColor:[UIColor clearColor]];
    UIImage *imageFooter = [UIImage imageNamed:@"bg-list-bottom-index"];
    UIImageView *tableFooterView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    tableFooterView.image = imageFooter;
    [footer addSubview:tableFooterView];
    [_tableView setTableFooterView:footer];

    self.navigationItem.title = NSLocalizedString(@"Followings", @"");
    
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
        [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }
    
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, 600)];
    [loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(135,140,42,42)];
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(6, 170, 308, 44)];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textAlignment = UITextAlignmentCenter;
    loadingLabel.textColor = [ColorHelper greyColor];
    loadingLabel.text = NSLocalizedString(@"Loading ...", @"");
    loadingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
    
    [loadingView addSubview:loadingLabel];
    [loadingView addSubview:anImageView];
    
    [self.view addSubview:loadingView];
    [self.view sendSubviewToBack:loadingView];
    
    [self.view insertSubview:loadingView atIndex:1];        
    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
    
    [self fetchContent];

}

-(void) fetchContent
{
    NSURL *url;
    UINavigationController *navController = [self navigationController];
    NSUInteger arraySize = [navController.viewControllers count];
    ProfileViewController* prevController = [navController.viewControllers objectAtIndex:arraySize-2];
    if ([prevController onCurrentUserProfile])
    {
        url = [RequestHelper createURL:@"/my/followings" withPrefix:@"/api"];
    } else
    {
        url = [RequestHelper createURL:[NSString stringWithFormat:@"/users/%@/followings",[prevController user_id]] withPrefix:@"/api"];
    }
    
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *feed = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        users = [[feed objectForKey:@"users"] mutableCopy];
        NSLog(@"%@", feed);
        [_tableView reloadData];
        [loadingView setHidden:YES];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
    NSDictionary *user = [users objectAtIndex:indexPath.row];
    cell.name.text = [[users objectAtIndex:indexPath.row] objectForKey:@"nickname"];
    NSURL *imageUrl = [NSURL URLWithString:[[[user objectForKey:@"avatar_url" ] objectForKey:@"thumb" ] objectForKey:@"url" ]];
    
    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];
//    UIImage *myImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
//    cell.avatar.image = myImage;
    
    cell.followButton.tag = indexPath.row;
    
    if ((BOOL)[[user objectForKey:@"already_following"]intValue])
    {
        NSLog(@"selecting : %@", user);
        [cell.followButton setSelected:YES];
    } else {
        [cell.followButton setSelected:NO];
    }
    
    if ([[user objectForKey:@"id"] isEqualToNumber:[BeechHelper currentUserId]])
    {
        cell.followButton.hidden = YES;
    } else
    {
        cell.followButton.hidden = NO;
    }
    
    [cell.followButton addTarget:self action:@selector(touchFollowButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    UserCell *cell = (UserCell*) sender;
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    NSDictionary *user = [users objectAtIndex:indexPath.row];
    if ([identifier isEqualToString:@"FollowingToProfileSegue"])
    {
        if ([[user objectForKey:@"id" ] isEqualToNumber:[BeechHelper currentUserId]])
        {
            return NO;
        } else
        {
            return YES;
        }
    } else
    {
        return YES;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59.0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"FollowingToProfileSegue"])
    {
        NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
        
        selectedUserId = [[users objectAtIndex:selectedIndexPath.row] objectForKey:@"id"];
        // Get reference to the destination view controller
        ProfileViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setUser_id:selectedUserId];
        [vc setForceRefresh:YES];
    }
}

- (void)touchFollowButton:(id)sender
{
    NSLog(@"Hello");
    NSLog(@"%d", [sender tag]);
    
    [sender setSelected:![sender isSelected]];
    
    
    NSString *httpMethod = [sender isSelected] ? @"POST" : @"DELETE";
    NSNumber *followeeId = [[users objectAtIndex:[sender tag]] objectForKey:@"id"];
    
    NSString *urlString = @"/my/followings";
    
    NSURL *url = [RequestHelper createURL:urlString withPrefix:@"/api"];
    NSDictionary *args = @{@"user_id": followeeId};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];

        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        // Change the already following attribute
        NSNumber *already_following = [NSNumber numberWithInt:0];
        if ([sender isSelected]) {
            already_following = [NSNumber numberWithInt:1];
        }
        NSMutableDictionary *user = [[users objectAtIndex:[sender tag]] mutableCopy];
        [user setValue:already_following forKey:@"already_following"];
        [users setObject:user atIndexedSubscript:[sender tag]];
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];

    [request setRequestMethod:httpMethod];
    [request startAsynchronous];
    
}

@end
