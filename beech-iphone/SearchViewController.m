//
//  SearchViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SearchViewController.h"
#import "ProfileViewController.h"
#import "UserCell.h"
#import "RequestHelper.h"
#import "ColorHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "User.h"
#import "BeechHelper.h"

@interface SearchViewController ()
{
    UIView *loadingView;
    UserCollection *users;
    UITapGestureRecognizer *tapEvent;
    IBOutlet UITableView *_tableView;
    IBOutlet UILabel *_title;
    IBOutlet UIView *_titleBackground;
    IBOutlet UITextField *textField;
    __weak IBOutlet UINavigationItem *navigationItem;
    int originalHeight;
    int keyboardHeight;
    BOOL alreadyLoaded;
}
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) onKeyboardHide:(id) sender
{
    [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, originalHeight)];
}

- (void) onKeyboardShow:(id) notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardHeight = keyboardFrameBeginRect.size.height;

    [UIView beginAnimations:@"slide down" context:nil];
    [UIView setAnimationDuration:0.3];
    
    int tbHeight = self.tabBarController.tabBar.frame.size.height;
    int height =  originalHeight - keyboardHeight + tbHeight;
    if (_titleBackground.hidden) {
        [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, height + _titleBackground.frame.size.height)];
    } else {
        [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, height)];
    }
    [UIView commitAnimations];

}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
	
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	
	[self reloadTableViewDataSource];
    [self refresh];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	
	return _reloading; // should return if data source model is reloading
	
}

-(void) refresh
{
    int current_timestamp;
    if (users == nil || [users count] == 0 )
    {
        current_timestamp = 0;
    } else
    {
        current_timestamp = [[[users objectAtIndex:0] created_at]intValue];
    }
    NSURL *url = [RequestHelper createURL: @"/users" withPrefix:@"/api"];

    NSDictionary *args = @{@"after": [NSNumber numberWithInt:current_timestamp]};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSMutableDictionary *feed = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        for (NSDictionary* user in [[feed objectForKey:@"users"] reverseObjectEnumerator] )
        {
            NSError *err = nil;
            User *u = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:user error:&err];
            [users insertObject:u atIndex:0];
        }
        [loadingView setHidden:YES];
        [_tableView reloadData];
        [self doneLoadingTableViewData];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [self doneLoadingTableViewData];
        [loadingView setHidden:YES];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}

- (void)viewDidAppear:(BOOL)animated
{
    originalHeight = _tableView.frame.size.height;
    NSLog(@"%d", originalHeight);
}

- (void) setPTR
{
    if (_refreshHeaderView == nil) {

		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, self.view.frame.size.width, _tableView.bounds.size.height)];
		view.delegate = self;
		[_tableView addSubview:view];
		_refreshHeaderView = view;
	} else {
        [_tableView addSubview:_refreshHeaderView];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    alreadyLoaded = NO;
    originalHeight = _tableView.frame.size.height;
    NSLog(@"%d", originalHeight);
    users = [[UserCollection alloc] init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [self setPTR];
    
    [_tableView setHidden:YES];
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [header setBackgroundColor:[UIColor clearColor]];
    UIImage *imageHeader = [UIImage imageNamed:@"heading-list-wip"];
    UIImageView *tableHeaderView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    tableHeaderView.image = imageHeader;
    [header addSubview:tableHeaderView];
    [_tableView setTableHeaderView:header];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [header setBackgroundColor:[UIColor clearColor]];
    UIImage *imageFooter = [UIImage imageNamed:@"bg-list-bottom-index"];
    UIImageView *tableFooterView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    tableFooterView.image = imageFooter;
    [footer addSubview:tableFooterView];
    [_tableView setTableFooterView:footer];
    
    tapEvent = [[UITapGestureRecognizer alloc]
                initWithTarget:self
                action:@selector(dismissKeyboard)];
    
    [_title setText:[NSLocalizedString(@"Last 10 rookies", @"") uppercaseString]];
    [textField setPlaceholder:NSLocalizedString(@"Touch here to search a Beecher", @"")];
    [textField setTextColor:[UIColor colorWithRed:.51 green:.51 blue:.51 alpha:1.0]];
    [textField setValue:[UIColor colorWithRed:.51 green:.51 blue:.51 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];

    self.navigationItem.title = NSLocalizedString(@"Beechers", @"");

    //    [navigationBar pushNavigationItem:item animated:NO];
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    _titleBackground.layer.masksToBounds = NO;
    _titleBackground.layer.shadowOffset = CGSizeMake(0, 2);
    _titleBackground.layer.shadowRadius = 2;
    _titleBackground.layer.shadowOpacity = 0.1;
    
    [_title setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:10.0]];
    
    [self loadContent];
    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }
    
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, 600)];
    [loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(135,210,42,42)];
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(6, 240, 308, 44)];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textAlignment = UITextAlignmentCenter;
    loadingLabel.textColor = [ColorHelper greyColor];
    loadingLabel.text = NSLocalizedString(@"Loading ...", @"");
    loadingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];
    
    [loadingView addSubview:loadingLabel];
    [loadingView addSubview:anImageView];
    
    //    [self.view insertSubview:loadingView atIndex:1];
    [self.view addSubview:loadingView];
    [self.view sendSubviewToBack:loadingView];
    
    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];

}

-(void) loadContent
{
    NSURL *url = [RequestHelper createURL:@"/users" withPrefix:@"/api"];
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSMutableDictionary *feed = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        [users addItems:[feed objectForKey:@"users"]];
        [loadingView setHidden:YES];
        [_tableView setHidden:NO];
        [_tableView reloadData];
        alreadyLoaded = YES;
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [loadingView setHidden:YES];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
    
}

- (IBAction)didEndEditi:(id)sender
{
    NSLog(@"%@", sender);
    if ([[sender text] length] == 0) {
        [_titleBackground setHidden:NO];
        CGRect newFrame = _tableView.frame;
        newFrame.size = CGSizeMake(newFrame.size.width, originalHeight);
        _tableView.frame = newFrame;
        _tableView.transform = CGAffineTransformIdentity;
        [self setPTR];
        
    } else {
        CGRect newFrame = _tableView.frame;
        newFrame.size = CGSizeMake(newFrame.size.width, originalHeight + _titleBackground.frame.size.height);
        _tableView.frame = newFrame;
    }
}

- (void) disablePTR
{
    [self reloadTableViewDataSource];
    [_refreshHeaderView setHidden:YES];
}

- (void) enablePTR
{
    [self doneLoadingTableViewData];
    [_refreshHeaderView setHidden:NO];
}

- (void)textFieldDidChange:(id) sender
{
    // Do any additional setup after loading the view.
    NSString *urlString = @"/users";
    if ([[sender text] length] > 0) {
        if (_titleBackground.hidden == NO) {
            [_titleBackground setHidden:YES];
            CGRect newFrame = _tableView.frame;
            newFrame.size = CGSizeMake(newFrame.size.width, newFrame.size.height + _titleBackground.frame.size.height);
            _tableView.frame = newFrame;
            _tableView.transform = CGAffineTransformMakeTranslation(0, -_titleBackground.frame.size.height);
            [self disablePTR];

        }
    } else {
        if (_titleBackground.hidden == YES) {
            [_titleBackground setHidden:NO];
            CGRect newFrame = _tableView.frame;
            newFrame.size = CGSizeMake(newFrame.size.width, newFrame.size.height - _titleBackground.frame.size.height);
            _tableView.frame = newFrame;
            _tableView.transform = CGAffineTransformIdentity;
            [self enablePTR];
        }
    }

    NSURL *url = [RequestHelper createURL:urlString withPrefix:@"/api"];
    NSDictionary *args = @{@"s": [textField text]};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *feed = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        [users removeAllObjects];
        
        for (id f in [feed objectForKey:@"users"])
        {
            NSError *err = nil;
            User *u = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:f error:&err];
            [users addObject:u];
        }
        [_tableView reloadData];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    _tableView = nil;
    navigationItem = nil;
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([users count] > 0) {
        [self loadContent];
    }
    [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"];
    if (cell == nil) {
        cell = [[UserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserCell"];
    }

    User *u = [users objectAtIndex:indexPath.row];
    cell.name.text = u.nickname;
    NSURL *imageUrl = [NSURL URLWithString:[BeechHelper validURLStringWithString:[[u.avatar_url objectForKey:@"thumb" ] objectForKey:@"url" ]]];

    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];

    cell.followButton.tag = indexPath.row;
    
    if ([u.already_following intValue] == 1)
    {
        [cell.followButton setSelected:YES];
    } else
    {
        [cell.followButton setSelected:NO];
    }
    
    [cell.followButton addTarget:self action:@selector(touchFollowButton:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59.0;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"SearchToProfileSegue"])
    {
        NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];
        
        NSNumber *selectedUserId = [[users objectAtIndex:selectedIndexPath.row] user_id];
        // Get reference to the destination view controller
        ProfileViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setUser_id:selectedUserId];
        [vc setForceRefresh:YES];
    }
}

- (IBAction)textFieldBeganEdit:(id)sender
{
    [_tableView addGestureRecognizer:tapEvent];
    if ([[sender text] length] > 0) {
        CGRect newFrame = _tableView.frame;
        _tableView.frame = newFrame;
    }
}

- (IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
    [_tableView removeGestureRecognizer:tapEvent];
}

- (void)dismissKeyboard
{
    [textField resignFirstResponder];
    [_tableView removeGestureRecognizer:tapEvent];
}

- (IBAction)touchFollowButton:(id)sender
{
    [sender setSelected:![sender isSelected]];
    
    NSString *httpMethod = [sender isSelected] ? @"POST" : @"DELETE";
    NSNumber *followeeId = [[users objectAtIndex:[sender tag]] user_id];
    
    NSString *urlString = @"/my/followings";
    
    NSURL *url = [RequestHelper createURL:urlString withPrefix:@"/api"];
    NSDictionary *args = @{@"user_id": followeeId};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        // Change the already following attribute
        NSNumber *already_following = [NSNumber numberWithInt:0];
        if ([sender isSelected]) {
            already_following = [NSNumber numberWithInt:1];
        }
        User *u = [users objectAtIndex:[sender tag]];
//        NSMutableDictionary *user = [[users objectAtIndex:[sender tag]] mutableCopy];
//        [user setValue:already_following forKey:@"already_following"];
        [u setAlready_following:already_following];
//        [users setObject:user atIndexedSubscript:[sender tag]];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:httpMethod];
    [request startAsynchronous];

}

@end
