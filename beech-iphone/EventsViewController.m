//
//  EventsViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/14/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "EventsViewController.h"
#import "ASIHTTPRequest.h"
#import "RequestHelper.h"
#import "CommentsViewController.h"
#import "LikesViewController.h"
#import <Social/Social.h>
#import <Twitter/Twitter.h>


@interface EventsViewController ()
{

}
@end

@implementation EventsViewController

@synthesize events, comments, awards, badges, beers, checks, likes;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.awards = [[AwardCollection alloc] init];
    self.checks = [[CheckCollection alloc] init];
    self.users = [[UserCollection alloc] init];
    self.beers = [[BeerCollection alloc] init];
    self.badges = [[BadgeCollection alloc] init];
    self.events = [[EventCollection alloc] init];
    self.likes = [[LikeCollection alloc] init];
    self.comments = [[CommentCollection alloc] init];

    self.likingEvent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (int) loadDataWithDictionary:(NSDictionary*) dict Reversed:(BOOL)reversed
{
    int numberOfEvents = 0;
    if ([dict objectForKey:@"users"]) {
        for (id u in [dict objectForKey:@"users"])
        {
            NSError *err = nil;
            User *user = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:u error:&err];
            [self.users addObject:user];
            [user saveWithDB:nil];
        }
    }

    if ([dict objectForKey:@"beers"]) {
        for (id b in [dict objectForKey:@"beers"])
        {
            NSError *err = nil;
            Beer *beer = [MTLJSONAdapter modelOfClass:Beer.class fromJSONDictionary:b error:&err];
            [self.beers addObject:beer];
        }
    }

    if ([dict objectForKey:@"badges"]) {
        for (id b in [dict objectForKey:@"badges"])
        {
            NSError *err = nil;
            Badge *badge = [MTLJSONAdapter modelOfClass:Badge.class fromJSONDictionary:b error:&err];
            [self.badges addObject:badge];
        }
    }

    if ([dict objectForKey:@"awards"]) {
        for (id a in [dict objectForKey:@"awards"])
        {
            NSError *err = nil;
            Award *award = [MTLJSONAdapter modelOfClass:Award.class fromJSONDictionary:a error:&err];
            [award setBadge:[self.badges badgeForId:award.badge_id]];
            [self.awards addObject:award];
        }
    }


    if ([dict objectForKey:@"checks"]) {
        for (id c in [dict objectForKey:@"checks"])
        {
            NSError *err = nil;
            Check *check = [MTLJSONAdapter modelOfClass:Check.class fromJSONDictionary:c error:&err];
            NSLog(@"%@", check);

            [check setBeer:[self.beers beerForId:check.beer_id]];
            [self.checks addObject:check];
        }
    }

    if ([dict objectForKey:@"feed"]) {
        for (id f in [dict objectForKey:@"feed"])
        {
            NSError *err = nil;
            Event *event = [MTLJSONAdapter modelOfClass:Event.class fromJSONDictionary:f error:&err];
            if([event isAward])
            {
                [event setAward:[self.awards awardForId: event.eventable[@"id"]]];
            } else if ([event isCheck])
            {
                [event setCheck:[self.checks checkForId: event.eventable[@"id"]]];
            } else
            {
                [NSException raise:@"Invalid foo value" format:@"foo of %@ is invalid", @"foo"];
            }
            [event setUser:[self.users userForId:event.user_id]];

            if ([self.events addObject:event]) {
                numberOfEvents += 1;
            }
        }
        // persist the last 30 events
        [self.events save:[NSNumber numberWithInt:30]];
    }
    if ([dict objectForKey:@"likes"]) {
        [self.likes removeAllObjects];
        for (id l in [dict objectForKey:@"likes"])
        {
            NSError *err = nil;
            Like *like = [MTLJSONAdapter modelOfClass:Like.class fromJSONDictionary:l error:&err];
            [self.likes addObject:like];
            [like setEvent:[self.events eventForId:like.event_id ]];
            [like setUser:[self.users userForId:like.user_id]];
        }
    }

    if ([dict objectForKey:@"comments"]) {
        for (id c in [dict objectForKey:@"comments"])
        {
            NSError *err = nil;
            Comment *comment = [MTLJSONAdapter modelOfClass:Comment.class fromJSONDictionary:c error:&err];
            [comment setEvent:[self.events eventForId:comment.event_id ]];
            [comment setUser:[self.users userForId:comment.user_id]];
            [self.comments addObject:comment];
        }
    }

    // Associate likes & comments to events
    for (Event* event in self.events) {
        [event setLikes:[self.likes likesForEventId:event.event_id]];
        [event setComments:[self.comments commentsForEventId:event.event_id]];
    }
    
    return numberOfEvents;
}

- (void)clear
{
    [self.awards removeAllObjects];
    [self.checks removeAllObjects];
    [self.users removeAllObjects];
    [self.beers removeAllObjects];
    [self.badges removeAllObjects];
    [self.events removeAllObjects];
    [self.likes removeAllObjects];
    [self.comments removeAllObjects];
}

- (void)handleCommentTap:(id)sender
{
    NSLog(@"%@", sender);
    self.selectedEventId = ((BeechCell*)sender).eventId;
    [self performSegueWithIdentifier:@"CommentsSegue" sender:self];
}

- (void)handleLikeLabelTap:(id)sender
{
    NSLog(@"%@", sender);
    self.selectedEventId = ((BeechCell*)sender).eventId;
    [self performSegueWithIdentifier:@"LikesSegue" sender:self];
}

- (void)handleMoreTap:(id)sender
{
    NSLog(@"%@", sender);
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    [actionSheet setDelegate:self];
    if (NSClassFromString(@"SLRequest") != Nil) {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Twitter", nil)];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Facebook", nil)];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Mail", nil)];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
        [actionSheet setCancelButtonIndex:3];
        [actionSheet showInView:self.view];
    } else {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Twitter", nil)];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Mail", nil)];
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
        [actionSheet setCancelButtonIndex:2];
    }
//    [actionSheet showInView:self.view];
    [actionSheet showInView:self.parentViewController.tabBarController.view];

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    if ([buttonTitle isEqualToString:@"Twitter"])
    {
        if (NSClassFromString(@"SLRequest") != Nil) {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
            {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [tweetSheet setInitialText:@"Tweeting from my own app! :)"];
                [self presentViewController:tweetSheet animated:YES completion:nil];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Sorry"
                                          message:@"You can't send a tweet right now, make sure\
                                          your device has an internet connection and you have\
                                          at least one Twitter account setup"
                                          delegate:self
                                          cancelButtonTitle:@"OK"                                       
                                          otherButtonTitles:nil];
                [alertView show];
            }
        } else {
            if ([TWTweetComposeViewController canSendTweet])
            {
                TWTweetComposeViewController *tweetSheet =
                [[TWTweetComposeViewController alloc] init];
                [tweetSheet setInitialText:
                 @"Tweeting from iOS 5 By Tutorials! :)"];
                [self presentModalViewController:tweetSheet animated:YES];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Sorry"
                                          message:@"You can't send a tweet right now, make sure\
                                          your device has an internet connection and you have\
                                          at least one Twitter account setup"
                                          delegate:self
                                          cancelButtonTitle:@"OK"                                                   
                                          otherButtonTitles:nil];
                [alertView show];
            }
        }
    } else if ([buttonTitle isEqualToString:@"Facebook"])
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [facebookSheet setInitialText:@"Beech is awesome !"];
            [self presentViewController:facebookSheet animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"You can't post to Facebook right now, make sure\
                                      your device has an internet connection and you have\
                                      at least one Facebook account setup"
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }

    } else if ([buttonTitle isEqualToString:@"Mail"])
    {
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        if ([MFMailComposeViewController canSendMail]) {
            picker.mailComposeDelegate = self;
            //        [picker setToRecipients:recipients];
            //        [picker setSubject:subject];
            [picker setMessageBody:@"Hey" isHTML:YES];
            [self presentModalViewController:picker animated:YES];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:NSLocalizedString(@"It seems you can't send mails.", @"")
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }

    }
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)handleLikeTap:(id)sender
{
    if (self.likingEvent) {
        return;
    }
    NSNumber *eventId = ((FeedCell*)sender).eventId;
    self.likingEvent = YES;
    Event *event = [self.events eventForId:eventId];
    BOOL isLiked = ((FeedCell*)sender).likeButton.selected;
    [((FeedCell*)sender).likeButton setSelected:!isLiked];

    NSString *httpMethod = [event is_liked] ? @"DELETE" : @"POST";

    NSString *urlString = [NSString stringWithFormat:@"/events/%@/likes", eventId];

    NSURL *url = [RequestHelper createURL:urlString withPrefix:@"/api"];
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        if ([request responseStatusCode] == 200 || [request responseStatusCode] == 201) {
            id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            [self.events updateEventWithId:eventId andData:json];
            // Change the already liked attribute
            Event *newEvent = [self.events eventForId:eventId];
            [((FeedCell*)sender).likeButton setSelected:[newEvent is_liked]];
            [((FeedCell*)sender) setLikeCountWithEvent:newEvent];
        } else {
            event.is_liked = !event.is_liked;
            [((FeedCell*)sender).likeButton setSelected:event.is_liked];
        }
        self.likingEvent = NO;
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        self.likingEvent = NO;
    }];

    [request setRequestMethod:httpMethod];
    [request startAsynchronous];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CommentsSegue"])
    {
        CommentsViewController *vc = [segue destinationViewController];
        Event *ev = [self.events eventForId:self.selectedEventId];
        [vc setEvent:ev];
    } else if ([[segue identifier] isEqualToString:@"LikesSegue"])
    {
        LikesViewController *vc = [segue destinationViewController];
        Event *ev = [self.events eventForId:self.selectedEventId];
        [vc setEvent:ev];
    }

}

@end
