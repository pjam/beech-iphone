//
//  LikeCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "LikeCell.h"
#import "ColorHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>

@interface LikeCell ()
{
}
@end

@implementation LikeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSLog(@"Just loaded like cell");
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];

        int yOffset = 8;
        int xOffset = 8;
        int imageWidth = 38;
        int paddingLeft = 6;
        int nameHeight = 20;
        int textWidth = 230;
        self.name = [[UILabel alloc] initWithFrame:CGRectMake(imageWidth + xOffset + paddingLeft, 17, textWidth, nameHeight)];
//        [self.name setTextColor:[ColorHelper blackColor]];
//        [self.name setFont: [UIFont fontWithName:@"ProximaNova-Bold" size:18]];
        self.name.textColor = [UIColor colorWithRed:0.20f green:0.20f blue:0.20f alpha:1.00f];
        self.name.font = [UIFont fontWithName:@"ProximaNova-Bold" size:18.0];


        self.accessoryView = [[ UIImageView alloc ] initWithImage:[UIImage imageNamed:@"list-arrow" ]];

        [self.time setHidden:YES];

        self.avatar = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset, yOffset, imageWidth, imageWidth)];
        [self.avatar setImageWithURL:[[NSURL alloc] init] placeholderImage:[UIImage imageNamed:@"avatar-4"]];
        self.avatar.layer.masksToBounds = YES;
        self.avatar.layer.cornerRadius = 3.0f;

        [self.contentView addSubview:self.name];
        [self.contentView addSubview:self.time];
        [self.contentView addSubview:self.avatar];
    }
    return self;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
