//
//  BeechCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechCell.h"
#import "ColorHelper.h"
#import <QuartzCore/QuartzCore.h>

@interface BeechCell ()
{
    UIView *cartouche;
    UIView *cartoucheBorder;
    NSMutableArray *colorPool;
    NSMutableArray *blackListedColors;
}
@end

@implementation BeechCell

@synthesize customBackgroundView;
@synthesize avatar;
@synthesize name;
@synthesize beer;
@synthesize time, delegate, eventId, likeButton, likeLabelButton, commentCountButton, moreButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];

        avatar = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 34, 34)];
        avatar.layer.masksToBounds = YES;
        avatar.layer.cornerRadius = 3.0f;

        name = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 228, 21)];
        name.backgroundColor = [UIColor clearColor];
        name.textColor = [UIColor colorWithRed:0.20f green:0.20f blue:0.20f alpha:1.00f];

        [self.contentView addSubview:customBackgroundView];
        [self.contentView addSubview:avatar];
        [self.contentView addSubview:name];
        [self.contentView addSubview:beer];

        [self.contentView sendSubviewToBack:customBackgroundView];


        colorPool = [[NSMutableArray alloc] init];
        blackListedColors = [[NSMutableArray alloc] init];

        int topOffset = 20;
        customBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(2, topOffset, 304, 190)];
        [customBackgroundView setBackgroundColor:[UIColor whiteColor]];
        [customBackgroundView.layer setBorderColor:[[UIColor colorWithRed:0.80f green:0.80f blue:0.78f alpha:1.00f] CGColor]];
        [customBackgroundView.layer setBorderWidth:1.0];
        customBackgroundView.layer.cornerRadius = 3.0;

        [customBackgroundView addSubview:self.avatar];
        [customBackgroundView addSubview:self.name];

        [self.name setTextColor:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]];
        self.name.frame = CGRectMake(48, 7, 228, 21);
        self.name.font = [UIFont fontWithName:@"ProximaNova-Bold" size:16.0];


        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 140, 304, 50)];
        [footer setBackgroundColor:[UIColor colorWithRed:0.99f green:0.99f blue:0.99f alpha:1.00f]];
        footer.layer.cornerRadius = 4.0f;

        UIView *footerBorderTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 304, 1)];
        [footerBorderTop setBackgroundColor:[UIColor colorWithRed:0.92f green:0.92f blue:0.92f alpha:1.00f]];

        int likeButtonWidth = 40;
        UIView *footerBorderLeft = [[UIView alloc] initWithFrame:CGRectMake(likeButtonWidth, 0, 1, footer.frame.size.height)];
        [footerBorderLeft setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];

        int commentCountButtonWidth = 70;
        UIView *footerBorderRight = [[UIView alloc] initWithFrame:CGRectMake(footer.frame.size.width - commentCountButtonWidth, 0, 1, footer.frame.size.height)];
        [footerBorderRight setBackgroundColor:[UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1.0]];

        UIImage *defaultImage = [UIImage imageNamed:@"widget-button-default"];
        UIImage *selectedImage = [UIImage imageNamed:@"widget-button-pressed"];
        defaultImage = [defaultImage stretchableImageWithLeftCapWidth:4.0f topCapHeight:8.0f];
        selectedImage = [selectedImage stretchableImageWithLeftCapWidth:4.0f topCapHeight:9.0f];


        likeLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [likeLabelButton setFrame:CGRectMake(43, 3 , 62, 44)];
        [likeLabelButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
        [likeLabelButton setBackgroundImage:selectedImage forState:UIControlStateHighlighted];

        [likeLabelButton setTitle:@"0 Like" forState:UIControlStateNormal];
        [likeLabelButton setTitleColor:[UIColor colorWithRed:0.40f green:0.40f blue:0.40f alpha:1.00f] forState:UIControlStateNormal];
        likeLabelButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:13.0];
        [likeLabelButton setTitleEdgeInsets:UIEdgeInsetsMake(2.0f, 0.0f, 0.0f, 0.0f)];

        [likeLabelButton addTarget:self
                       action:@selector(likesLabelTrigger:)
             forControlEvents:UIControlEventTouchUpInside];


        moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [moreButton setFrame:CGRectMake(255, 3, 40, 44)];
        [moreButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
        [moreButton setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
        [moreButton setImage:[UIImage imageNamed:@"icon-share-default"] forState:UIControlStateNormal];
        [moreButton setImage:[UIImage imageNamed:@"icon-share-pressed"] forState:UIControlStateHighlighted];

        [moreButton addTarget:self
                       action:@selector(moreEvent:)
             forControlEvents:UIControlEventTouchUpInside];


        likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        

        [likeButton addTarget:self
                       action:@selector(likeEvent:)
             forControlEvents:UIControlEventTouchUpInside];

        [likeButton setFrame:CGRectMake(10, 3 , 35, 44)];
        [likeButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
        [likeButton setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
        [likeButton setBackgroundImage:selectedImage forState:UIControlStateSelected | UIControlStateHighlighted];

        [likeButton setImage:[UIImage imageNamed:@"icon-like-default"] forState:UIControlStateNormal];
        [likeButton setImage:[UIImage imageNamed:@"icon-like-active"] forState:UIControlStateSelected];


        commentCountButton = [UIButton buttonWithType:UIButtonTypeCustom];
        commentCountButton.frame = CGRectMake(footer.frame.size.width - commentCountButtonWidth, 0, commentCountButtonWidth, 35);
        [commentCountButton setFrame:CGRectMake(113, 3 , 124, 44)];
        [commentCountButton setBackgroundColor:[UIColor colorWithRed:0.99f green:0.99f blue:0.99f alpha:1.00f]];
        [commentCountButton setImage:[UIImage imageNamed:@"icon-comment-default"] forState:UIControlStateNormal];
        [commentCountButton setImage:[UIImage imageNamed:@"icon-comment-pressed"] forState:UIControlStateSelected];
        [commentCountButton setBackgroundImage:defaultImage forState:UIControlStateNormal];
        [commentCountButton setBackgroundImage:selectedImage forState:UIControlStateHighlighted];


        [commentCountButton setTitleColor:[UIColor colorWithRed:0.40f green:0.40f blue:0.40f alpha:1.00f] forState:UIControlStateNormal];
        [commentCountButton setTitleEdgeInsets:UIEdgeInsetsMake(2.0f, 6.0f, 0.0f, 0.0f)];

        [commentCountButton setBackgroundColor:[UIColor clearColor]];
        commentCountButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:13.0];
        [commentCountButton.titleLabel setTextAlignment:NSTextAlignmentRight];
        [commentCountButton setTitle:[NSString stringWithFormat:@"0 %@", NSLocalizedString(@"Comment", nil)] forState:UIControlStateNormal];
        [commentCountButton addTarget:self
                               action:@selector(commentEvent:)
                     forControlEvents:UIControlEventTouchUpInside];

        [footer addSubview:likeButton];
        [footer addSubview:commentCountButton];
        [footer addSubview:footerBorderTop];
        [footer addSubview:likeLabelButton];
        [footer addSubview:moreButton];

        [customBackgroundView addSubview:footer];

        time = [[UILabel alloc] initWithFrame:CGRectMake(47, 25, 200, 21)];
        time.textAlignment = UITextAlignmentLeft;
        time.textColor = [UIColor colorWithRed:0.60f green:0.60f blue:0.60f alpha:1.00f];
        time.font = [UIFont systemFontOfSize:13.0];
        [time setBackgroundColor:[UIColor clearColor]];

        [customBackgroundView addSubview:time];
        [self.contentView addSubview:customBackgroundView];

    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self initContent];
    }
    return self;
}

- (void) initContent
{
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.size.width = 308.0;
    [super setFrame:frame];
}

-(void) commentEvent:(id)sender
{
    [delegate handleCommentTap:self];
}

-(void) likeEvent:(id)sender
{
    [delegate handleLikeTap:self];
}

-(void) likesLabelTrigger:(id)sender
{
    [delegate handleLikeLabelTap:self];
}

-(void) moreEvent:(id)sender
{
    [delegate handleMoreTap:self];
}

- (void)setCommentCount:(NSUInteger)commentCount
{
    [commentCountButton setTitle:[NSString stringWithFormat:@"%d %@", commentCount, NSLocalizedString(@"Comments", nil)] forState:UIControlStateNormal];
}

- (void)setLikeCountWithEvent:(Event *)event
{
    NSUInteger likeCount = [event.likes count];
    NSString *likeDescription;
    if (likeCount == 0) {
        likeDescription  = [NSString stringWithFormat:@"%d Like", likeCount];
    } else if (likeCount == 1) {
        likeDescription  = [NSString stringWithFormat:@"%d Like", likeCount];
    } else {
        likeDescription  = [NSString stringWithFormat:@"%d Likes", likeCount];
    }

    [self.likeLabelButton setTitle:likeDescription forState:UIControlStateNormal];
    [self.likeLabelButton setTitle:likeDescription forState:UIControlStateSelected];
}


@end
