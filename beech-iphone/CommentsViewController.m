//
//  CommentsViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "CommentsViewController.h"
#import "BeechHelper.h"
#import "ProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CommentCollection.h"
#import "UserCollection.h"
#import "CommentCell.h"
#import "RequestHelper.h"
#import <QuartzCore/QuartzCore.h>
#import "ColorHelper.h"

#define PADDING_TOP 35.0f
#define PADDING_BOTTOM 13.0f
#define TABBAR_HEIGHT 49

@interface CommentsViewController ()
{
    CommentCollection *comments;
    UserCollection *users;
    NSNumber *selectedUserId;
    UIFont *content_font;
    CGSize maximumLabelSize;
    int originalHeight;
    int originalChatInputSize;
    int keyboardHeight;
    BOOL sendingComment;
    UIView *emptyView;
}
@end

@implementation CommentsViewController

@synthesize event, tapEvent, _tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self._tableView setTableHeaderView:self.tableHeader];    
    sendingComment = NO;
    self._tableView.separatorColor = [UIColor colorWithRed:0.93f green:0.93f blue:0.93f alpha:1.00f];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];

    [self.navigationItem setTitle:NSLocalizedString(@"Comments", @"")];
    comments = [[CommentCollection alloc] init];
    users = [[UserCollection alloc] init];
    [self._tableView setHidden:YES];
    [self setLoadingGlobal:YES];
    [self loadContent];
    content_font = [UIFont fontWithName:@"ProximaNova-Regular" size:16];
    maximumLabelSize = CGSizeMake(260, FLT_MAX); // SHould be a constant

    _chatInput.backgroundColor = [UIColor clearColor];
    [_chatInput setBackgroundColor:[UIColor colorWithRed:0.82f green:0.82f blue:0.82f alpha:1.00f]];
	[_chatInput.sendButton setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
    [_chatInput setHidden:YES];

    tapEvent = [[UITapGestureRecognizer alloc]
                initWithTarget:self
                action:@selector(dismissKeyboard)];
    originalHeight = 0;


    emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 120, 320, 100)];
    UIImage *emptyImage = [UIImage imageNamed:@"icon-comment-empty"];
    UIImageView *emptyIcon = [[UIImageView alloc] initWithImage:emptyImage];
    emptyIcon.frame = CGRectMake(124.5, 0, 76, 71);

    UILabel *emptyText = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 320, 50)];
    emptyText.text = NSLocalizedString(@"Come on, just one comment, please !", nil);
    emptyText.textAlignment = UITextAlignmentCenter;
    [emptyText setBackgroundColor:[UIColor clearColor]];
    [emptyText setTextColor:[UIColor colorWithRed:0.56f green:0.56f blue:0.56f alpha:1.00f]];
    [emptyText setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:16.0]];

    [emptyView addSubview:emptyIcon];
    [emptyView addSubview:emptyText];
    emptyView.hidden = YES;

    [self.view insertSubview:emptyView atIndex:1];
}

-(void) setLoadingGlobal:(BOOL)glob
{
    // Set loading
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }

    if (glob) {
        self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 130, 308, [_tableView tableHeaderView].frame.size.height)];
        [self.view addSubview:self.loadingView];
    } else {
        self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, [_tableView tableHeaderView].frame.size.height)];
        [_tableView.tableHeaderView addSubview:self.loadingView];
    }

    [self.loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width / 2) - 21, 10,42,42)];
    [self.loadingView addSubview:anImageView];

    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"View did appear");
    _chatInput.frame = CGRectMake(0, self.view.frame.size.height - _chatInput.frame.size.height, 320, _chatInput.frame.size.height);
    [_chatInput setHidden:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"View will appear");
}

- (void) dismissKeyboard
{
    [[self.chatInput textView] resignFirstResponder];
    [_tableView removeGestureRecognizer:tapEvent];
}

- (void)enableTapEvent
{
    [[self _tableView] addGestureRecognizer:tapEvent];
}

- (void)disableTapEvent
{
    [[self _tableView] removeGestureRecognizer:tapEvent];
}

- (void) goToLastRow
{
    if ([comments count] > 0) {
        NSIndexPath* ip = [NSIndexPath indexPathForRow:[comments count] - 1 inSection:0];
        [_tableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

- (void)loadContent
{
    NSURL *url = [RequestHelper createURL:[NSString stringWithFormat:@"/events/%@/comments", event.event_id] withPrefix:@"/api"];

    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        NSNumber *total = [[json objectForKey:@"meta"] objectForKey:@"total"];
        [self loadDataFromDictionary:json];
        if ([total intValue] > [comments count]) {
        } else
        {
            [_tableView setTableHeaderView:nil];
        }

        [self._tableView setHidden:NO];
        [self._tableView reloadData];
        [self goToLastRow];
        [self.loadingView setHidden:YES];
        if ([comments count] == 0) {
            [emptyView setHidden:NO];
        }
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [self.loadingView setHidden:YES];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
    
}

- (void) refresh
{

    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO];
    NSArray *sortedArray=[comments sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];

    NSNumber *timestamp = [[sortedArray lastObject] created_at];
    NSURL *url = [RequestHelper createURL:[NSString stringWithFormat:@"/events/%@/comments?before=%@", event.event_id, timestamp] withPrefix:@"/api"];

    NSDictionary *args = @{@"before": timestamp};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [self.refreshButton setHidden:YES];
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        [self loadDataFromDictionary:json];
        [_tableView setTableHeaderView:nil];
        [self._tableView reloadData];
        [self.loadingView setHidden:YES];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
    

}

- (void) loadDataFromDictionary:(NSDictionary*) dict
{
    NSLog(@"%@", dict);
    for (NSDictionary* u in [dict objectForKey:@"users"]) {
        NSError *err = nil;
        User *user = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:u error:&err];

        [users addObject:user];
    }


    for (NSDictionary* c in [dict objectForKey:@"comments"]) {
        NSError *err = nil;
        Comment *comment = [MTLJSONAdapter modelOfClass:Comment.class fromJSONDictionary:c error:&err];
        [comment setUser:[users userForId:comment.user_id]];
        [comments addObject:comment];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    self._tableView = nil;
    _chatInput = nil;
    [self set_tableView:nil];
    [self setChatInput:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Comment *comment = [comments objectAtIndex:indexPath.row];
    
    CGSize expectedLabelSize = [comment.content sizeWithFont:content_font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    return expectedLabelSize.height + PADDING_BOTTOM + PADDING_TOP;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [comments count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell"];

    Comment *comment = [comments objectAtIndex:indexPath.row];

    [cell.name setText:comment.user.nickname];
    [cell.desc setText:comment.content];
    [cell computeDescLabelHeight];
    NSURL *imageUrl = [NSURL URLWithString:[[comment.user.avatar_url objectForKey:@"thumb"] objectForKey:@"url"]];
    //    NSURL *imageUrl = [NSURL URLWithString:[[[user objectForKey:@"avatar_url" ] objectForKey:@"thumb" ] objectForKey:@"url" ]];

    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];

    cell.time.text = [BeechHelper stringForTimestamp:comment.created_at serverTime:[NSDate date]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedUserId = [[comments objectAtIndex:indexPath.row] user_id];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];

    if ([[defaults valueForKey:@"user_id"] isEqualToNumber:selectedUserId])
    {
        // GO to my profile
        self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
    } else
    {
        [self performSegueWithIdentifier:@"CommentProfileSegue" sender:self];
    }

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CommentProfileSegue"])
    {
        // Get reference to the destination view controller
        ProfileViewController *vc = [segue destinationViewController];

        NSLog(@"Hei : %@", vc);
        // Pass any objects to the view controller here, like...
        Comment *comment = [comments objectAtIndex:[[self._tableView indexPathForSelectedRow] row]];
        [vc setUser_id:comment.user_id];
    }
}

- (void) returnButtonPressed:(id)sender {

//    _textView.text = [sender text];
    [self dismissKeyboard];
}

- (void) onKeyboardHide:(id) sender
{
    [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, originalHeight - (self.chatInput.frame.size.height - originalChatInputSize))];
//    [self.chatInput keyboardDidDisappear];
}

- (void) onKeyboardShow:(id) notification
{
    if (originalHeight == 0) {
        // As I have some issues with viewWillAppear, I use htis little hack to initialize original heights only once
        originalHeight = self._tableView.frame.size.height;
        originalChatInputSize = self.chatInput.frame.size.height;
        [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, originalHeight - originalChatInputSize)];
    }

    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardHeight = keyboardFrameBeginRect.size.height;

    [UIView animateWithDuration:0.3f
                     animations:^{
                         // Slightly buggy so far
                         NSLog(@"%d", originalHeight);
                         NSLog(@"%d", keyboardHeight);
                         NSLog(@"%f", self.chatInput.frame.size.height);
                         int delta = originalHeight - (keyboardHeight - self.chatInput.frame.size.height) + 5;
                         NSLog(@"%d", delta);
                         [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, delta)];

//                         [self goToLastRow];
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                         }
                     }];
//    [self.chatInput keyboardDidAppear];

}

- (void) textViewDidBeginEditing:(id) sender
{
    [self enableTapEvent];
    [[self chatInput] setKeyboardHeight:keyboardHeight];
}

- (void) textViewDidEndEditing:(id) sender
{
    [self disableTapEvent];
    [[self chatInput] setKeyboardHeight:keyboardHeight];    
}

- (void) textViewDidChangeFrameSize:(NSNumber*) delta
{
//    CGPoint bottomOffset = CGPointMake(0, self._tableView.contentSize.height - self._tableView.bounds.size.height);
//    [self._tableView setContentOffset:bottomOffset animated:YES];
    [self goToLastRow];
    [_tableView setFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y, _tableView.frame.size.width, _tableView.frame.size.height - [delta floatValue])];
}

- (void) textViewDidEndSlindingUp:(UITextView*) textView
{
    NSLog(@"Did enf sliding up");
    if (_tableView.contentSize.height > _tableView.frame.size.height) {
        NSLog(@"Should slide down");
        [self goToLastRow];
    }
}

- (void) sendButtonPressed:(NSDictionary*)args
{
    NSLog(@"%@", args);
    if ([args[@"text"] length] > 0) {
        [self sendComment: args[@"text"]];
    }
}

-(void) sendComment:(NSString*) text
{
    if (sendingComment) {
        return;
    }
    NSLog(@"%@", text);
    NSLog(@"Should do a request");
    NSNumber *eventId = event.event_id;
    sendingComment = YES;
    _chatInput.sendingComment.hidden = NO;
    _chatInput.sendButton.hidden = YES;

    NSString *urlString = [NSString stringWithFormat:@"/events/%@/comments", eventId];

    NSURL *url = [RequestHelper createURL:urlString withPrefix:@"/api"];
    ASIFormDataRequest *_request = [RequestHelper prepareFormRequestWithUrl:url withParams:@{}];
    __weak ASIFormDataRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];

        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
        // Change the already following attribute
        if ([request responseStatusCode] == 201 || [request responseStatusCode] == 200) {
            // Create new comment
            NSError *err = nil;
            User *user = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:[json objectForKey:@"users"][0] error:&err];
            Comment *comment = [MTLJSONAdapter modelOfClass:Comment.class fromJSONDictionary:[json objectForKey:@"comment"] error:&err];
            [comment setUser:user];
            [comments addObject:comment];
            [self._tableView reloadData];
        }
        _chatInput.sendingComment.hidden = YES;
        _chatInput.sendButton.hidden = NO;
        sendingComment = NO;
        //    _textView.text = _chatInput.textView.text;

        _chatInput.textView.text = @"";
        [_chatInput fitText];
        [self.view sendSubviewToBack:emptyView];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        sendingComment = NO;
        // Send alert
    }];

    [request setRequestMethod:@"POST"];
    [request setPostValue:text forKey:@"comment[content]"];
    [request startAsynchronous];
}

- (IBAction)refreshButtonTapped:(id)sender {
    NSLog(@"Tapped");
    [self setLoadingGlobal:NO];
    [self refresh];
}

@end
