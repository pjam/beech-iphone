//
//  BeechHelper.h
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

@interface BeechHelper : NSObject
+ (NSString *)stringForTimestamp:(NSNumber *)timestamp serverTime:(NSDate *)serverDateTime;
+ (NSNumber *) currentUserId;
+ (NSString *) validURLStringWithString:(NSString *) string;
+ (void) redirectToLoginScreenWithRequest:(ASIHTTPRequest*)request andTabController:(UIViewController*)controller;

@end
