//
//  FirstViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "FeedViewController.h"
#import "ProfileViewController.h"
#import "BeechTabBarViewController.h"
#import "CommentsViewController.h"
#import "LikesViewController.h"
#import "FeedCell.h"
#import "BadgeCell.h"
#import "BeechHelper.h"
#import "RequestHelper.h"
#import "ColorHelper.h"
#import "ASIHTTPRequest.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "Badge.h"
#import "BadgeCollection.h"
#import "Event.h"
#import "EventCollection.h"
#import "Check.h"
#import "CheckCollection.h"
#import "Award.h"
#import "AwardCollection.h"
#import "Beer.h"
#import "BeerCollection.h"
#import "User.h"
#import "UserCollection.h"
#import "Like.h"
#import "LikeCollection.h"
#import "Comment.h"
#import "CommentCollection.h"
#import "UIColor+HexString.h"
#import "UIBarButtonItem+StyledButton.h"


@interface FeedViewController ()
{
    UIView *loadingView;
    IBOutlet UITableView *_tableView;
    UIButton *notificationButton;
    NSNumber *selectedUserId;
    BOOL alreadyLoaded;
    UIView *placeHolder;
    UIView *footer;
}
@end

@implementation FeedViewController

@synthesize loadingAnteriorData;

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
	
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	
	[self reloadTableViewDataSource];
    [self refresh:self after:YES];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	
	return _reloading; // should return if data source model is reloading
	
}

-(void) reloadTableData
{
    // call to reload your data
    [self refresh:self after:YES];
    [_tableView reloadData];
//    [pull finishedLoading];
}

- (void) setRegularFooter
{
    footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [_tableView setTableFooterView:footer];

}

- (void) setLoadingFooter
{
    footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 59)];

    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader-grey-bg@2x-%d (dragged).tiff", i]] ];
    }

    loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 59)];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(135,8,42,42)];
    
    [loadingView addSubview:anImageView];
    
    [footer addSubview:loadingView];
    
    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
    
    [_tableView setTableFooterView:footer];
}

- (void)viewDidLoad
{
//    self.navigationController.delegate = se;

    [super viewDidLoad];


    [self setLoadingAnteriorData:NO];
    alreadyLoaded = NO;
    [self.view setBackgroundColor:[UIColor colorWithRed:0.88f green:0.88f blue:0.85f alpha:1.00f]];
    
    if (_refreshHeaderView == nil) {
		
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _tableView.bounds.size.height, self.view.frame.size.width, _tableView.bounds.size.height)];
		view.delegate = self;
		[_tableView addSubview:view];
		_refreshHeaderView = view;
	}
    
    
	// Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadContent) name:@"login-success" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clear) name:@"logout-success" object:nil];

    [self setRegularFooter];
    notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *notifButtonImage = [[UIImage imageNamed:@"navbar-button-basic-default"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
//    UIImage *backBtnImagePressed = [UIImage imageNamed:@"btn-back-pressed"];
    [notificationButton setBackgroundImage:notifButtonImage forState:UIControlStateNormal];

//    [backBtn setBackgroundImage:backBtnImagePressed forState:UIControlStateHighlighted];
    [notificationButton addTarget:self action:@selector(notificationButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    notificationButton.frame = CGRectMake(2, 20, 44, 44);

    UIImage *icon = [UIImage imageNamed:@"icon-notification-default"];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem styledBackBarButtonItemWithTarget:self selector:@selector(notificationButtonPressed) icon:icon];


    [_tableView setHidden:YES];
}

- (void) notificationButtonPressed
{
    [self performSegueWithIdentifier:@"NotificationSegue" sender:self];
}

- (void) loadContentFromCache
{
    FMDatabase *db = [SqliteDB DB];
    [db open];

    FMResultSet *results = [db executeQuery:@"SELECT events.id AS event_id, users.id AS user_id, color_pattern, beer_name, beer_id, event_type, is_liked, badge_name, badge_description, badge_image_url, badge_image_url_thumb, events.created_at, users.nickname, users.avatar_url FROM events INNER JOIN users on events.user_id = users.id"];

    while([results next]) {
//        NSLog(@"%@", [results columnNameToIndexMap]);
        NSString *eventId = [results stringForColumn:@"event_id"];
        NSString *userId = [results stringForColumn:@"user_id"];
//        NSString *beerName = [results stringForColumn:@"beer_name"];
        NSString *beerId = [results stringForColumn:@"beer_id"];
        NSString *eventType = [results stringForColumn:@"event_type"];
        NSString *badgeDescription = [results stringForColumn:@"badge_description"];
        NSString *badgeName = [results stringForColumn:@"badge_name"];
        NSString *badgePhotoUrl = [results stringForColumn:@"badge_image_url"];
        NSString *badgePhotoUrlThumb = [results stringForColumn:@"badge_image_url_thumb"];
        NSString *createdAt = [results stringForColumn:@"created_at"];
        NSString *avatartUrl = [results stringForColumn:@"avatar_url"];
        NSString *userNickname = [results stringForColumn:@"nickname"];
        NSString *isLiked = [results stringForColumn:@"is_liked"];

        Event *event = [[Event alloc] init];
        User *user = [[User alloc] init];
        event.event_id = (NSNumber*)eventId;
        event.user = user;
        event.user_id = user.user_id;
        event.created_at = [NSNumber numberWithInt:[createdAt intValue]];
        event.is_liked = [isLiked boolValue];
        user.user_id = (NSNumber*)userId;
        user.nickname = userNickname;
        user.avatar_url = @{@"thumb": @{@"url": avatartUrl}};
        NSDictionary *eventable;
        if ([eventType isEqualToString:@"check"]) {
            Check *check = [[Check alloc] init];
            Beer *beer = [Beer find:[NSNumber numberWithInt:[beerId intValue]] withDB:db];
//            beer.name = beerName;
//            beer.beer_id = ;
            check.beer = beer;
            event.check = check;
            eventable = @{@"type": @"check"};
        } else {
            Award *award = [[Award alloc] init];
            Badge *badge = [[Badge alloc] init];
            badge.name = badgeName;
            badge.description = badgeDescription;
            badge.photo_url = @{
                                @"url": badgePhotoUrl,
                                @"thumb": @{
                                        @"url": badgePhotoUrlThumb
                                        }
                                };
            award.badge = badge;
            event.award = award;
            eventable = @{@"type": @"award"};            
        }
        event.eventable = eventable;
        [self.events addObject:event];
    }
    [db close];
    [_tableView reloadData];
    [loadingView setHidden:YES];
    [_tableView setHidden:NO];
    alreadyLoaded = YES;
    // Might need to display empty here
}

-(void)loadContent
{
    // Successful login
    [BeerCollection syncWithServer];

    NSURL *url = [RequestHelper createURL:@"/my/feed" withPrefix:@"/api"];

    ASIHTTPRequest *request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    [request setRequestMethod:@"GET"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [request setUsername:[defaults stringForKey:@"username"]];
    [request setPassword:[defaults stringForKey:@"password"]];
    [request startAsynchronous];
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        [self loadDataWithDictionary:json Reversed:NO];

        [loadingView setHidden:YES];
        [_tableView setHidden:NO];
        alreadyLoaded = YES;
        [_tableView reloadData];
        if ([self.events count] == 0)
        {
            [_tableView setHidden:YES];
            placeHolder = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, self.view.frame.size.height)];
            [placeHolder setBackgroundColor:[UIColor whiteColor]];
            [self.view addSubview:placeHolder];
            int ycoord = self.view.frame.size.height - 35;
            UILabel *topLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, ycoord - 20, 100, 44)];
            topLabel.text = NSLocalizedString(@"I beech here !", @"");
            topLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:14.0];
            topLabel.textColor = [UIColor colorWithRed:48/255.0 green:48/255.0 blue:48/255.0 alpha:1];
            UIImage *arrowImage = [UIImage imageNamed:@"help-empty-arrow"];
            UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
            arrowImageView.frame = CGRectMake(25, ycoord, arrowImageView.frame.size.width, arrowImageView.frame.size.height);
            
            UIImage *placeholderImage = [UIImage imageNamed:@"help-empty-icon"];
            UIImageView *placeholderImageView = [[UIImageView alloc] initWithImage:placeholderImage];
            placeholderImageView.frame = CGRectMake(139, (self.view.frame.size.height / 2) - 55, 42, 42 );
            UILabel *middleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, (self.view.frame.size.height / 2) - 35, 230, 70)];
            middleLabel.text = NSLocalizedString(@"You haven't beeched anything yet.\nWhyyyyy ??!!", @"");
            middleLabel.numberOfLines = 2;
            middleLabel.textAlignment = UITextAlignmentCenter;
            middleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:14.0];
            middleLabel.textColor = [UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1];
            
            [placeHolder addSubview:arrowImageView];
            [placeHolder addSubview:topLabel];
            [placeHolder addSubview:middleLabel];
            [placeHolder addSubview:placeholderImageView];
        } else
        {
            [_tableView setHidden:NO];
            [placeHolder setHidden:YES];
        }
    }];

    [request setFailedBlock:^{
        NSLog(@"failed with error: %d", [request responseStatusCode]);
        // tell user incorrect username/password
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
        [loadingView setHidden:YES];
    }];


}

-(void) viewDidAppear:(BOOL)animated
{
    NSLog(@"view did appear");

    BeechCell *cell = (BeechCell*)[_tableView cellForRowAtIndexPath:[_tableView indexPathForSelectedRow]];
    [cell setSelected:NO animated:YES];
    [super viewDidAppear:animated];    
}

- (void)viewDidUnload
{
    _tableView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"view will appear");

    if(alreadyLoaded)
    {
        [self refresh:self after:NO];
        [self setLoadingAnteriorData:NO];        
    } else
    {
        [self loadContentFromCache];
        [self loadContent];
    }
    [super viewWillAppear:animated];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.events count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *FeedIdentifier = @"FeedCell";
    static NSString *BadgeIdentifier = @"BadgeCell";

    BeechCell *cell;
    
    UIImage *tappedImage = [UIImage imageNamed:@"bg-list-index-pressed"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:tappedImage];
    
    Event *event = [self.events objectAtIndex:indexPath.row];

    if ([event isAward]) {
        cell = (BadgeCell*)[tableView dequeueReusableCellWithIdentifier:BadgeIdentifier];

        if (cell == nil) {
            cell = [[BadgeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:BadgeIdentifier];
            [[[cell subviews] objectAtIndex:0] setTag:111];
        }
        ((BadgeCell*)cell).delegate = self;
        ((BadgeCell*)cell).eventId = event.event_id;

        cell.beer.text = NSLocalizedString(@"Just earned a badge", @"");

        NSLog(@"badgeName : %@", ((BadgeCell*)cell).badgeDesc );
        [((BadgeCell*)cell).badgeName setText:event.award.badge.name];
        ((BadgeCell*)cell).badgeDesc.text = event.award.badge.description;
        ((BadgeCell*)cell).badgeCount.text = [NSString stringWithFormat:@"BADGE #%@", event.award.badge.badge_id];

        NSString *imageStringUrl = [BeechHelper validURLStringWithString:event.award.badge.photo_url[@"url"]];
        NSURL *badgeImageUrl = [NSURL URLWithString:imageStringUrl];

        [((BadgeCell*)cell).badgeImageView setImageWithURL:badgeImageUrl placeholderImage:[UIImage imageNamed:@"profile-badge-empty"]];

    } else if ([event isCheck])
    {
        cell = (FeedCell*)[tableView dequeueReusableCellWithIdentifier:FeedIdentifier];

        if (cell == nil) {
            cell = [[FeedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FeedIdentifier];
            [[[cell subviews] objectAtIndex:0] setTag:111];
        }
        
        ((FeedCell*)cell).delegate = self;
        ((FeedCell*)cell).eventId = event.event_id;
        ((FeedCell*)cell).colorPattern = [NSNumber numberWithInt:[event.check.beer_id intValue] % 3];
        cell.beer.text = event.check.beer.name;

        // Set like button state

        ((FeedCell*)cell).beer.text = event.check.beer.name;
        [((FeedCell*)cell) setDetailsWithBeer:event.check.beer];
        [((FeedCell*)cell).beer setTextColor:[UIColor colorWithHexString:event.check.beer.font_color]];
        [((FeedCell*)cell).cartouche setBackgroundColor:[UIColor colorWithHexString:event.check.beer.background_color]];
    }

    if ([event is_liked]) {
        [cell.likeButton setSelected:YES];
    } else
    {
        [cell.likeButton setSelected:NO];
    }

    // Set comments
    [cell setCommentCount:[[event comments] count]];

    // Set likes
    [cell setLikeCountWithEvent:event];



    cell.name.text = event.user.nickname;

    NSString *imageStringUrl = [BeechHelper validURLStringWithString:[[event.user.avatar_url objectForKey:@"thumb"] objectForKey:@"url"]];

    NSURL *imageUrl = [NSURL URLWithString:imageStringUrl];

    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];

    cell.time.text = [BeechHelper stringForTimestamp:event.created_at serverTime:[NSDate date]];


    if (indexPath.row == [self.events count] - 1)
    {
        NSLog(@"should reload");
        if ([self loadingAnteriorData] == NO){
            [self setLoadingAnteriorData:YES];
            // last item
            NSLog(@"feed : %@", self.events);
            NSLog(@"feed : %d", [[[self.events lastObject] created_at] intValue]);
            [self setLoadingFooter];
            [self loadDataAfter: [[[self.events lastObject] created_at] intValue]];
        }
    }

    return cell;
}

- (void)loadDataAfter: (int)timestamp
{
    // TODO : Stop loading if connection fails
    NSURL *url = [RequestHelper createURL: @"/my/feed" withPrefix:@"/api"];

    NSDictionary *args = @{@"before": [NSNumber numberWithInt:timestamp]};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        NSArray *jsonChecks = [json objectForKey:@"checks"];
        NSArray *jsonAwards = [json objectForKey:@"awards"];

        if ((jsonChecks && [jsonChecks count] > 0) || (jsonAwards && [jsonAwards count] > 0))
        {
            [self loadDataWithDictionary:json Reversed:NO];
            
            [_tableView reloadData];
            [self setLoadingAnteriorData:NO];

        }
        [loadingView setHidden:YES];
        [self setRegularFooter];        
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
        [self setLoadingAnteriorData:NO];
        [self setRegularFooter];
    }];
    [request setRequestMethod:@"GET"];
    
    [request startAsynchronous];
}

- (void) switchToProfile
{
    self.tabBarController.selectedViewController = [self.tabBarController.viewControllers objectAtIndex:1];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    Event *event = [self.events objectAtIndex:indexPath.row];
    selectedUserId = [NSNumber numberWithInt:[event.user.user_id intValue]];

    if ([[defaults valueForKey:@"user_id"] isEqualToNumber:selectedUserId])
    {
        // GO to my profile
        [self performSelector:@selector(switchToProfile) withObject:nil afterDelay:0.15];
    } else
    {
        // Push a new profile view for the user.
//        self.navigationController
        [self performSegueWithIdentifier:@"UserProfileSegue" sender:self];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"UserProfileSegue"])
    {
        // Get reference to the destination view controller
        ProfileViewController *vc = [segue destinationViewController];
        
        NSLog(@"Hei : %@", vc);
        // Pass any objects to the view controller here, like...
        [vc setUser_id:selectedUserId];
    }
}

- (void)refresh:(id)sender after:(BOOL) after
{
    NSURL *url;
    ASIHTTPRequest *_request;
    if (after)
    {
        int current_timestamp;
        if (self.events == nil || [self.events count] == 0 )
        {
            current_timestamp = 0;
        } else
        {
            current_timestamp = [[[self.events objectAtIndex:0] created_at] intValue];
        }
        url = [RequestHelper createURL: @"/my/feed" withPrefix:@"/api"];
        _request = [RequestHelper prepareRequestWithUrl:url withParams:@{@"after": [NSNumber numberWithInt:current_timestamp]}];
    } else
    {
        url = [RequestHelper createURL: @"/my/feed" withPrefix:@"/api"];
        _request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    }


//    ASIHTTPRequest *_request = [ASIHTTPRequest requestWithURL:url];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        NSArray *jsonChecks = [json objectForKey:@"checks"];
        NSArray *jsonAwards = [json objectForKey:@"awards"];
        if ((jsonChecks && [jsonChecks count] > 0) || (jsonAwards && [jsonAwards count] > 0))
        {

            // if after is yes, then we want to insert all new data at the top, otherwise, no
            int change = [self loadDataWithDictionary:json Reversed:after];
            if (change > 0) {
                [UIView transitionWithView: _tableView
                                  duration: 0.5f
                                   options: UIViewAnimationOptionTransitionCrossDissolve
                                animations: ^(void)
                 {
                     NSLog(@"%@", self.events);
                     [_tableView reloadData];
                 }
                                completion: ^(BOOL isFinished)
                 {
                     /* TODO: Whatever you want here */
                 }];
            } else {
                [_tableView reloadData];
            }
        }
        
        [loadingView setHidden:YES];
        [self doneLoadingTableViewData];
        if ([self.events count] > 0)
        {
            [_tableView setHidden:NO];
            [placeHolder setHidden:YES];
        }
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", request);
        NSLog(@"%@", error);
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
        [loadingView setHidden:YES];
        [self doneLoadingTableViewData];
    }];
    [request setRequestMethod:@"GET"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"Accept" value:@"application/json"];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [request setUsername:[defaults stringForKey:@"username"]];
    [request setPassword:[defaults stringForKey:@"password"]];

    [request startAsynchronous];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.events count] - 1) {
        return 230.0;
    } else {
        return 210.0;
    }
}

- (void) clear
{
    [super clear];
    [_tableView reloadData];
    [self setLoadingAnteriorData:NO];    
}

@end
