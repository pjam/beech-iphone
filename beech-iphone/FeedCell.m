//
//  FeedCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "FeedCell.h"
#import <QuartzCore/QuartzCore.h>


@implementation FeedCell

@synthesize beer;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.beer = [[UILabel alloc] initWithFrame:CGRectMake(6, 13, 296, 40)];
        self.beer.backgroundColor = [UIColor clearColor];
        self.beer.textColor = [UIColor whiteColor];
        self.beer.textAlignment = UITextAlignmentCenter;
        self.beer.font = [UIFont fontWithName:@"ProximaNova-Light" size:28.0];
        self.beer.adjustsFontSizeToFitWidth = YES;

        self.beerDetails = [[UILabel alloc] initWithFrame:CGRectMake(6, 47, 296, 20)];
        self.beerDetails.backgroundColor = [UIColor clearColor];
        self.beerDetails.textColor = [UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.7];
        self.beerDetails.textAlignment = UITextAlignmentCenter;
        self.beerDetails.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:14.0];

        self.cartouche = [[UIView alloc] initWithFrame:CGRectMake(0, 69, 308, 84)];
        [self.cartouche setBackgroundColor:[UIColor colorWithRed:231/255.0 green:81/255.0 blue:94/255.0 alpha:1.0]];
        [self.cartouche addSubview:self.beer];
        [self.cartouche addSubview:self.beerDetails];
        [self.contentView addSubview:self.cartouche];
    }
    return self;
}

- (void)setDetailsWithBeer:(Beer *)abeer
{
    // if there's not detail, vertical align the beer name
    [self.beerDetails setText:abeer.country];
    if (abeer.country && [abeer.country length] > 0) {
        self.beer.frame = CGRectMake(6, 13, 296, 40);
    } else {
        self.beer.frame = CGRectMake(6, 22, 296, 40);
    }


}

@end
