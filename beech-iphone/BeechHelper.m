//
//  BeechHelper.m
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechHelper.h"
#import "AppDelegate.h"

@implementation BeechHelper

+ (NSString *)stringForTimestamp:(NSNumber *)timestamp serverTime:(NSDate *)serverDateTime{

    NSDate *dateTime = [NSDate dateWithTimeIntervalSince1970: [timestamp doubleValue]];

    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = abs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    
    if(interval >= 86400)
        
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%i%@", DayInterval, NSLocalizedString(@"Day abbreviation", nil)];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%i%@", DayInterval, NSLocalizedString(@"Day abbreviation", nil)];
                }
                else {
                    return [NSString stringWithFormat:@"%i%@", DayInterval, NSLocalizedString(@"Day abbreviation", nil)];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%i%@", DayInterval, NSLocalizedString(@"Day abbreviation", nil)];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%ih", HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%im", MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"%is", interval];
        }
        
    }
    
}

+ (NSNumber *)currentUserId
{
    // Store the id here once user is logged in, or maybe use UserDefaults
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults valueForKey:@"user_id"];
}

+ (NSString *) validURLStringWithString:(NSString *)string
{
    if ([string hasPrefix:@"http"])
    {
        return string;
    } else
    {
        return [NSString stringWithFormat:@"%@/%@", kServerURL, string];
    }
}

+ (void) redirectToLoginScreenWithRequest:(id)request andTabController:(UIViewController *)controller
{
    if ([request responseStatusCode] == 401)
    {
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [controller performSegueWithIdentifier:@"LoginSegue" sender:controller];
    }    
}

@end
