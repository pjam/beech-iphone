//
//  Event.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Event.h"
#import "SqliteDB.h"

@implementation Event
@synthesize event_id, user_id, created_at, eventable, check, award, user, is_liked, comments, likes;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"event_id": @"id",
             @"user_id": @"user_id",
             @"is_liked": @"is_liked",
             @"created_at": @"created_at",
             @"eventable": @"eventable",
             };
}

- (BOOL)isEqualToEvent:(Event *)event {
    return event.event_id == self.event_id;
}

- (BOOL)isCheck
{
    return [[[self eventable] objectForKey:@"type"] isEqualToString:@"check"];
}

- (BOOL)isAward
{
    return [[[self eventable] objectForKey:@"type"] isEqualToString:@"award"];    
}

- (NSString *)eventType
{
    return [[self eventable] objectForKey:@"type"];
}

- (BOOL)saveWithDB:(FMDatabase *)db
{
    BOOL dbWasNil = (db == nil);
    if (dbWasNil) {
        db = [SqliteDB DB];
        [db open];
    }
    NSDictionary *argsDict = @{@"event_id": self.event_id};
    FMResultSet *results = [db executeQuery:@"SELECT * FROM events WHERE id = :event_id" withParameterDictionary:argsDict];

    Event *eventInDb = [[Event alloc] init];
    while([results next]) {
        eventInDb.event_id = (NSNumber*)[results stringForColumn:@"id"];
        eventInDb.is_liked = [[results stringForColumn:@"is_liked"] boolValue];
    }

    // User might have changed, to try to save it
    if(eventInDb.event_id > 0) {
        // Should we update the existing recode ?
//        NSLog(@"%@", eventInDb);
        if (self.is_liked != eventInDb.is_liked) {
            [db executeUpdate:@"UPDATE events SET is_liked = ? where id = ?", [NSNumber numberWithBool:self.is_liked ], self.event_id];
        }
        return NO;
    } else {
        NSDictionary *argsDict;
        if ([self isCheck]) {
            argsDict = @{@"event_id": self.event_id,
                         @"event_type": self.eventType,
                         @"beer_name": self.check.beer.name,
                         @"badge_name": @"",
                         @"badge_description": @"",
                         @"badge_image_url": @"",
                         @"badge_image_url_thumb": @"",
                         @"created_at": self.created_at,
                         @"user_id": self.user.user_id,
                         @"beer_id": self.check.beer_id,
                         @"is_liked": [NSNumber numberWithBool: self.is_liked]};
        } else {
            NSString *badgeDesc = self.award.badge.description;
            NSString *badgeName = self.award.badge.name;
            if (!badgeDesc) {
                badgeDesc = @"";
            }
            if (!badgeName) {
                badgeName = @"";
            }
            argsDict = @{@"event_id": self.event_id,
                         @"event_type": self.eventType,
                         @"beer_name": @"",
                         @"badge_name": badgeName,
                         @"badge_description": badgeDesc,
                         @"badge_image_url": self.award.badge.photo_url[@"url"],
                         @"badge_image_url_thumb": self.award.badge.photo_url[@"thumb"][@"url"],
                         @"created_at": self.created_at,
                         @"user_id": self.user.user_id,
                         @"beer_id": [NSNumber numberWithInt:0],
                         @"is_liked": [NSNumber numberWithBool: self.is_liked]};
        }
        [self.user saveWithDB:db];
        [db executeUpdate:@"INSERT INTO events (id, event_type, beer_name, beer_id, badge_name, badge_description, badge_image_url, badge_image_url_thumb, created_at, user_id, is_liked) VALUES (:event_id, :event_type, :beer_name, :beer_id, :badge_name, :badge_description, :badge_image_url, :badge_image_url_thumb, :created_at, :user_id, :is_liked)" withParameterDictionary:argsDict];
        return YES;
    }
    if (dbWasNil) {
        [db close];
    }
}

- (void)setUser:(User *)auser
{
    self->user = auser;
    self.user_id = auser.user_id;
}
@end
