//
//  UIButton+StyledButton.m
//  beech-iphone
//
//  Created by Pierre Jambet on 5/5/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "UIButton+StyledButton.h"

@implementation UIButton (StyledButton)

+ (UIButton *)styledButtonWithBackgroundImage:(UIImage *)image imageHighlighted:(UIImage *)imageHighlighted icon:(UIImage *)iconImage font:(UIFont *)font title:(NSString *)title target:(id)target selector:(SEL)selector
{
    CGSize textSize = [title sizeWithFont:font];
    CGSize buttonSize;
    if (title == nil) {
        buttonSize = CGSizeMake(36, 33);
    } else {
        textSize = CGSizeMake(33, textSize.height);
        buttonSize = CGSizeMake(textSize.width + 28, 33);
    }


    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, buttonSize.width, buttonSize.height)];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:imageHighlighted forState:UIControlStateHighlighted];

    UIImageView *icon = [[UIImageView alloc] initWithImage:iconImage];
    icon.frame = CGRectMake(1.5, 0, 33, 33);
    [button addSubview:icon];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:font];

    return button;
}

@end
