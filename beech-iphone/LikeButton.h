//
//  LikeButton.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/14/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeButton : UIButton
-(id) initLikeButton;
@end
