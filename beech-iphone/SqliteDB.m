//
//  SqliteDB.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/20/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SqliteDB.h"

@implementation SqliteDB


+ (FMDatabase *)DB
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"%@", paths);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *path = [docsPath stringByAppendingPathComponent:@"database.sqlite"];

    FMDatabase *db = [FMDatabase databaseWithPath:path];
    db.logsErrors = YES;
    db.traceExecution = YES;
    db.crashOnErrors = YES;
    return db;
}

+ (void) createDB
{
    FMDatabase *database = [self DB];
    [database open];
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS users(id int primary key, nickname text, avatar_url text);"];
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS events(id int primary key, event_type text, beer_name text, beer_id int, badge_name text, badge_description text, badge_image_url text, badge_image_url_thumb text, created_at int, user_id int, is_liked int, color_pattern int);"];
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS beers(id int primary key, name text collate nocase, country text, beer_color_id int, added_by_id int, font_color text, background_color text);"];
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS beer_colors(id int primary key, name text)"];

    // TODO table profile
    
    [database close];
}

+ (void)clearDB
{
    FMDatabase *database = [self DB];
    [database open];
    [database executeUpdate:@"DROP TABLE users;"];
    [database executeUpdate:@"DROP TABLE events;"];
    [database executeUpdate:@"DROP TABLE beers;"];
    [database executeUpdate:@"DROP TABLE beer_colors;"];
    // TODO table profile

    [database close];
}

@end
