//
//  BeechCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"
#import "LikeButton.h"
#import "Event.h"

@protocol FeedCellDelegate <NSObject>
@required
-(void) handleCommentTap:(id)sender;
-(void) handleLikeTap:(id)sender;
-(void) handleLikeLabelTap:(id)sender;
-(void) handleMoreTap:(id)sender;
@end

@interface BeechCell : UITableViewCell
{
    __weak id <FeedCellDelegate> delegate;
}
@property(nonatomic) UIView *customBackgroundView;
@property(nonatomic) UIImageView *avatar;
@property(nonatomic) UILabel *name;
@property(atomic) UILabel *beer;
@property(nonatomic) UILabel *time;
@property (nonatomic, weak) id <FeedCellDelegate> delegate;
@property (nonatomic) NSNumber *eventId;
@property (nonatomic) NSNumber *colorPattern;
@property (nonatomic) UIButton *moreButton;
@property (nonatomic) UIButton *likeButton;
@property (nonatomic) UIButton *likeLabelButton;
@property (nonatomic) UIButton *commentCountButton;
@property (nonatomic) UIView *cartouche;

-(void) setLikeCountWithEvent:(Event*) event;
-(void) setCommentCount:(NSUInteger)commentCount;
-(void) initContent;
@end
