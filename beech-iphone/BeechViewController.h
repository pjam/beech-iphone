//
//  BeechSegueViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 11/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeechViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
- (void)dismissKeyboard;
- (UITableView*)tableView;
@end
