//
//  BadgeCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Badge.h"

@interface BadgeCollection : Collection
-(Badge*) badgeForId:(NSNumber*) badge_id;
@end
