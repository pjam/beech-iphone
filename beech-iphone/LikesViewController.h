//
//  LikesViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "EventDetailViewController.h"

@interface LikesViewController : EventDetailViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic) Event *event;
@end
