//
//  EventCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Event.h"

@interface EventCollection : Collection
- (void) sort;
- (Event*) eventForId:(NSNumber*) event_id;
- (void) save:(NSNumber*) limit;
- (Event*) objectAtIndex:(NSUInteger)index;
- (void) updateEventWithId:(NSNumber*)eventId andData:(NSDictionary*)jsonDict;
@end
