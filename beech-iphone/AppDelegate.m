//
//  AppDelegate.m
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "AppDelegate.h"
#import "ASIHTTPRequest.h"
#import <Crashlytics/Crashlytics.h>
#import "Flurry.h"
#import "SqliteDB.h"
#import "BeerCollection.h"

@interface AppDelegate ()
{
    BOOL activatedFromBackground;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    activatedFromBackground = NO;
    [Flurry startSession:@"VDSZY8RBFTXFCQ2SRQ6X"];
    [Crashlytics startWithAPIKey:@"b549bdc975ea41263f3a4203987954840a290c95"];    
    // Override point for customization after application launch.
    [[UIApplication sharedApplication]
     setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:NO];
    
    [ASIHTTPRequest setDefaultTimeOutSeconds:30];
   
    UIImage *backButtonImage = [UIImage imageNamed:@"navbar-button-back-default"];
    backButtonImage = [backButtonImage stretchableImageWithLeftCapWidth:14.0f topCapHeight:0.0f];

    UIImage *backButtonImageActive = [UIImage imageNamed:@"navbar-button-back-active"];
    backButtonImageActive = [backButtonImageActive stretchableImageWithLeftCapWidth:14.0f topCapHeight:0.0f];

    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButtonImageActive forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];

//    UIImage *buttonImage = [[UIImage imageNamed:@"navbar-button-cancel"]
//                            resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 2, 5)];
//    buttonImage = [UIImage imageNamed:@"user-button-follow"];
//    [[UIBarButtonItem appearance] setBackgroundImage:buttonImage forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    [[UIBarButtonItem appearance] setBackButtonBackgroundVerticalPositionAdjustment:10.0 forBarMetrics: UIBarMetricsDefault];

    [[UIBarButtonItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0], UITextAttributeTextShadowColor,
      [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0], UITextAttributeFont, nil] forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0], UITextAttributeTextShadowColor,
      [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0], UITextAttributeFont, nil] forState:UIControlStateHighlighted];

//    [[[UIBarButtonItem appearance] titleLabel] setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:14.0]];

    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], UITextAttributeTextColor,
      [UIFont fontWithName:@"ProximaNova-Light" size:22.0], UITextAttributeFont,
      [UIColor clearColor], UITextAttributeTextShadowColor,nil]];

    [[UINavigationBar appearance] setTitleVerticalPositionAdjustment:1.0f forBarMetrics:UIBarMetricsDefault];

//    [[UILabel appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//    [UIFont fontWithName:@"ProximaNova-Semibold" size:14.0], UITextAttributeFont,
//    nil]];

//    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"Transparent.png"]];

    [SqliteDB createDB];
    return YES;
}


							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    activatedFromBackground = YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if (activatedFromBackground) {
        [BeerCollection syncWithServer];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    // Should go back to login
    return YES;
}

@end
