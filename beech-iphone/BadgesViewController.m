//
//  BadgesViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BadgesViewController.h"
#import "ASIHTTPRequest.h"
#import "ProfileViewController.h"
#import "RequestHelper.h"
#import "BadgeDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BeechTabBarViewController.h"

@interface BadgesViewController ()
{
    __weak IBOutlet UIScrollView *_scrollView;
    NSMutableArray *badges;
    UIView *placeHolder;
}
@end

@implementation BadgesViewController

@synthesize badgesViews;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    badgesViews = [[NSMutableArray alloc] init];
	// Do any additional setup after loading the view.

    self.navigationItem.title = @"Badges";
//    BeechTabBarViewController *tabBar = [[self parentViewController] parentViewController];
    _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, self.view.frame.size.height - 95);

    [self loadBadges];
}

- (void) loadBadges
{
    NSURL *url;
    NSUInteger arraySize = [self.navigationController.viewControllers count];
    ProfileViewController* prevController = [self.navigationController.viewControllers objectAtIndex:arraySize-2];
    if ([prevController onCurrentUserProfile])
    {
        url = [RequestHelper createURL:@"/my/badges" withPrefix:@"/api"];
    } else
    {
        url = [RequestHelper createURL:[NSString stringWithFormat:@"/users/%@/badges",[prevController user_id]] withPrefix:@"/api"];
    }
    
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSMutableDictionary *feed = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        badges = [[feed objectForKey:@"badges"] mutableCopy];

        //        [loadingView setHidden:YES];
        if ([badges count] == 0)
        {
            [_scrollView setHidden:YES];
            placeHolder = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, self.view.frame.size.height)];
            [placeHolder setBackgroundColor:[UIColor whiteColor]];
            [self.view addSubview:placeHolder];
            
            UIImage *placeholderImage = [UIImage imageNamed:@"help-empty-icon"];
            UIImageView *placeholderImageView = [[UIImageView alloc] initWithImage:placeholderImage];
            placeholderImageView.frame = CGRectMake(139, (self.view.frame.size.height / 2) - 55, 42, 42 );
            UILabel *middleLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, (self.view.frame.size.height / 2) - 35, 210, 70)];
            middleLabel.text = NSLocalizedString(@"You didn't earn any badges yet.", @"No badges yet");
            middleLabel.numberOfLines = 2;
            middleLabel.textAlignment = UITextAlignmentCenter;
            middleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:14.0];
            middleLabel.textColor = [UIColor colorWithRed:109/255.0 green:109/255.0 blue:109/255.0 alpha:1];
            
            [placeHolder addSubview:middleLabel];
            [placeHolder addSubview:placeholderImageView];
        } else
        {
            [_scrollView setHidden:NO];
            [placeHolder setHidden:YES];
            int i = 0;
            for(NSDictionary* badge in badges) {
                BadgeDetailViewController *controller = [[BadgeDetailViewController alloc] initWithNothing];
                
                [controller setDelegate:self];
                // add the controller's view to the scroll view
                if (controller.view.superview == nil)
                {
                    NSString *backgroundImageName;
                    CGRect frame = _scrollView.frame;
                    int xOffset = 0;
                    if (i % 2 == 0)
                    {
                        xOffset = 0;
                        backgroundImageName = @"bg-list-badge-left";
                        // If the element is the last, there would be a blank space to fill
                        // next to it
                        if (i == [badges count] - 1){
                            UIImage *stubImage = [UIImage imageNamed:@"bg-list-badge-right"];
                            UIImageView *stubBackground = [[UIImageView alloc] initWithImage:stubImage];
                            stubBackground.frame = CGRectMake(154.0, (floor(i / 2)) * 177, 154, 177);
                            [_scrollView addSubview:stubBackground];
                        }
                    } else
                    {
                        xOffset = 154;
                        [controller.rightBorder setBackgroundColor:[UIColor clearColor]];
                        backgroundImageName = @"bg-list-badge-right";
                    }
                    controller.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:backgroundImageName]];
                    frame.origin.x = xOffset;
                    frame.origin.y = (floor(i / 2)) * 177;
                    frame.size.height = 177;
                    frame.size.width = 154;
                    
                    controller.view.frame = frame;
                    controller.badgeName.text = [badge objectForKey:@"name"];
                    controller.badgeIndex.text = [NSString stringWithFormat:@"BADGE #%@", [badge objectForKey:@"id"]];
                    NSString *description = [badge objectForKey:@"description"];
                    if (description != [NSNull null]) {
                        controller.badgeDescription.text = description;
                    }
                    NSURL *badgeImageUrl = [NSURL URLWithString:[[badge objectForKey:@"photo_url" ] objectForKey:@"url" ]];
                    
                    [controller.badgeImage setImageWithURL:badgeImageUrl placeholderImage:[UIImage imageNamed:@"profile-badge-empty"]];

                    [badgesViews addObject:controller];
                    [_scrollView addSubview:controller.view];
                }
                i++;
            }
            _scrollView.contentSize = CGSizeMake(308, ceil(i/2.0) * 177);
            
            UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, -9, 308, 9)];
            [header setBackgroundColor:[UIColor clearColor]];
            UIImage *imageHeader = [UIImage imageNamed:@"heading-list-wip"];
            UIImageView *tableHeaderView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
            tableHeaderView.image = imageHeader;
            [header addSubview:tableHeaderView];
            
            [_scrollView addSubview:header];
            
            UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, ceil(i/2.0) * 177, 308, 9)];
            [header setBackgroundColor:[UIColor clearColor]];
            UIImage *imageFooter = [UIImage imageNamed:@"bg-list-bottom-index"];
            UIImageView *tableFooterView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
            tableFooterView.image = imageFooter;
            [footer addSubview:tableFooterView];
            [_scrollView insertSubview:footer atIndex:1];
            
            //        _scrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-list-badge"]];
            

        }
        
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)doSomething:(id)sender
{
    //code for doing what you want your button to do.
}

- (void)viewDidUnload {
    _scrollView = nil;
    [super viewDidUnload];
}
@end
