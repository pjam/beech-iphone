//
//  BeerDetailCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 16/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeerDetailCell : UITableViewCell
@property(nonatomic) UILabel *name;
@property(nonatomic) UILabel *count;
- (void) setCountColor:(UIColor*) color;
+(UIColor*) blueFontColor;
+(UIColor*) greyFontColor;
@end
