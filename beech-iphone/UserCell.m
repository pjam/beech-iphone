//
//  UserCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "UserCell.h"
#import <QuartzCore/QuartzCore.h>

@interface UserCell ()

@end

@implementation UserCell
@synthesize name;
@synthesize followButton;
@synthesize avatar;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSLog(@"foo");
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code

        avatar = [[UIImageView alloc] initWithFrame:CGRectMake(11, 11, 34, 34)];
        avatar.layer.masksToBounds = YES;
        avatar.layer.cornerRadius = 3.0f;

        UIImage *img = [UIImage imageNamed:@"bg-list-index.png"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
        
        [self.contentView insertSubview:imageView atIndex:0];
        
        name = [[UILabel alloc] initWithFrame:CGRectMake(55, 20, 228, 21)];
        name.backgroundColor = [UIColor clearColor];
        name.textColor = [UIColor colorWithRed:0.20 green:0.45 blue:0.72 alpha:1];
        name.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:18.0];
        
        followButton = [[FollowButton alloc] initWithOffsetX: 10];

        [self.contentView addSubview:avatar];
        [self.contentView addSubview:name];
        [self.contentView addSubview:followButton];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)buttonClick:(id) sender
{
    NSLog(@"buttonClick");
}

@end
