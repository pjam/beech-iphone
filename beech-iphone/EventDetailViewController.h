//
//  EventDetailViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/19/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UIViewController
@property UIView *tableHeader;
@property UIView *loadingView;
@property UIButton *refreshButton;
@end
