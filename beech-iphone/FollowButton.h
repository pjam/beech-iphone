//
//  FollowButton.h
//  beech-iphone
//
//  Created by Pierre Jambet on 26/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowButton : UIButton
-(id) initWithOffsetX:(int) offset;
@property (assign) int offsetX;
@property (assign) int offsetY;
@end
