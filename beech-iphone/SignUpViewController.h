//
//  SignUpViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 19/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController <UITextFieldDelegate>

@end
