//
//  Collection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"

@implementation Collection

@synthesize collection;

-(id) init
{
    self = [super init];
    if (self)
    {
        self.collection = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)initWithCollection:(NSMutableArray *)col
{
    self = [self init];
    if (self) {
        [self setCollection:col];
    }
    return self;
}

- (NSUInteger)count
{
    return [collection count];
}

- (id)lastObject
{
    return [self.collection lastObject];
}

- (BOOL)addObject:(id)anObject
{
    [self.collection addObject:anObject];
    return YES;
}

- (id)objectAtIndex:(NSUInteger)index
{
    return [self.collection objectAtIndex:index];
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index
{
    [self.collection insertObject:anObject atIndex:index];
}

- (void)removeAllObjects
{
    [self.collection removeAllObjects];
}
- (void)addItems:(NSArray *)items
{
    [NSException raise:@"Not implemented yet" format:@""];
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len
{
    return [self.collection countByEnumeratingWithState:state objects:buffer count:len];
}

- (NSArray *)sortedArrayUsingDescriptors:(NSArray *)sortDescriptors
{
    return [self.collection sortedArrayUsingDescriptors:sortDescriptors];
}
@end
