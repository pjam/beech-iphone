//
//  BeechSegueViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 11/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechViewController.h"
#import "BeerCell.h"
#import "RequestHelper.h"
#import "ColorHelper.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "BeerCollection.h"
#import <QuartzCore/QuartzCore.h>
#import <Crashlytics/Crashlytics.h>
#import "UIBarButtonItem+StyledButton.h"
#import "SearchDelegateViewController.h"

static NSString *letters = @"#abcdefghijklmnopqrstuvwxyz";

@interface BeechViewController ()
{
    UILabel *beerName;
    UILabel *beechCount;
    UIImageView *bg;
    UIBarButtonItem *beechButton;
    UIBarButtonItem *suggestButton;
    NSMutableArray *content;
    NSArray *searchResults;
    
    BeerCollection *beers;
    NSArray *allBeers;
    UIView *modalView;
    UIView *loadingView;
    UIView *wrapper;
    UINavigationBar *subNavBar;
    UIView *modalViewContent;
    UITapGestureRecognizer *tapEvent;
    UITextField *beerNameTextField;
    __weak IBOutlet UIView *titleBanner;
    IBOutlet UITableView *_tableView;
    IBOutlet UINavigationBar *_navigationBar;
    IBOutlet UINavigationItem *_navigationTitle;
    IBOutlet UITextField *textField;
    __weak IBOutlet UIBarButtonItem *backButton;
    __weak IBOutlet UILabel *_title;
    __weak IBOutlet UITextField *searchWrapper;
    BOOL keyboardIsVisible;
    int keyboardHeight;
    int originalHeight;
    UITableView *searchTableView;
    SearchDelegateViewController *searchDelegate;
//    __weak IBOutlet UIBarButtonItem *suggestButton;
}
@end

@implementation BeechViewController 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) onKeyboardHide:(id) sender
{
    keyboardIsVisible = NO;
    [UIView beginAnimations:@"slide up" context:nil];
    [UIView setAnimationDuration:0.3];
    wrapper.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
    [_tableView setFrame:CGRectMake(0, _tableView.frame.origin.y, 320, originalHeight)];
}

- (void) onKeyboardShow:(id) notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardHeight = keyboardFrameBeginRect.size.height;

    keyboardIsVisible = YES;
    [UIView beginAnimations:@"slide down" context:nil];
    [UIView setAnimationDuration:0.3];
    wrapper.transform = CGAffineTransformMakeTranslation(0, -70);
    [_tableView setFrame:CGRectMake(0, _tableView.frame.origin.y, 320, originalHeight - keyboardHeight)];
    [UIView commitAnimations];


}

- (void)viewDidLoad
{
    // TODO : Ping the server to see if new beers should be downloaded

    searchTableView = [[UITableView alloc] initWithFrame:_tableView.frame style:UITableViewStylePlain];
    [searchTableView setHidden:YES];
    searchDelegate = [[SearchDelegateViewController alloc] init];
    searchDelegate.parent = self;
    searchTableView.delegate = searchDelegate;
    searchTableView.dataSource = searchDelegate;
    [searchTableView setTableHeaderView:nil];
    [self.view addSubview:searchTableView];

    UIImage *clearImage = [UIImage imageNamed:@"search-button-close"];
    UIImageView *clearButton = [[UIImageView alloc] initWithImage:clearImage];
//    textField.rightView = clearButton;
//    textField.rightViewMode = UITextFieldViewModeAlways;

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [_tableView setSeparatorColor:[UIColor colorWithRed:0.97f green:0.97f blue:0.97f alpha:1.00f]];
    [super viewDidLoad];
    originalHeight = _tableView.frame.size.height;    
    [_title setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:10.0]];
    tapEvent = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
//    [backButton setTitle:NSLocalizedString(@"Cancel", @"")];
    _navigationTitle.leftBarButtonItem = [UIBarButtonItem styledCancelBarButtonItemWithTarget:self selector:@selector(done:)];

    [textField setPlaceholder:NSLocalizedString(@"Touch here to search", @"")];
    [textField setTextColor:[UIColor colorWithRed:.51 green:.51 blue:.51 alpha:1.0]];
    [textField setValue:[UIColor colorWithRed:.51 green:.51 blue:.51 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    
    if ([_navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [_navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    _navigationTitle.title = NSLocalizedString(@"A brewski ?", @"Which beer ?");
    
    
    CGFloat myWidth = 22.0f;
    CGFloat myHeight = 22.0f;
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, myWidth, myHeight)];
    [button setImage:[UIImage imageNamed:@"search-button-close"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"search-button-close"] forState:UIControlStateHighlighted];
    [button setBackgroundColor:[UIColor redColor]];
    
    beers = [[BeerCollection alloc] init];
    [beers loadFromDB];
    allBeers = beers.collection;
    [self prePopulateTableViewContent];

    [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    // Setting suggest button
    UIButton *topSuggestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *suggestButtonImage = [UIImage imageNamed:@"navbar-button-suggest"];
    //    UIImage *backBtnImagePressed = [UIImage imageNamed:@"btn-back-pressed"];
    [topSuggestButton setBackgroundImage:suggestButtonImage forState:UIControlStateNormal];
    //    [backBtn setBackgroundImage:backBtnImagePressed forState:UIControlStateHighlighted];
    [topSuggestButton addTarget:self action:@selector(showSuggest) forControlEvents:UIControlEventTouchUpInside];
    topSuggestButton.frame = CGRectMake(7, 20, 44, 44);
    UIView *suggestButtonView = [[UIView alloc] initWithFrame:CGRectMake(2, 20, 44, 44)];
    suggestButtonView.bounds = CGRectOffset(suggestButtonView.bounds, 2, 20);
    [suggestButtonView addSubview:topSuggestButton];
    UIBarButtonItem *suggestBarButton = [[UIBarButtonItem alloc] initWithCustomView:suggestButtonView];
    _navigationTitle.rightBarButtonItem = suggestBarButton;
    [self.view addSubview:suggestButtonView];
    
    
}

- (void) showSuggest
{
    wrapper = [[UIView alloc] initWithFrame:CGRectMake(20, 120, 280, 144)];
    
    modalView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    subNavBar = [[UINavigationBar alloc]
                 initWithFrame:CGRectMake(0, 0, 280, 44)];

    
    modalView.opaque = NO;
    modalView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8f];

    UIBarButtonItem *closeButton = [UIBarButtonItem styledCancelBarButtonItemWithTarget:self selector:@selector(closeModal)];
    suggestButton = [UIBarButtonItem styledSubmitBarButtonItemWithTitle:NSLocalizedString(@"Send", @"") target:self selector:@selector(performSuggest:)];

    
    UINavigationItem *item = [[UINavigationItem alloc] init];
    [item setTitle:NSLocalizedString(@"Suggestion", @"")];
    
    item.leftBarButtonItem = closeButton;
    item.rightBarButtonItem = suggestButton;
    [suggestButton setEnabled:NO];
    
    
    subNavBar.items = [NSArray arrayWithObject:item];
    [wrapper addSubview:subNavBar];
    
    if ([subNavBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [subNavBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    modalViewContent = [[UIView alloc] initWithFrame:CGRectMake(0, 44, 280, 100)];
    modalViewContent.backgroundColor = [UIColor whiteColor];
    
    beerNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 20, 250, 44)];
    beerNameTextField.placeholder = NSLocalizedString(@"Type a beer's name", @"");
    beerNameTextField.font = [UIFont fontWithName:@"ProximaNova-Regular" size:20.0];
    beerNameTextField.backgroundColor = [UIColor clearColor];
    beerNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [beerNameTextField addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [beerNameTextField addTarget:self action:@selector(textFieldBeganEdit:) forControlEvents:UIControlEventEditingDidBegin];
    [beerNameTextField addTarget:self action:@selector(beerNameTFDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [beerNameTextField becomeFirstResponder];
    [beerNameTextField setText:[textField text]];
    if ([[beerNameTextField text] length] > 0) {
        [suggestButton setEnabled:YES];
    }

    
    UILabel *disclaimer = [[UILabel alloc] initWithFrame:CGRectMake(15, 58, 250, 30)];
    disclaimer.text = NSLocalizedString(@"Suggestions will be validated before they are published. Thank you for you help !", @"");
    disclaimer.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:11.0];
    disclaimer.textColor = [UIColor colorWithRed:134.0/256.0 green:134.0/256.0 blue:134.0/256.0 alpha:1];
    disclaimer.backgroundColor = [UIColor clearColor];
    disclaimer.adjustsFontSizeToFitWidth = NO;
    disclaimer.numberOfLines = 2;
    disclaimer.textAlignment = UITextAlignmentLeft;
    
    
    UIView *sep = [[UIView alloc] initWithFrame:CGRectMake(15, 50, 250, 1)];
    [sep setBackgroundColor:[UIColor colorWithRed:186/255.0 green:186/255.0 blue:186/255.0 alpha:1]];
    
    [modalViewContent addSubview:beerNameTextField];
    [modalViewContent addSubview:sep];
    [modalViewContent addSubview:disclaimer];
    
    
    wrapper.layer.masksToBounds = YES;
    wrapper.layer.cornerRadius = 7;
    //    wrapper.layer.shadowOffset = CGSizeMake(0, 10);
    //    wrapper.layer.shadowRadius = 1;
    //    wrapper.layer.shadowOpacity = 0.3;
    
    
    [modalView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
    subNavBar.alpha = 0;
    modalViewContent.alpha = 0;
    
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.3];
    [modalView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    modalViewContent.alpha = 1;
    subNavBar.alpha = 1;
    [UIView commitAnimations];
    
    [wrapper addSubview:modalViewContent];
    [self.view addSubview:modalView];
    [self.view addSubview:wrapper];
    if (keyboardIsVisible)
    {
        wrapper.transform = CGAffineTransformMakeTranslation(0, -70);
    }

    // pre fill text field

}

- (void) prePopulateTableViewContent
{
    content = [[NSMutableArray alloc] init];
    for (int i = 0; i < [letters length]; i++ ) {
        NSMutableDictionary *row = [[NSMutableDictionary alloc] init];
        NSString *ichar  = [NSString stringWithFormat:@"%c", [letters characterAtIndex:i]];
        NSArray *words = [beers beersStartingWith:ichar];
        if ([words count] > 0) {
            [row setValue:[ichar uppercaseString] forKey:@"headerTitle"];
            [row setValue:words forKey:@"rowValues"];
            [content addObject:row];
        }
    }
}

- (void)beerNameTFDidChange:(id)sender
{
    if ([sender text].length > 0 )
    {
        [suggestButton setEnabled:YES];
    } else
    {
        [suggestButton setEnabled:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clearContent
{
    [textField setText:@""];
    [self textFieldDidChange:self];
}

- (IBAction)done:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return [content valueForKey:@"headerTitle"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [content count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [content[section][@"rowValues"] count];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [[content valueForKey:@"headerTitle"] indexOfObject:title];
}

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section
{
    return [[content valueForKey:@"headerTitle"] objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 24.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 24)];
    UIImage *bgImage = [UIImage imageNamed:@"list-heading-bg"];
    UIImageView *background = [[UIImageView alloc] initWithImage:bgImage];
    [background setFrame:CGRectMake(0, -1, tableView.frame.size.width, 24)];
    [header addSubview:background];
    UILabel *headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(10 , 0, tableView.frame.size.width, header.frame.size.height)];
    [headerTitle setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:12.0]];
    [headerTitle setTextColor:[UIColor colorWithRed:0.50f green:0.50f blue:0.50f alpha:1.00f]];
    [headerTitle setBackgroundColor:[UIColor clearColor]];
    [headerTitle setText:[self tableView:tableView titleForHeaderInSection:section]];
    [header addSubview:headerTitle];
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BeerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BeerCell"];
    if (cell == nil) {
        cell = [[BeerCell alloc] init];
    }

    cell.name.text = [[content objectAtIndex:indexPath.section][@"rowValues"][indexPath.row] name];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    wrapper = [[UIView alloc] initWithFrame:CGRectMake(20, 120, 280, 144)];
    
    modalView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    subNavBar = [[UINavigationBar alloc]
                                initWithFrame:CGRectMake(0, 0, 280, 44)];
    
    modalView.opaque = NO;
    modalView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8f];
    
    UIBarButtonItem *closeButton = [UIBarButtonItem styledCancelBarButtonItemWithTarget:self selector:@selector(closeModal)];

    beechButton = [UIBarButtonItem styledSubmitBarButtonItemWithTitle:NSLocalizedString(@"Beech !", @"") target:self selector:@selector(performBeech:)];
    
    UINavigationItem *item = [[UINavigationItem alloc] init];
    
    item.leftBarButtonItem = closeButton;
    item.rightBarButtonItem = beechButton;

    subNavBar.items = [NSArray arrayWithObject:item];
    subNavBar.topItem.title = @"Beech it !";
    [wrapper addSubview:subNavBar];

    if ([subNavBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [subNavBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }

    modalViewContent = [[UIView alloc] initWithFrame:CGRectMake(0, 44, 280, 100)];
    modalViewContent.backgroundColor = [UIColor whiteColor];

    Beer* beer = [content objectAtIndex:indexPath.section][@"rowValues"][indexPath.row];
    beerName = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, 280, 20)];
    beerName.text = [beer name];
    beerName.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:22.0];
    beerName.backgroundColor = [UIColor clearColor];
    beerName.textAlignment = UITextAlignmentCenter;

    beechCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, 280, 20)];
    beechCount.text = [NSString stringWithFormat:@"BEECH #%@", beer.beer_id];
    beechCount.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0];
    beechCount.textColor = [UIColor colorWithRed:134.0/256.0 green:134.0/256.0 blue:134.0/256.0 alpha:1];
    beechCount.backgroundColor = [UIColor clearColor];
    beechCount.adjustsFontSizeToFitWidth = YES;
    beechCount.textAlignment = UITextAlignmentCenter;
    
    UIImage *image = [UIImage imageNamed:@"modal-bg"];
    bg = [[UIImageView alloc] initWithImage:image];
    bg.frame = CGRectMake(112, 10, 56, 76);
    
    [modalViewContent addSubview:bg]; 
    [modalViewContent addSubview:beerName];
    [modalViewContent addSubview:beechCount];

    
    wrapper.layer.masksToBounds = YES;
    wrapper.layer.cornerRadius = 7;
//    wrapper.layer.shadowOffset = CGSizeMake(0, 10);
//    wrapper.layer.shadowRadius = 1;
//    wrapper.layer.shadowOpacity = 0.3;
    
    
    [modalView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0]];
    subNavBar.alpha = 0;
    modalViewContent.alpha = 0;
    
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.3];
    [modalView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.8]];
    modalViewContent.alpha = 1;
    subNavBar.alpha = 1;
    [UIView commitAnimations];

    [wrapper addSubview:modalViewContent];
    [self.view addSubview:modalView];
    [self.view addSubview:wrapper];
}

- (void)textFieldDidChange:(id) sender
{
//    [beers search:[textField text]];
//    [self prePopulateTableViewContent];
    NSString *text = [sender text];
    if ([text length] > 0) {
        [searchTableView setHidden:NO];
        NSIndexSet *indices = [allBeers indexesOfObjectsPassingTest:^BOOL(id beer, NSUInteger idx,BOOL *stop)
         {
             return [((Beer*)beer).name rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound;
         }];
        searchDelegate.searchResults = [allBeers objectsAtIndexes:indices];

    } else {
        [searchTableView setHidden:YES];
    }
    [searchTableView reloadData];
}

- (void) fadeOut
{
    [modalView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.7]];
    subNavBar.alpha = 1;
    modalViewContent.alpha = 1;
    
    [UIView beginAnimations:@"fade out" context:nil];
    [UIView setAnimationDuration:0.3];
    [modalView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.0]];
    modalViewContent.alpha = 0;
    subNavBar.alpha = 0;
    SEL theSelector = NSSelectorFromString(@"removeViews");
    NSInvocation *anInvocation = [NSInvocation
                                  invocationWithMethodSignature:
                                  [BeechViewController instanceMethodSignatureForSelector:theSelector]];
    
    [anInvocation setSelector:theSelector];
    [anInvocation setTarget:self];
    
    [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.3];
    [UIView commitAnimations];

}

- (void) removeViews
{
    [modalView removeFromSuperview];
    [wrapper removeFromSuperview];
    [self.view endEditing:YES];
}

- (void) closeModal
{
    [self fadeOut];
}

- (void) performBeech:(id)sender
{
    [bg setHidden:YES];
    [beechCount setHidden:YES];
    [beerName setHidden:YES];
    [beechButton setEnabled:NO];
    
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }
    
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, 600)];
    [loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(119,30,42,42)];
    [loadingView addSubview:anImageView];
    
    //    [self.view insertSubview:loadingView atIndex:1];
    [modalViewContent addSubview:loadingView];
//    [self.view sendSubviewToBack:loadingView];
    
    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
    
    // POST request
    NSLog(@"%@", sender);
    NSIndexPath *ip = [_tableView indexPathForSelectedRow];
    Beer *beer = [content objectAtIndex:ip.section][@"rowValues"][ip.row];
//    NSDictionary *postDictionary = @{@"check": @{
//    @"beer_id": }};
    
//    NSData *postData = [NSJSONSerialization dataWithJSONObject:postDictionary options:kNilOptions error:nil];

    NSURL *url = [RequestHelper createURL:@"/checks" withPrefix:@"/api"];
    ASIFormDataRequest *_request = [RequestHelper prepareFormRequestWithUrl:url withParams:@{}];
    __weak ASIFormDataRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@",json);
        //         [modalView removeFromSuperview];
        //         [wrapper removeFromSuperview];
//        [self fadeOut];
        SEL theSelector = NSSelectorFromString(@"dismissModalViewControllerAnimated:");
        NSInvocation *anInvocation = [NSInvocation
                                      invocationWithMethodSignature:
                                      [BeechViewController instanceMethodSignatureForSelector:theSelector]];
        
        [anInvocation setSelector:theSelector];
        [anInvocation setTarget:self];
        BOOL anim = YES;
        [anInvocation setArgument:&anim atIndex:2];
        
        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.3];
        //         [self dismissModalViewControllerAnimated:YES];

        
    }];
    [request setFailedBlock:^{
        [self fadeOut];
        
        SEL theSelector = NSSelectorFromString(@"failAction");
        NSInvocation *anInvocation = [NSInvocation
                                      invocationWithMethodSignature:
                                      [BeechViewController instanceMethodSignatureForSelector:theSelector]];
        
        [anInvocation setSelector:theSelector];
        [anInvocation setTarget:self];
        
        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.25];
    }];
    [request setRequestMethod:@"POST"];
    [request setPostValue:beer.beer_id forKey:@"check[beer_id]"];
    [request startAsynchronous];
}

- (void) successSuggest
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Thanks !", @"")
                                                      message:NSLocalizedString(@"We'll beech it soon.", @"")
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];

}

- (void) failAction
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Damn !", @"")
                                                      message:NSLocalizedString(@"Seems like you don't have any internet.", @"")
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
}

- (void) performSuggest:(id)sender
{
    [bg setHidden:YES];
    [beechCount setHidden:YES];
    [beerName setHidden:YES];
    [beechButton setEnabled:NO];
    [suggestButton setEnabled:NO];
    
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }
    
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, 600)];
    [loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(119,30,42,42)];
    [loadingView addSubview:anImageView];
    
    //    [self.view insertSubview:loadingView atIndex:1];
    [modalViewContent addSubview:loadingView];
    //    [self.view sendSubviewToBack:loadingView];
    
    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
    
    // POST request
    NSLog(@"%@", sender);
    
    NSURL *url = [RequestHelper createURL:@"/beers" withPrefix:@"/api"];
    ASIFormDataRequest *_request = [RequestHelper prepareFormRequestWithUrl:url withParams:@{}];
    __weak ASIFormDataRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@",json);
        [self fadeOut];

        SEL theSelector = NSSelectorFromString(@"successSuggest");
        NSInvocation *anInvocation = [NSInvocation
                                      invocationWithMethodSignature:
                                      [BeechViewController instanceMethodSignatureForSelector:theSelector]];
        
        [anInvocation setSelector:theSelector];
        [anInvocation setTarget:self];
        
        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.25];
        
    }];
    [request setFailedBlock:^{
        [self fadeOut];
        
        SEL theSelector = NSSelectorFromString(@"failAction");
        NSInvocation *anInvocation = [NSInvocation
                                      invocationWithMethodSignature:
                                      [BeechViewController instanceMethodSignatureForSelector:theSelector]];
        
        [anInvocation setSelector:theSelector];
        [anInvocation setTarget:self];
        
        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.25];
        
    }];
    [request setRequestMethod:@"POST"];
    [request setPostValue:[beerNameTextField text] forKey:@"beer[name]"];
    [request startAsynchronous];
}

- (IBAction)textFieldBeganEdit:(id)sender
{
    [_tableView addGestureRecognizer:tapEvent];

}

- (IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
    [_tableView removeGestureRecognizer:tapEvent];
}


- (void)dismissKeyboard
{
    [textField resignFirstResponder];
    [_tableView removeGestureRecognizer:tapEvent];
}


- (void)viewDidUnload {
//    suggestButton = nil;
    _title = nil;
    backButton = nil;
    titleBanner = nil;
    searchWrapper = nil;
    [super viewDidUnload];
}

- (UITableView*) tableView
{
    return _tableView;
}

@end
