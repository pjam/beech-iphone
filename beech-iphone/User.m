//
//  User.m
//  beech-iphone
//
//  Created by Pierre Jambet on 23/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "User.h"
#import "SqliteDB.h"

@implementation User
@synthesize nickname, email, avatar_url, user_id, already_following, created_at, dictionaryValue;

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    return dateFormatter;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"nickname": @"nickname",
             @"email": @"email",
             @"already_following": @"already_following",
             @"avatar_url": @"avatar_url",
             @"created_at": @"created_at",
             @"user_id": @"id",
             };
}

+ (NSValueTransformer *)nicknameJSONTransformer {
	return [MTLValueTransformer
            reversibleTransformerWithForwardBlock:^(NSString *str) {
                return str;
            }
            reverseBlock:^(NSString *str) {
                return str;
            }];}

+ (NSValueTransformer *)avatar_urlJSONTransformer {
	return [MTLValueTransformer
            reversibleTransformerWithForwardBlock:^(NSDictionary *dict) {
                return dict;
            }
            reverseBlock:^(NSDictionary *dict) {
                return dict;
            }];}


- (BOOL)isEqualToUser:(User *)user {
    return user.user_id == self.user_id;
}

- (NSString *)thumbUrl
{
    return [[self.avatar_url objectForKey:@"thumb"] objectForKey:@"url"];
}

- (BOOL)saveWithDB:(FMDatabase *)db
{
    BOOL dbWasNil = NO;
    if (db == nil) {
        db = [SqliteDB DB];
        [db open];
        dbWasNil = YES;
    }
    NSDictionary *argsDict = @{@"user_id": self.user_id};
    FMResultSet *results = [db executeQuery:@"SELECT * FROM users WHERE id = :user_id" withParameterDictionary:argsDict];

    User *userInDb = [[User alloc] init];
    while([results next]) {
        userInDb.user_id = (NSNumber*)[results stringForColumn:@"id"];
        userInDb.avatar_url = @{@"thumb": @{@"url": [results stringForColumn:@"avatar_url"]}};
    }

    if(userInDb.user_id > 0) {
        if ([self thumbUrl] != [userInDb thumbUrl]) {
            [db executeUpdate:@"UPDATE users SET avatar_url = ? where id = ?", [self thumbUrl], self.user_id];
        }
        return NO;
    } else {
        argsDict = @{@"user_id": self.user_id,
                     @"nickname": self.nickname,
                     @"avatar_url": [self thumbUrl]};
        [db executeUpdate:@"INSERT INTO users (id, nickname, avatar_url) VALUES (:user_id, :nickname, :avatar_url)" withParameterDictionary:argsDict];
        return YES;
    }

    if (dbWasNil) {
        [db close];
    }
}
@end
