//
//  Badge.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface Badge : MTLModel <MTLJSONSerializing>
@property(nonatomic) NSNumber *badge_id;
@property(nonatomic) NSString *name;
@property(nonatomic) NSString *description;
@property(nonatomic) NSDictionary *photo_url;
@end
