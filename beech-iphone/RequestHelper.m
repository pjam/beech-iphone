//
//  RequestHelper.m
//  beech-iphone
//
//  Created by Pierre Jambet on 27/12/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "RequestHelper.h"
#import "AppDelegate.h"

@implementation RequestHelper

+ (NSURL *)createURL:(NSString *)path withPrefix:(NSString *) prefix
{
    NSMutableString *urlString = [NSMutableString stringWithString:kServerURL];
    [urlString appendString:prefix];
    [urlString appendString:path];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"%@", urlString);
    return url;
}

+ (NSURL *)URL:(NSString *) urlString ByAppendingQueryString:(NSDictionary *)queryStringArgs {

    NSMutableArray *queryStringList = [[NSMutableArray alloc] init];
    for(id key in queryStringArgs) {
        [queryStringList addObject:[NSString stringWithFormat:@"%@=%@", key, queryStringArgs[key]]];
    }

    NSString *queryString = [queryStringList componentsJoinedByString:@"&"];
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", urlString, queryString]];
    return theURL;
}

+ (ASIHTTPRequest *)prepareRequestWithUrl:(NSURL *) url withParams:(NSDictionary*) queryArgs
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *queryArgsWithAuth = [queryArgs mutableCopy];
    NSString *authToken = [defaults stringForKey:@"authentication_token"];
    if (authToken) {
        queryArgsWithAuth[@"auth_token"] = authToken;
    }

    NSURL *requestURL = [self URL:[url absoluteString] ByAppendingQueryString:queryArgsWithAuth];
    ASIHTTPRequest* request = [ASIHTTPRequest requestWithURL:requestURL];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"fr"]) {
        language = @"fr";
    } else
    {
        language = @"en";
    }
    [request addRequestHeader:@"Accept-Language" value:language];
    [request setShouldAttemptPersistentConnection:NO];
    
    return request;
}

+ (ASIFormDataRequest *)prepareFormRequestWithUrl:(NSURL *) url withParams:(NSDictionary*) queryArgs
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *queryArgsWithAuth = [queryArgs mutableCopy];
    NSString *authToken = [defaults stringForKey:@"authentication_token"];
    if (authToken) {
        queryArgsWithAuth[@"auth_token"] = authToken;
    }

    NSURL *requestURL = [self URL:[url absoluteString] ByAppendingQueryString:queryArgsWithAuth];
    ASIFormDataRequest* request = [ASIFormDataRequest requestWithURL:requestURL];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request setShouldAttemptPersistentConnection:NO];
    

    return request;
}

@end
