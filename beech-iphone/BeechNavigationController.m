//
//  BeechNavigationController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 24/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechNavigationController.h"

@interface BeechNavigationController ()

@end

@implementation BeechNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([self.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [self.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [super pushViewController:viewController animated:animated];
}

@end
