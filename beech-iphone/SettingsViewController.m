//
//  SettingsViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 09/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingsContactViewController.h"
#import "ASIHTTPRequest.h"
#import "ColorHelper.h"
//#import <objc/runtime.h>

@interface SettingsViewController ()
{
    
    __weak IBOutlet UIButton *profileButton;
    __weak IBOutlet UIButton *contactButton;
    __weak IBOutlet UIButton *creditsButton;
    __weak IBOutlet UIButton *helpButton;
    __weak IBOutlet UIButton *logoutButton;
    __weak IBOutlet UIButton *boomButton;
}
@end

@implementation SettingsViewController
@synthesize user;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController viewWillAppear:animated];
}
- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [viewController viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Settings", @"");
//    self.navigationController.delegate = self;
    
    [boomButton setTitleColor:[ColorHelper greyColor] forState:UIControlStateNormal];

    [profileButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [creditsButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [helpButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [contactButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [logoutButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    [logoutButton setTitle:NSLocalizedString(@"Logout", @"") forState:UIControlStateNormal];
    [logoutButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [logoutButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] forState:UIControlStateNormal];
    [logoutButton setTitleShadowColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0] forState:UIControlStateNormal];
    [logoutButton addTarget:self action:@selector(logoutTapped) forControlEvents:UIControlEventTouchUpInside];

    [contactButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [contactButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] forState:UIControlStateNormal];
    [contactButton setTitleShadowColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0] forState:UIControlStateNormal];

    [creditsButton setTitle:NSLocalizedString(@"Credits", @"") forState:UIControlStateNormal];
    [creditsButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [creditsButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] forState:UIControlStateNormal];
    [creditsButton setTitleShadowColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0] forState:UIControlStateNormal];

    [profileButton setTitle:NSLocalizedString(@"Update your profile", @"") forState:UIControlStateNormal];
    [profileButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [profileButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] forState:UIControlStateNormal];
    [profileButton setTitleShadowColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0] forState:UIControlStateNormal];

    [helpButton setTitle:NSLocalizedString(@"Help", @"") forState:UIControlStateNormal];
    [helpButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [helpButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] forState:UIControlStateNormal];
    [helpButton setTitleShadowColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0] forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)tapProfile:(id)sender
{
    [self performSegueWithIdentifier:@"SettingsProfileSegue" sender:self];
}
- (IBAction)contactTap:(id)sender
{
    [self performSegueWithIdentifier:@"SettingsContactSegue" sender:self];
}

- (IBAction)creditsTap:(id)sender
{
    [self performSegueWithIdentifier:@"SettingsCreditsSegue" sender:self];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"SettingsProfileSegue"])
    {
        SettingsContactViewController *vc = [segue destinationViewController];
        [vc setDelegate:self];
        [vc setUser:user];
    } else if ([segue.identifier isEqualToString:@"SettingsContactSegue"])
    {
        SettingsContactViewController *vc = [segue destinationViewController];
        [vc setDelegate:self];
    }
}

- (void) logoutTapped
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Log out", @"")
                                                      message:NSLocalizedString(@"Are you sure you want to log out ?", @"")
                                                     delegate:self
                                            cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                            otherButtonTitles:NSLocalizedString(@"Logout", @""), nil];
    [message show];
}

- (void)viewDidUnload {
    profileButton = nil;
    contactButton = nil;
    creditsButton = nil;
    helpButton = nil;
    logoutButton = nil;
    boomButton = nil;
    [super viewDidUnload];
}
- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:NSLocalizedString(@"Hey !", @"")];
        NSArray *toRecipients = [NSArray arrayWithObjects:@"contact@getbeech.com", nil];
        [mailer setToRecipients:toRecipients];
        NSString *emailBody = NSLocalizedString(@"Leave us a message", @"");
        [mailer setMessageBody:emailBody isHTML:NO];
        mailer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
//        [self presentModalViewController:mailer animated:YES];

        
        // remove the custom nav bar font
//        NSMutableDictionary* navBarTitleAttributes = [[UINavigationBar appearance] titleTextAttributes].mutableCopy;
//        UIFont* navBarTitleFont = navBarTitleAttributes[UITextAttributeFont];
//        navBarTitleAttributes[UITextAttributeFont] = [UIFont systemFontOfSize:navBarTitleFont.pointSize];
//        [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleAttributes];
        
        // set up and present the MFMailComposeViewController
//        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
//        mailComposer.mailComposeDelegate = self;
//        [mailComposer setSubject:emailInfo[@"subject"]];
//        [mailComposer setMessageBody:emailInfo[@"message"] isHTML:YES];


        [self presentViewController:mailer animated:YES completion:nil];
        [self performSelector:@selector(styleMailer:) withObject:mailer afterDelay:1];

        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                                        message:NSLocalizedString(@"It seems you can't send mails.", @"")
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void) styleMailer:(MFMailComposeViewController*)mailer
{

    UIImage *image = [UIImage imageNamed:@"navbar.png"];
    [mailer.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, 44)];
    [backgroundView setBackgroundColor:[UIColor colorWithPatternImage:image]];
                                                                                                          
//    mailer.topViewController.navigationItem.titleView = backgroundView;
//    mailer.navigationItem.leftBarButtonItem setb
    
//    [mailer.navigationBar setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor whiteColor], UITextAttributeTextColor,
//      [UIFont fontWithName:@"System" size:18.0], UITextAttributeFont,
//      [UIColor clearColor], UITextAttributeTextShadowColor,nil]];
    [mailer.navigationBar setTitleVerticalPositionAdjustment:0.0f forBarMetrics:UIBarMetricsDefault];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        UIViewController *tabController = [[self parentViewController] parentViewController];
        [tabController performSegueWithIdentifier:@"LoginSegue" sender:tabController];
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [ASIHTTPRequest setSessionCookies:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"logout-success" object:nil];
    }
}
- (IBAction)boomButtonTapped:(id)sender {
    [NSException raise:@"Big badaboom" format:@"BOOM"];
}

@end
