//
//  FirstViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "FeedCell.h"
#import "EventsViewController.h"

@interface FeedViewController : EventsViewController <UITableViewDelegate, UITableViewDataSource, EGORefreshTableHeaderDelegate, FeedCellDelegate>
{
    
    EGORefreshTableHeaderView *_refreshHeaderView;
	
	//  Reloading var should really be your tableviews datasource
	//  Putting it here for demo purposes
	BOOL _reloading;
}

- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;
- (void)clear;
@property(nonatomic) BOOL loadingAnteriorData;
@end
