//
//  BeechTabBarViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 11/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechTabBarViewController.h"
#import "BeerCollection.h"

@interface BeechTabBarViewController ()
{
    NSMutableArray *buttons;
    UIView *bottomBar;
}
@end

@implementation BeechTabBarViewController

@synthesize beechButton, tabButtons;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tabBar setHidden:YES];
    buttons = [[NSMutableArray alloc] init];
    [self buildNewBar];
    // Do any additional setup after loading the view.

    self.delegate = self;
    self.tabBar.frame = CGRectMake(0, self.view.frame.size.height-self.tabBar.frame.size.height+5, self.tabBar.frame.size.width, self.tabBar.frame.size.height);
    
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    bottomBar.userInteractionEnabled = YES;
    NSLog(@"ViewDidLoad");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    NSLog(@"%@", cookies);
    if ([defaults stringForKey:@"username"] && [defaults stringForKey:@"password"])
    {
        NSLog(@"Already authenticated");
    } else
    {
        [self performSegueWithIdentifier:@"LoginSegue" sender:self];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
//    navigationBar.topItem.title = viewController.title;
    NSLog(@"SElected");

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor redColor];
    label.text = @"beech";
    label.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:17.0];
//    self.navigationItem.titleView = label;
    
//    viewController.navigationItem.titleView = label;
//    [viewController.navigationItem setTitleView:label];
}

- (void)beechTap
{
    [self performSegueWithIdentifier:@"BeechSegue" sender:self];    
}

- (void)buildNewBar{
    bottomBar = [[UIView alloc] initWithFrame:self.tabBar.frame];
    [bottomBar setBackgroundColor:[UIColor colorWithRed:0.18f green:0.18f blue:0.18f alpha:1.00f]];
    [self.view addSubview:bottomBar];

    beechButton = [UIButton buttonWithType:UIButtonTypeCustom];
    beechButton.frame = CGRectMake(0, 0, 65, 50);
    [beechButton setImage:[UIImage imageNamed:@"tabs-button-beech-default"] forState:UIControlStateNormal];
    [beechButton setImage:[UIImage imageNamed:@"tabs-button-beech-active"] forState:UIControlStateHighlighted];
    [beechButton addTarget:self action:@selector(beechTap) forControlEvents:UIControlEventTouchUpInside];

    UIImage *imgWall = [UIImage imageNamed:@"tabs-button-wall-active"];
    UIImage *imgWallDefault = [UIImage imageNamed:@"tabs-button-wall-default"];

    NSString *profileCurrentName = @"tabs-button-profile-active";
    NSString *profileDefaultName = @"tabs-button-profile-default";
    UIImage *imgProfile = [UIImage imageNamed:profileCurrentName];
    UIImage *imgProfileDefault = [UIImage imageNamed:profileDefaultName];

    UIImage *imgSearch = [UIImage imageNamed:@"tabs-button-beechers-active"];
    UIImage *imgSearchDefault = [UIImage imageNamed:@"tabs-button-beechers-default"];

    UIButton *modal = [UIButton buttonWithType:UIButtonTypeCustom];
//    [modal setFrame:CGRectMake(0, 0, 44, 44)];
    [bottomBar addSubview:beechButton];
    [beechButton setBackgroundColor:[UIColor greenColor]];
    [beechButton addTarget:self action:@selector(beechTap) forControlEvents:UIControlEventTouchUpInside];

    modal = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttons addObject:modal];
    [modal setFrame:CGRectMake(65, 0, 85, 50)];
    [modal setTag:0];
    [bottomBar addSubview:modal];
    [modal setBackgroundImage:imgWall forState:UIControlStateSelected];
    [modal setBackgroundImage:imgWall forState:UIControlStateHighlighted | UIControlStateSelected];
    [modal setBackgroundImage:imgWallDefault forState:UIControlStateNormal];
    [modal setSelected:YES];
    [modal addTarget:self action:@selector(openThisView:) forControlEvents:UIControlEventTouchUpInside];

    modal = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttons addObject:modal];
    [modal setFrame:CGRectMake(150, 0, 85, 50)];
    [modal setTag:1];
    [bottomBar addSubview:modal];
    [modal setBackgroundImage:imgProfile forState:UIControlStateSelected];
    [modal setBackgroundImage:imgProfile forState:UIControlStateHighlighted | UIControlStateSelected];    
    [modal setBackgroundImage:imgProfileDefault forState:UIControlStateNormal];
    [modal addTarget:self action:@selector(openThisView:) forControlEvents:UIControlEventTouchUpInside];

    modal = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttons addObject:modal];    
    [modal setFrame:CGRectMake(235, 0, 85, 50)];
    [modal setTag:2];
    [bottomBar addSubview:modal];
    [modal setBackgroundImage:imgSearch forState:UIControlStateSelected];
    [modal setBackgroundImage:imgSearch forState:UIControlStateHighlighted | UIControlStateSelected];
    [modal setBackgroundImage:imgSearchDefault forState:UIControlStateNormal];
    [modal addTarget:self action:@selector(openThisView:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)openThisView:(UIButton *)button{

    [self setSelectedIndex:button.tag];
    if ([button isSelected]) {
        [(UINavigationController*)self.selectedViewController popToRootViewControllerAnimated:YES];
    } else {
        [button setSelected:YES];
    }

    for (id abutton in buttons) {
        if (abutton != button) {
            [abutton setSelected:NO];
        }
    }
//    UIButton *abutton = (UIButton*)[buttons objectAtIndex:button.tag];
}


- (void)setSelectedViewController:(UIViewController *)selectedViewController
{
    [super setSelectedViewController:selectedViewController];
    for (id button in buttons) {
        [button setSelected:NO];
    }
    NSUInteger vcIndex = [self.viewControllers indexOfObject:selectedViewController];
    [[buttons objectAtIndex:vcIndex] setSelected:YES];
}


- (void)open{
    NSLog(@"open!!!!");
}

@end
