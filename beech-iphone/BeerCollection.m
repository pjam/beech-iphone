//
//  BeerCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "BeerCollection.h"
#import "RequestHelper.h"

@implementation BeerCollection
{
    FMDatabase *database;
}

- (id)init
{
    self = [super init];

    return self;
}

- (FMDatabase*) getDatabase
{
    if (database == nil) {
        database = [SqliteDB DB];
        [database open];
    }

    return database;
}

- (Beer*)beerForId:(NSNumber *)beer_id
{
    NSUInteger beerIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id beer, NSUInteger idx,BOOL *stop)
     {
         return [((Beer*)beer).beer_id intValue] == [beer_id intValue];
     }];
    return [self.collection objectAtIndex:beerIndex];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Beer *newBeer = [MTLJSONAdapter modelOfClass:Beer.class fromJSONDictionary:item error:nil];
        
        NSUInteger beerIndex =
        [self.collection indexOfObjectPassingTest:^BOOL(id beer, NSUInteger idx,BOOL *stop)
         {
             return [((Beer*)beer).beer_id intValue] == [((Beer*)newBeer).beer_id intValue];
         }];
        if (beerIndex == NSNotFound) {
            [self.collection addObject:newBeer];
        }
    }
}

+ (void) syncWithServer
{
    // Pass the last id
    NSURL *url = [RequestHelper createURL:[NSString stringWithFormat:@"/journal_entries/"] withPrefix:@"/api"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    id lastId = [defaults objectForKey:@"last_journal_id"];
    if (lastId == nil) {
        lastId = [NSNumber numberWithInt:0];
    }

    NSDictionary *args = @{@"last_id": lastId};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        [self persistBeers:json];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];
}

- (void)loadFromDB
{
    FMResultSet *results = [[self getDatabase] executeQuery:@"SELECT * FROM beers ORDER BY name ASC"];

    [self.collection removeAllObjects];
    while([results next]) {
        [self.collection addObject:[self loadBeerFromResultSet:results]];
    }
}

+ (void) persistBeers:(NSDictionary*)jsonData
{
    if ([[jsonData objectForKey:@"journal_entries"] count] == 0) {
        return;
    }
    long long milliseconds = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);


    // Remove too old elements
    // If there are deleted beers, they should be removed
    FMDatabase* db = [SqliteDB DB];
    [db open];

    // Find all existing beers
    FMResultSet *results = [db executeQuery:@"SELECT id AS beer_id FROM beers ORDER BY name ASC"];
    NSMutableArray *allBeerIds = [[NSMutableArray alloc] init];
    while ([results next]) {
        NSNumber *beerId = [NSNumber numberWithInt:[[results stringForColumn:@"beer_id"] intValue]];
        [allBeerIds addObject:beerId];
    }

    for (NSDictionary *jsonJournalEntry in [jsonData objectForKey:@"journal_entries"]) {
        if ([jsonJournalEntry[@"loggable_type"] isEqualToString:@"Beer"]) {
            if ([jsonJournalEntry[@"entry_type"] isEqualToString:@"new"]) {
                if (jsonJournalEntry[@"loggable"] != [NSNull null]) {
                    // Record might be nil if ts been deleted
                    NSDictionary *jsonBeer = jsonJournalEntry[@"loggable"][@"beer"];
                    Beer *beer = [MTLJSONAdapter modelOfClass:Beer.class fromJSONDictionary:jsonBeer error:nil];
                    BOOL beerAlreadySaved = NO;
                    int i = 0;
                    while (i < [allBeerIds count] && !beerAlreadySaved) {
                        NSNumber *bid = [allBeerIds objectAtIndex:i];
                        if ([bid isEqualToNumber:beer.beer_id]) {
                            beerAlreadySaved = YES;
                        }
                        i++;
                    }
                    if (!beerAlreadySaved) {
                        [beer saveWithDB:db forceInsert:YES];
                    }

                }
            } else if ([jsonJournalEntry[@"entry_type"] isEqualToString:@"edit"]) {
                if (jsonJournalEntry[@"loggable"] != [NSNull null]) {
                    NSDictionary *jsonBeer = jsonJournalEntry[@"loggable"][@"beer"];
                    Beer *beer = [MTLJSONAdapter modelOfClass:Beer.class fromJSONDictionary:jsonBeer error:nil];
                    [beer saveWithDB:db forceInsert:NO];
                }
            } else if ([jsonJournalEntry[@"entry_type"] isEqualToString:@"deletion"]) {
                [db executeUpdate:@"DELETE FROM beers WHERE id = ?", jsonJournalEntry[@"loggable_id"]];
            }
        }
    }

    // Store last id
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *lastEntry = [[jsonData objectForKey:@"journal_entries"] lastObject];
    [defaults setValue:[lastEntry objectForKey:@"id"] forKey:@"last_journal_id"];
    [db close];


    long long milliseconds2 = (long long)([[NSDate date] timeIntervalSince1970] * 1000.0);

    NSLog(@"This lasted : %lld", milliseconds2 - milliseconds);
}

- (void)search:(NSString *)query
{
    FMResultSet *results = [[self getDatabase] executeQuery:@"SELECT * FROM beers WHERE name LIKE ? ORDER BY name ASC", [NSString stringWithFormat:@"%%%@%%", query] ];

    [self.collection removeAllObjects];
    while([results next]) {
        [self.collection addObject:[self loadBeerFromResultSet:results]];
    }

}

- (NSUInteger)numberOfBeersStartingWith:(NSString *)prefix
{
    FMResultSet *results = [[self getDatabase] executeQuery:@"SELECT COUNT(*) AS total_count FROM beers WHERE name LIKE ? ORDER BY name ASC", [NSString stringWithFormat:@"%%%@", prefix] ];

    NSUInteger totalBeersForPrefix = 0;
    while([results next]) {
        totalBeersForPrefix = [results intForColumn:@"total_count"];
    }
    return totalBeersForPrefix;
}

- (NSArray*) beersStartingWith:(NSString *)prefix
{
    NSString *richPrefix;
    if ([prefix isEqualToString:@"#"]) {
        richPrefix = @"[0-9]";
    } else {
        richPrefix = [NSString stringWithFormat:@"[%@%@]", [prefix uppercaseString], [prefix lowercaseString]];
    }
    FMResultSet *results = [[self getDatabase] executeQuery:@"SELECT * FROM beers WHERE name GLOB ? ORDER BY name ASC", [NSString stringWithFormat:@"%@*", richPrefix] ];


    NSMutableArray *beers = [[NSMutableArray alloc] init];
    while([results next]) {
        [beers addObject:[self loadBeerFromResultSet:results]];
    }
    return beers;
}

- (Beer *) loadBeerFromResultSet:(FMResultSet*) resultSet
{
    Beer *beer = [[Beer alloc] init];
    NSString *beerId = [resultSet stringForColumn:@"id"];
    NSString *beerName = [resultSet stringForColumn:@"name"];
    NSString *countryName = [resultSet stringForColumn:@"country"];
    if (!countryName) {
        countryName = @"";
    }
    beer.beer_id = [NSNumber numberWithInt:[beerId intValue]];
    beer.country = countryName;
    beer.name = beerName;
    return beer;
}

- (void)dealloc
{
    [database close];
}

- (void)removeAllObjects
{
    [super removeAllObjects];
    FMDatabase *db = [SqliteDB DB];
    [db open];

    [db executeUpdate:@"DELETE FROM beers"];


    [db close];
}
@end
