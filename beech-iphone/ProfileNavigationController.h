//
//  ProfileNavigationController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeechNavigationController.h"

@interface ProfileNavigationController : BeechNavigationController

@end
