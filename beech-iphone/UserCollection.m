//
//  UserCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "UserCollection.h"

@implementation UserCollection
- (User*)userForId:(NSNumber *)user_id
{
    NSUInteger userIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id user, NSUInteger idx,BOOL *stop)
     {
         return [((User*)user).user_id intValue] == [user_id intValue];
     }];
    return [self.collection objectAtIndex:userIndex];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        User *newUser = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:item error:nil];
        
        NSUInteger userIndex =
        [self.collection indexOfObjectPassingTest:^BOOL(id user, NSUInteger idx,BOOL *stop)
         {
             return [((User*)user).user_id intValue] == [((User*)newUser).user_id intValue];
         }];
        if (userIndex == NSNotFound) {
            [self.collection addObject:newUser];
        }
    }
}
@end
