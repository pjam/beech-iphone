//
//  SlideItemViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 09/02/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideItemViewController : UIViewController
@property(nonatomic) NSString *stringTitle;
@property(nonatomic) NSString *stringContent;
@property (weak, nonatomic) IBOutlet UIImageView *slideImage;
@property (weak, nonatomic) IBOutlet UIView *bottomText;

- (id)initWithTitle:(NSString*)title andContent:(NSString*) content;
@end
