//
//  Profile.h
//  beech-iphone
//
//  Created by Pierre Jambet on 28/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Profile : NSObject
@property(nonatomic, strong) NSString *name;
@property(nonatomic, strong) NSNumber *followerCount;
@property(nonatomic, strong) NSNumber *followingCount;
@end
