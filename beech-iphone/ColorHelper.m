//
//  ColorHelper.m
//  beech-iphone
//
//  Created by Pierre Jambet on 15/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "ColorHelper.h"

@implementation ColorHelper

+(UIColor*) greyColor
{
    return [UIColor colorWithRed:0.61 green:0.61 blue:0.61 alpha:1.0];
}

+(UIColor*) darkColor
{
    return [UIColor colorWithRed:0.07 green:0.07 blue:0.07 alpha:1.0];
}

+(UIColor*) blackColor
{
    return [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
}

+(UIColor*) blueColor
{
    return [UIColor colorWithRed:66/255.0 green:138/255.0 blue:194/255.0 alpha:1.0];
}

@end
