//
//  BadgeDetailViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 20/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "BadgeDetailViewController.h"
#import "BadgesViewController.h"

@interface BadgeDetailViewController ()
@end

@implementation BadgeDetailViewController

@synthesize badgeName, badgeIndex, badgeDescription, delegate, descriptionBackground;

- (id) initWithNothing
{
    self = [super initWithNibName:@"BadgeDetailViewController" bundle:nil];
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [badgeName setAdjustsFontSizeToFitWidth:YES];
    [descriptionBackground setBackgroundColor:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.8]];
    badgeDescription.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:16.0];
    [badgeName setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:20.0]];
    [badgeName setTextColor:[UIColor colorWithRed:76.0/255 green:76.0/255 blue:76.0/255 alpha:1.0]];
    [badgeIndex setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:10.0]];
    [badgeIndex setTextColor:[UIColor colorWithRed:135.0/255 green:135.0/255 blue:135.0/255 alpha:1.0]];
    descriptionBackground.alpha = 0;
    
    UITapGestureRecognizer *badgesTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:badgesTapGesture];
}

- (void) handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    BadgesViewController *bvc = (BadgesViewController*)delegate;
    for ( BadgeDetailViewController* bdvc in [bvc badgesViews])
    {
        if (bdvc != self )
        {
            [UIView animateWithDuration:0.2
                             animations:^{
                                 [bdvc descriptionBackground].alpha = 0.0;
                             }];
        }
    }
    if (descriptionBackground.alpha == 1.0)
    {
        [UIView animateWithDuration:0.2
                         animations:^{
                             descriptionBackground.alpha = 0.0;
                         }];
    } else
    {
        [UIView animateWithDuration:0.2
                         animations:^{
                             descriptionBackground.alpha = 1.0;
                         }];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    badgeName = nil;
    badgeIndex = nil;
    [self setBadgeIndex:nil];
    [self setBadgeName:nil];
    [self setRightBorder:nil];
    [self setBackgroundView:nil];
    [self setBadgeImage:nil];
    [self setBadgeDescription:nil];
    [self setDescriptionBackground:nil];
    [super viewDidUnload];
}
@end
