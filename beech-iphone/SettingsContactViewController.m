//
//  SettingsContactViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 09/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SettingsContactViewController.h"
#import "BeechTabBarViewController.h"
#import "RequestHelper.h"
#import "BeechHelper.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIBarButtonItem+StyledButton.h"


@interface SettingsContactViewController ()
{
    IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *nicknameTextField;
    __weak IBOutlet UITextField *passwordConfirmationTextfield;
    __weak IBOutlet UIButton *logoutButton;
    __weak IBOutlet UIImageView *profilePhoto;
    __weak IBOutlet UIButton *photoButton;
    UIBarButtonItem *submitButton;    
    NSString *password;
    NSString *email;
    NSString *nickname;
    CGRect originalRect;
    UIImage *selectedImage;
    UIView *overlay;
}
@end

@implementation SettingsContactViewController

@synthesize delegate, user;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void) onKeyboardHide:(id) sender
{
    NSDictionary* keyboardInfo = [sender userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect rect = [keyboardFrameBegin CGRectValue];
    scrollView.frame = CGRectMake(0, 0, 320, scrollView.frame.size.height + 150);
//    self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height + 250);
}

- (void) onKeyboardShow:(id) sender
{
    NSDictionary* keyboardInfo = [sender userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect rect = [keyboardFrameBegin CGRectValue];
    [self performSelector:@selector(reduceContentHeight:) withObject:nil afterDelay:0.5];
//    self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - 250);
}

- (void) reduceContentHeight: (id)sender
{
    scrollView.frame = CGRectMake(0, 0, 320, scrollView.frame.size.height - 150);    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    originalRect = self.view.frame;
	// Do any additional setup after loading the view.
    self.navigationItem.title = NSLocalizedString(@"Update profile", @"");
    [scrollView setScrollEnabled:YES];
    scrollView.contentSize = CGSizeMake(320, self.view.frame.size.height - 50 - 20 - self.navigationController.navigationBar.frame.size.height);

    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height)];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [overlay setBackgroundColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:0.9]];
    [overlay addSubview:spinner];
    spinner.center = CGPointMake(self.view.frame.size.width / 2, (self.view.frame.size.height / 2) - 50 );
    [spinner startAnimating];
    [self.view addSubview:overlay];
    [overlay setHidden:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    
    
    submitButton = [UIBarButtonItem styledSubmitBarButtonItemWithTitle:NSLocalizedString(@"Submit", @"") target:self selector:@selector(submitButtonTapped)];

    NSString *imageStringUrl = [BeechHelper validURLStringWithString:[[user.avatar_url objectForKey:@"thumb"] objectForKey:@"url"]];
    NSURL *imageUrl = [NSURL URLWithString:imageStringUrl];

    [profilePhoto setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];

    profilePhoto.layer.cornerRadius = 5.0f;
    profilePhoto.layer.masksToBounds = YES;
    
    [photoButton setTitle:NSLocalizedString(@"Profile photo", @"") forState:UIControlStateNormal];
    [photoButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [photoButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] forState:UIControlStateNormal];
    [photoButton setTitleColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:0.7] forState:UIControlStateHighlighted];
    photoButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

    [nicknameTextField setPlaceholder:user.nickname];
    [nicknameTextField setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [nicknameTextField setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
    [nicknameTextField addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];

    [emailTextField setPlaceholder:user.email];
    [emailTextField setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [emailTextField setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
    [emailTextField addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];

    [passwordTextField setPlaceholder:NSLocalizedString(@"Password", @"")];
    [passwordTextField setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [passwordTextField setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
    [passwordTextField addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];

    [passwordConfirmationTextfield setPlaceholder:NSLocalizedString(@"Password confirmation", @"")];
    [passwordConfirmationTextfield setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:16.0]];
    [passwordConfirmationTextfield setTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
    [passwordConfirmationTextfield addTarget:self action:@selector(textFieldReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    self.navigationItem.rightBarButtonItem = submitButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    BeechTabBarViewController *tabController = (BeechTabBarViewController*)[[self parentViewController] parentViewController];
}

- (void) viewDidAppear:(BOOL)animated
{
    NSLog(@"View did appear");
}

- (void)viewWillDisappear:(BOOL)animated
{
    BeechTabBarViewController *tabController = (BeechTabBarViewController*)[self tabBarController]; //(BeechTabBarViewController*)[[delegate parentViewController] parentViewController];
}

- (void) viewDidDisappear:(BOOL)animated
{


}

- (void)viewDidUnload {
    passwordTextField = nil;
    emailTextField = nil;
    nicknameTextField = nil;
    passwordConfirmationTextfield = nil;
    logoutButton = nil;
    scrollView = nil;
    profilePhoto = nil;
    photoButton = nil;
    [super viewDidUnload];
}
- (void)submitButtonTapped
{
    NSURL *url = [RequestHelper createURL:@"/my/profile" withPrefix:@"/api"];
    

    [overlay setHidden:NO];
    [scrollView setScrollEnabled:NO];

    
    self.navigationController.navigationItem.backBarButtonItem.enabled = NO;
    ASIFormDataRequest *_request = [RequestHelper prepareFormRequestWithUrl:url withParams:@{}];
    __weak ASIFormDataRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        [overlay setHidden:YES];
        [scrollView setScrollEnabled:YES];

        NSData *data = [request responseData];
        if ([request responseStatusCode] != 200 && [request responseStatusCode] != 201) {
            NSError *error = [request error];
            NSLog(@"success with error : %@", error);
            NSLog(@"%@", data);
        }
        else {
            // authentication successful, store credentials
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSLog(@"%@", data);
//            NSString *nickname = [[data objectForKey:@"user"] objectForKey@"nickname"];
//            NSString *email = [[data objectForKey:@"user"] objectForKey@"nickname"];
//            NSString *password = [[data objectForKey:@"user"] objectForKey@"password"];

            // Refresh cookies
//            [ASIHTTPRequest clearSession];
            
        };
        
    }];
    [request setFailedBlock:^{
        [overlay setHidden:YES];
        [scrollView setScrollEnabled:YES];
        NSError *error = [request error];
        NSLog(@"failed with error : %@", error);
        NSLog(@"failed with error : %@", error);
        [[[[self navigationController] navigationItem] backBarButtonItem] setEnabled:YES];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error !", @"")
                                                          message:NSLocalizedString(@"Upload failed", @"")
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }];
    
    if ([self getEmail] && [[self getEmail] length] > 0 )
    {
        [request addPostValue:[self getEmail] forKey:@"user[email]"];
    }
    if ([self getPassword] && [[self getPassword] length] > 0 && [self getPasswordConfirmation] && [[self getPasswordConfirmation] length] > 0)
    {
        [request addPostValue:[self getPassword] forKey:@"user[password]"];
        [request addPostValue:[self getPasswordConfirmation] forKey:@"user[password_confirmation]"];
    }
    if ([self getNickName] && [[self getNickName] length] > 0)
    {
        [request addPostValue:[self getNickName] forKey:@"user[nickname]"];
    }
    
    //    [request addPostValue:[self getEmail] forKey:@"user[avatar]"];
    
    
    if (selectedImage)
    {
        NSData *imageData = UIImageJPEGRepresentation(selectedImage, 0);
        [request setData:imageData forKey:@"user[avatar]"];
    }
    
    password = [self getPassword];
    nickname = [self getNickName];
    email = [self getEmail];
    
    
    [request setRequestMethod:@"PUT"];
    [request startAsynchronous];
}

-(NSString*) getEmail
{
    return [emailTextField text];
}

-(NSString*) getPassword
{
    return [passwordTextField text];
}

-(NSString*) getPasswordConfirmation
{
    return [passwordConfirmationTextfield text];
}

-(NSString*) getNickName
{
    return [nicknameTextField text];
}
- (IBAction)photoButtonTapped:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] init];
    [actionSheet setDelegate:self];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Take a photo", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"From the library", nil)];
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
    [actionSheet setCancelButtonIndex:2];
//                                  initWithTitle:@"TItre"
//                                  delegate:self
//                                  cancelButtonTitle:@"Annuler"
//                                  destructiveButtonTitle:@""
//                                  otherButtonTitles:@"Prendre une photo", @"Photo depuis la bibliothèque", nil];
//    [actionSheet showInView:self.view];
    [actionSheet showInView:self.parentViewController.tabBarController.view];
}
- (IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [emailTextField resignFirstResponder];
    [nicknameTextField resignFirstResponder];
    [passwordConfirmationTextfield resignFirstResponder];
    [passwordTextField resignFirstResponder];
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    if (buttonIndex == 0)
    {
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            // Alert
        } else
        {
            [picker setSourceType:UIImagePickerControllerSourceTypeCamera];
            [picker setDelegate:self];
            [self presentModalViewController:picker animated:YES];
        }
        
    } else if (buttonIndex == 1)
    {
        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [picker setDelegate:self];
        [self presentModalViewController:picker animated:YES];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) mPicker {
    
    [mPicker dismissModalViewControllerAnimated:YES];
    
}

- (void)imagePickerController:(UIImagePickerController *) mPicker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    profilePhoto.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    [mPicker dismissModalViewControllerAnimated:YES];
}


@end
