//
//  BeerCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeerCell.h"

@implementation BeerCell
@synthesize name;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        
        [self initContent];
    }
    return self;
}

- (void)initContent
{
    name = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 280, self.contentView.frame.size.height)];
    name.backgroundColor = [UIColor clearColor];
    name.textColor = [UIColor colorWithRed:0.2 green:0.20 blue:0.20 alpha:1];

    name.font = [UIFont fontWithName:@"ProximaNova-Regular" size:18.0];

    [self.contentView addSubview:name];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.size.width = 320.0;
    [super setFrame:frame];
}


@end
