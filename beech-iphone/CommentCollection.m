//
//  CommentCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "CommentCollection.h"

@implementation CommentCollection
- (Comment*)commentForId:(NSNumber *)comment_id
{
    NSUInteger likeIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id like, NSUInteger idx,BOOL *stop)
     {
         return [((Comment*)like).comment_id intValue] == [comment_id intValue];
     }];
    return [self.collection objectAtIndex:likeIndex];
}

- (NSMutableArray *)commentsForEventId:(NSNumber *)event_id
{
    NSIndexSet *commentsIndexes = [self.collection indexesOfObjectsPassingTest:^BOOL(id comment, NSUInteger idx, BOOL *stop)
                                {
                                    return [((Comment*)comment).event_id intValue] == [event_id intValue];
                                }];
    return [[self.collection objectsAtIndexes:commentsIndexes] mutableCopy];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Comment *newComment = [MTLJSONAdapter modelOfClass:Event.class fromJSONDictionary:item error:nil];
        [self addObject:newComment];
    }
}

- (BOOL)addObject:(id)anObject
{

    NSUInteger commentIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id comment, NSUInteger idx,BOOL *stop)
     {
         return [((Comment*)comment).comment_id intValue] == [((Comment*)anObject).comment_id intValue];
     }];
    if (commentIndex == NSNotFound) {
        NSUInteger newIndex = [self.collection indexOfObject:anObject
                                               inSortedRange:(NSRange){0, [self.collection count]}
                                                     options:NSBinarySearchingInsertionIndex
                                             usingComparator:^NSComparisonResult(id a, id b) {
                                                 NSNumber *first = [(Event*)a created_at];
                                                 NSNumber *second = [(Event*)b created_at];
                                                 return [first compare:second];
                                             }];
        [self.collection insertObject:anObject atIndex:newIndex];
        return YES;
    } else {
        return NO;
    }
    
}
@end
