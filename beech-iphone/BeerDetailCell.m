//
//  BeerCell.m
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeerDetailCell.h"

@implementation BeerDetailCell
@synthesize name;
@synthesize count;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (UIColor*) blueFontColor
{
    return [UIColor colorWithRed:52/256.0 green:123/256.0 blue:184/256.0 alpha:1];
}

+ (UIColor*) greyFontColor
{
    return [UIColor colorWithRed:190/256.0 green:190/256.0 blue:190/256.0 alpha:1];
}

- (void) setCountColor:(UIColor *)color
{
    count.textColor = color;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        
        UIImage *img = [UIImage imageNamed:@"bg-list-index.png"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
        
        [self.contentView insertSubview:imageView atIndex:0];
        
        name = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 228, 21)];
        name.backgroundColor = [UIColor clearColor];
        name.textColor = [UIColor colorWithRed:0.2 green:0.20 blue:0.20 alpha:1];
        name.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:18.0];
        
        [self.contentView addSubview:name];
        
        count = [[UILabel alloc] initWithFrame:CGRectMake(200, 15, 100, 35)];
        count.textAlignment = UITextAlignmentRight;
        count.backgroundColor = [UIColor clearColor];
        count.textColor = [BeerDetailCell blueFontColor];
        count.font = [UIFont fontWithName:@"ProximaNova-Light" size:34.0];
        
        [self.contentView addSubview:count];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    frame.size.width = 320.0;
    [super setFrame:frame];
}


@end
