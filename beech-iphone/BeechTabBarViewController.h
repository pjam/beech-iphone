//
//  BeechTabBarViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 11/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeechTabBarViewController : UITabBarController <UITabBarControllerDelegate>

@property (nonatomic, retain) UIButton *beechButton;
@property (nonatomic, retain) NSMutableArray *tabButtons;
@end
