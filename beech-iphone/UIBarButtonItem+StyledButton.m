//
//  UIBarButtonItem+StyledButton.m
//  beech-iphone
//
//  Created by Pierre Jambet on 5/5/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "UIBarButtonItem+StyledButton.h"
#import "UIButton+StyledButton.h"

@implementation UIBarButtonItem (StyledButton)
+ (UIBarButtonItem *)styledBackBarButtonItemWithTarget:(id)target selector:(SEL)selector icon:(UIImage *)icon;
{
    UIImage *image = [UIImage imageNamed:@"navbar-button-basic-default"];
    UIImage *imageHighlighted = [UIImage imageNamed:@"navbar-button-basic-active"];
    image = [image stretchableImageWithLeftCapWidth:4.0f topCapHeight:0.0f];
    imageHighlighted = [imageHighlighted stretchableImageWithLeftCapWidth:4.0f topCapHeight:0.0f];

    UIFont *font = [UIFont boldSystemFontOfSize:12.0f];

    UIButton *button = [UIButton styledButtonWithBackgroundImage:image imageHighlighted:imageHighlighted icon:icon font:font title:nil target:target selector:selector];
    button.titleLabel.textColor = [UIColor blackColor];

//    CGSize textSize = [title sizeWithFont:font];
//    CGFloat margin = (button.frame.size.height - textSize.height) / 2;
//    CGFloat marginRight = 7.0f;
//    CGFloat marginLeft = button.frame.size.width - textSize.width - marginRight;
//    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
    [button setTitleColor:[UIColor colorWithRed:53.0f/255.0f green:77.0f/255.0f blue:99.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];

    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)styledCancelBarButtonItemWithTarget:(id)target selector:(SEL)selector;
{
    UIImage *image = [UIImage imageNamed:@"navbar-button-basic-default"];
    UIImage *imageHighlighted = [UIImage imageNamed:@"navbar-button-basic-active"];
    image = [image stretchableImageWithLeftCapWidth:4.0f topCapHeight:0.0f];
    imageHighlighted = [imageHighlighted stretchableImageWithLeftCapWidth:4.0f topCapHeight:0.0f];

    NSString *title = NSLocalizedString(@"Cancel", nil);
    UIFont *font = [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0];

    UIButton *button = [UIButton styledButtonWithBackgroundImage:image imageHighlighted:imageHighlighted icon:nil font:font title:title target:target selector:selector];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)styledSubmitBarButtonItemWithTitle:(NSString *)title target:(id)target selector:(SEL)selector;
{
    UIImage *image = [UIImage imageNamed:@"navbar-button-white-default"];
    UIImage *imageHighlighted = [UIImage imageNamed:@"navbar-button-white-active"];
    image = [image stretchableImageWithLeftCapWidth:4.0f topCapHeight:0.0f];
    imageHighlighted = [imageHighlighted stretchableImageWithLeftCapWidth:4.0f topCapHeight:0.0f];

    UIFont *font = [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0];

    UIButton *button = [UIButton styledButtonWithBackgroundImage:image imageHighlighted:imageHighlighted icon:nil font:font title:title target:target selector:selector];
    button.titleLabel.textColor = [UIColor whiteColor];
    [button setTitleColor:[UIColor colorWithRed:229.0/255 green:114.0/255 blue:48.0/255 alpha:1.0] forState:UIControlStateNormal];
    [button.titleLabel setShadowColor:[UIColor clearColor]];



//    [beechButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                         [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0], UITextAttributeTextColor,
//                                         [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.0], UITextAttributeTextShadowColor,
//                                         nil] forState:UIControlStateDisabled];


    return [[UIBarButtonItem alloc] initWithCustomView:button];
}
@end
