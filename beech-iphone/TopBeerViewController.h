//
//  TopBeerViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 20/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopBeerViewController : UIViewController
@property(nonatomic) int beerCount;
@property(nonatomic) NSString* beerName;
@property(nonatomic) int beerIndex;

- (id)initWithBeerCount:(int)abeerCount beerName:(NSString*) abeerName beerIndex:(int) abeerIndex;
@end
