//
//  UIButton+StyledButton.h
//  beech-iphone
//
//  Created by Pierre Jambet on 5/5/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (StyledButton)
+ (UIButton *)styledButtonWithBackgroundImage:(UIImage *)image imageHighlighted:(UIImage *)imageHighlighted icon:(UIImage*) icon font:(UIFont *)font title:(NSString *)title target:(id)target selector:(SEL)selector;
@end
