//
//  CommentsViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "ChatMessageView.h"
#import "EventDetailViewController.h"

@interface CommentsViewController : EventDetailViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) Event *event;
@property (nonatomic) UITapGestureRecognizer *tapEvent;
@property (weak, nonatomic) IBOutlet UITableView *_tableView;
@property (weak, nonatomic) IBOutlet ChatMessageView *chatInput;
-(void) enableTapEvent;
-(void) disableTapEvent;
@end
