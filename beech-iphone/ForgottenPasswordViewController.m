//
//  ForgottenPasswordViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/7/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "ForgottenPasswordViewController.h"
#import "RequestHelper.h"
#import "UIBarButtonItem+StyledButton.h"

@interface ForgottenPasswordViewController ()
{
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UIBarButtonItem *submitButton;
}
@end

@implementation ForgottenPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationItem setTitle:NSLocalizedString(@"Forgotten password", @"")];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem styledSubmitBarButtonItemWithTitle:NSLocalizedString(@"Submit", @"") target:self selector:@selector(submitButton:)];

    [emailTextField setPlaceholder:NSLocalizedString(@"Email", @"")];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    emailTextField = nil;
    submitButton = nil;
    [super viewDidUnload];
}
- (IBAction)submitButton:(id)sender {
    NSURL *url = [RequestHelper createURL:@"users/password" withPrefix:@"/"];
    ASIFormDataRequest *_request = [RequestHelper prepareFormRequestWithUrl:url withParams:@{}];
    __weak ASIFormDataRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        SEL theSelector;
        NSLog(@"status : %d", [request responseStatusCode]);
        if ([request responseStatusCode] == 200 || [request responseStatusCode] == 201) {
        
            NSData *data = [request responseData];
            id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSLog(@"%@",json);\
            
            theSelector = NSSelectorFromString(@"successAction");

        } else
        {
            theSelector = NSSelectorFromString(@"failAction");
        }
        NSInvocation *anInvocation = [NSInvocation
                                      invocationWithMethodSignature:
                                      [ForgottenPasswordViewController instanceMethodSignatureForSelector:theSelector]];
        
        [anInvocation setSelector:theSelector];
        [anInvocation setTarget:self];
        
        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.25];
    }];
    [request setFailedBlock:^{
        SEL theSelector = NSSelectorFromString(@"failAction");
        NSInvocation *anInvocation = [NSInvocation
                                      invocationWithMethodSignature:
                                      [ForgottenPasswordViewController instanceMethodSignatureForSelector:theSelector]];
        
        [anInvocation setSelector:theSelector];
        [anInvocation setTarget:self];
        
        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.25];
        
    }];
    [request setRequestMethod:@"POST"];
    [request setPostValue:[emailTextField text] forKey:@"user[email]"];
    [request startAsynchronous];
}

- (void) successAction
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Thanks !", @"")
                                                      message:NSLocalizedString(@"Check your mails", @"")
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
    NSLog(@"should go back to login");
}

- (void) failAction
{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error !", @"")
                                                      message:NSLocalizedString(@"Check the email address", @"")
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
}

@end
