//
//  BadgeDetailViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 20/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BadgeDetailViewController : UIViewController
-(id) initWithNothing;
@property (weak, nonatomic) IBOutlet UILabel *badgeName;
@property (weak, nonatomic) IBOutlet UILabel *badgeIndex;
@property (weak, nonatomic) IBOutlet UIView *rightBorder;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *badgeImage;
@property (weak, nonatomic) IBOutlet UILabel *badgeDescription;
@property (weak, nonatomic) IBOutlet UIView *descriptionBackground;
@property (nonatomic, assign) id delegate;
@end
