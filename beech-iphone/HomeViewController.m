//
//  HomeViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 19/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()
{
    __weak IBOutlet UIButton *loginButton;
    __weak IBOutlet UIButton *signupButton;
    __weak IBOutlet UIImageView *backgroundImage;
}
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    self.navigationItem.title = NSLocalizedString(@"How is it going dude ?", @"");
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", @"")
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
    loginButton.frame = CGRectMake(loginButton.frame.origin.x, self.view.frame.size.height - 200, loginButton.frame.size.width, loginButton.frame.size.height);
    [loginButton setTitle:NSLocalizedString(@"Sign in", @"") forState:UIControlStateNormal];
//    [loginButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    signupButton.frame = CGRectMake(signupButton.frame.origin.x, self.view.frame.size.height - 140, signupButton.frame.size.width, signupButton.frame.size.height);
    [signupButton setTitle:NSLocalizedString(@"Sign up", @"") forState:UIControlStateNormal];
    [loginButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:20.0]];
    [signupButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Regular" size:20.0]];
    [UIFont fontWithName:@"ProximaNova-Light" size:34.0];
    
    if (self.view.frame.size.height > 460)
    {
        backgroundImage.image = [UIImage imageNamed:@"login-bg-5"];
    } else
    {
        backgroundImage.image = [UIImage imageNamed:@"login-bg"];
    }
//    backgroundImage.frame = CGRectMake(0, -40, 320, 568);
//    backgroundImage.image = [UIImage imageNamed:@"login-bg-5"];

}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    loginButton = nil;
    signupButton = nil;
    backgroundImage = nil;
    [super viewDidUnload];
}
@end
