//
//  BeechModel.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/20/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "FMDatabase.h"

@interface BeechModel : MTLModel <MTLJSONSerializing>
- (BOOL) saveWithDB:(FMDatabase*)db;
@end
