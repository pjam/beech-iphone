//
//  BadgeCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 15/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeechCell.h"

@interface BadgeCell : BeechCell
@property(nonatomic) UILabel *badgeName;
@property(nonatomic) UILabel *badgeCount;
@property(nonatomic) UILabel *badgeDesc;
@property(nonatomic) UIImageView *badgeImageView;
@end
