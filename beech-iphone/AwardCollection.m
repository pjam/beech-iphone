//
//  AwardCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "AwardCollection.h"

@implementation AwardCollection
- (Award*)awardForId:(NSNumber *)award_id
{
    NSUInteger awardIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id award, NSUInteger idx,BOOL *stop)
     {
         return [((Award*)award).award_id intValue] == [award_id intValue];
     }];
    return [self.collection objectAtIndex:awardIndex];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Award *newAward = [MTLJSONAdapter modelOfClass:Award.class fromJSONDictionary:item error:nil];
        [self addObject:newAward];
    }
}

- (BOOL)addObject:(id)anObject
{
    NSUInteger eventIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id award, NSUInteger idx,BOOL *stop)
     {
         return [((Award*)award).award_id intValue] == [((Award*)anObject).award_id intValue];
     }];

    if (eventIndex == NSNotFound) {
        NSUInteger newIndex = [self.collection indexOfObject:anObject
                                               inSortedRange:(NSRange){0, [self.collection count]}
                                                     options:NSBinarySearchingInsertionIndex
                                             usingComparator:^NSComparisonResult(id a, id b) {
                                                 NSNumber *first = [(Award*)a created_at];
                                                 NSNumber *second = [(Award*)b created_at];
                                                 return [second compare:first];
                                             }];
        [self.collection insertObject:anObject atIndex:newIndex];
        return YES;
    } else {
        return NO;
    }
}
@end
