//
//  Check.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Check.h"

@implementation Check
@synthesize beer_id, check_id, user_id, created_at;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"check_id": @"id",
             @"beer_id": @"beer_id",
             @"created_at": @"created_at",
             @"user_id": @"user_id",                          
             };
}

- (BOOL)isEqualToCheck:(Check *)check {
    return check.check_id == self.check_id;
}

- (void)setBeer:(Beer *)abeer
{
    self->_beer = abeer;
    self.beer_id = abeer.beer_id;
}

@end
