//
//  Award.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Award.h"

@implementation Award

@synthesize user_id, award_id, badge, badge_id, created_at;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"award_id": @"id",
             @"badge_id": @"badge_id",
             @"created_at": @"created_at",
             @"user_id": @"user_id",
             };
}

@end
