//
//  BeechModalViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "BeechModalViewController.h"

@interface BeechModalViewController ()
@property (weak, nonatomic) IBOutlet UINavigationBar *_title;
@property (weak, nonatomic) IBOutlet UIView *_view;
@end

@implementation BeechModalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self set_title:nil];
    [self set_view:nil];
    [super viewDidUnload];
}
@end
