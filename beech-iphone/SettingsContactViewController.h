//
//  SettingsContactViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 09/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface SettingsContactViewController : UIViewController <UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, assign) id delegate;
@property (nonatomic) User* user;
@end
