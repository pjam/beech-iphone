//
//  BeerCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Beer.h"

@interface BeerCollection : Collection
-(Beer*) beerForId:(NSNumber*) beer_id;
+(void) syncWithServer;
-(void) loadFromDB;
-(void) search:(NSString*) query;
-(NSUInteger) numberOfBeersStartingWith:(NSString*)prefix;
-(NSArray*) beersStartingWith:(NSString*)prefix;
@end
