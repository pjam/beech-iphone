//
//  UserCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 25/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeechCell.h"
#import "FollowButton.h"

@interface UserCell : BeechCell
@property FollowButton *followButton;
@end
