//
//  LikeCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Like.h"

@interface LikeCollection : Collection
-(Like*) likeForId:(NSNumber*) like_id;
-(NSMutableArray*) likesForEventId:(NSNumber*) event_id;
@end
