//
//  SqliteDB.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/20/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface SqliteDB : NSObject
+ (void) createDB;
+ (void) clearDB;
+ (FMDatabase*) DB;
@end
