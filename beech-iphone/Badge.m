//
//  Badge.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Badge.h"

@implementation Badge

@synthesize badge_id, name, description, photo_url;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"badge_id": @"id",
             @"name": @"name",
             @"description": @"description",
             @"photo_url": @"photo_url",             
             };
}

@end
