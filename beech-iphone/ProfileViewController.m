//
//  ProfileViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "ProfileViewController.h"
#import "FeedCell.h"
#import "BeechCell.h"
#import "BadgeCell.h"
#import "BeechHelper.h"
#import "RequestHelper.h"
#import "ColorHelper.h"
#import "SettingsViewController.h"
#import "FollowButton.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "UIColor+HexString.h"
#import "UIBarButtonItem+StyledButton.h"

@interface ProfileViewController ()
{
    UIView *loadingView;
    UIImage *avatar;
    NSDictionary *myProfile;
    IBOutlet UITableView *_tableView;
    __weak IBOutlet UIScrollView *_badgesView;
    IBOutlet UILabel *nameLabel;
    UILabel *titleLabel;
    NSMutableArray *badgesBanner;
    BOOL alreadyLoaded;
    UIView *footer;

    __weak IBOutlet UIView *followingsView;
    __weak IBOutlet UIView *followersView;
    __weak IBOutlet UIView *checksView;
    __weak IBOutlet UILabel *followerTitleLabel;
    __weak IBOutlet UILabel *followerCountLabel;
    __weak IBOutlet UILabel *followingTitleLabel;
    __weak IBOutlet UILabel *followingCountLabel;
    __weak IBOutlet UILabel *checkTitleLabel;
    __weak IBOutlet UILabel *checkCountLabel;
    __weak IBOutlet UIView *labelsBackgroundView;
    __weak IBOutlet UIView *headerBackgroundView;
    __weak IBOutlet UIActivityIndicatorView *spinner;
    __weak IBOutlet UIImageView *userAvatar;
    FollowButton *followButton;
    __weak IBOutlet UINavigationItem *profileTitle;
    __weak IBOutlet UIScrollView *badgesScrollView;

    __weak IBOutlet UILabel *banner;
    __weak IBOutlet UIView *viewTitle;
}
@end

@implementation ProfileViewController

{
    PullToRefreshView *pull;
}

@synthesize user_id;
@synthesize forceRefresh;
@synthesize loadingAnteriorData;

- (void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view;
{
    [self reloadTableData];
}

-(void) reloadTableData
{
    // call to reload your data
    [self refresh];
    [_tableView reloadData];
}

-(void) refresh
{
    NSLog(@"Refreshing");
    NSURL *url;
    if ([self onCurrentUserProfile])
    {
        url = [RequestHelper createURL:@"/my/profile" withPrefix:@"/api"];
    } else
    {
        url = [RequestHelper createURL:[NSString stringWithFormat:@"/users/%@/profile", user_id] withPrefix:@"/api"];
    }

    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *personalInfo = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        int newEvents = [self loadDataWithDictionary:personalInfo Reversed:NO];

        [self loadProfileData:personalInfo];

        [self loadBadges];
        if (newEvents > 0) {
            [UIView transitionWithView: _tableView
                              duration: 0.35f
                               options: UIViewAnimationOptionTransitionCrossDissolve
                            animations: ^(void)
             {
                 [_tableView reloadData];
             }
                            completion: ^(BOOL isFinished)
             {
                 /* TODO: Whatever you want here */
             }];

        } else {
            [_tableView reloadData];
        }
        [pull finishedLoading];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];


}
- (BOOL) onCurrentUserProfile
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];

    if (user_id == nil) {
        return YES;
    } else
    {
        return ([[defaults valueForKey:@"user_id"] isEqualToNumber:user_id]);
    }
}

- (void) setRegularFooter
{
    footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 9)];
    [_tableView setTableFooterView:footer];
}

- (void) setLoadingFooter
{
    footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 59)];


    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }

    loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 308, 59)];
    [loadingView setBackgroundColor:[UIColor clearColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(135,8,42,42)];

    [loadingView addSubview:anImageView];

    [footer addSubview:loadingView];

    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];

    [_tableView setTableFooterView:footer];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clear) name:@"logout-success" object:nil];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.88f green:0.88f blue:0.85f alpha:1.00f]];

    alreadyLoaded = NO;

    [self.navigationItem setTitle:NSLocalizedString(@"My profile", @"")];

    spinner.hidesWhenStopped = YES;
    [_badgesView setScrollsToTop:NO];
    CGRect rect = badgesScrollView.frame;
    badgesScrollView.frame = CGRectMake(rect.origin.x, rect.origin.y, self.view.frame.size.width, badgesScrollView.frame.size.height);

    badgesBanner = [[NSMutableArray alloc] init];

    [banner setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:10.0]];

    self.forceRefresh = YES;

    // ???
    UIImage *i = [UIImage imageNamed:@"profile-stats-bg"];
    UIImageView *foo = [[UIImageView alloc] initWithImage:i];
    [foo setFrame:CGRectMake(0, 0, 320, 50)];
    [labelsBackgroundView addSubview:foo];

    checkTitleLabel.textColor = [ColorHelper greyColor];
    checkTitleLabel.text = NSLocalizedString(@"Beechs", @"");
    checkTitleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13.0];
    followerTitleLabel.textColor = [ColorHelper greyColor];
    followerTitleLabel.text = NSLocalizedString(@"Followers", @"");
    followerTitleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13.0];
    followingTitleLabel.textColor = [ColorHelper greyColor];
    followingTitleLabel.text = NSLocalizedString(@"Followings", @"");
    followingTitleLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:13.0];

    checkCountLabel.textColor = [ColorHelper blueColor];
    checkCountLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:16.0];
    followerCountLabel.textColor = [ColorHelper blueColor];
    followerCountLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:16.0];
    followingCountLabel.textColor = [ColorHelper blueColor];
    followingCountLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:16.0];

    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = UITextAlignmentLeft;
    nameLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:18.0];

    [labelsBackgroundView sendSubviewToBack:foo];

    UITapGestureRecognizer *badgesTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_badgesView addGestureRecognizer:badgesTapGesture];

    UITapGestureRecognizer *checksTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(checksTrigger:)];
    [checksView addGestureRecognizer:checksTapGesture];

    UITapGestureRecognizer *followerTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(followersTrigger:)];
    [followersView addGestureRecognizer:followerTapGesture];

    UITapGestureRecognizer *followingTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(followingsTrigger:)];
    [followingsView addGestureRecognizer:followingTapGesture];

    followButton = [[FollowButton alloc] initWithOffsetX:-6];
    [headerBackgroundView addSubview:followButton];
    [followButton setHidden:YES];
    if ([self onCurrentUserProfile])
    {
//        UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        UIImage *settingsButtonImage = [UIImage imageNamed:@"navbar-button-settings"];
//        //    UIImage *backBtnImagePressed = [UIImage imageNamed:@"btn-back-pressed"];
//        [settingsButton setBackgroundImage:settingsButtonImage forState:UIControlStateNormal];
//        //    [backBtn setBackgroundImage:backBtnImagePressed forState:UIControlStateHighlighted];
//        [settingsButton addTarget:self action:@selector(showSettings) forControlEvents:UIControlEventTouchUpInside];
//        settingsButton.frame = CGRectMake(2, 20, 44, 44);
//        UIView *settingsButtonView = [[UIView alloc] initWithFrame:CGRectMake(2, 20, 44, 44)];
//        settingsButtonView.bounds = CGRectOffset(settingsButtonView.bounds, 6, 20);
//        [settingsButtonView addSubview:settingsButton];
//        UIBarButtonItem *settingsBarButton = [[UIBarButtonItem alloc] initWithCustomView:settingsButtonView];
//        self.navigationItem.leftBarButtonItem = settingsBarButton;

        UIImage *icon = [UIImage imageNamed:@"icon-settings-default"];
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem styledBackBarButtonItemWithTarget:self selector:@selector(showSettings) icon:icon];

        titleLabel.text = NSLocalizedString(@"My profile", @"");

    } else
    {
//        [followButton setHidden:NO];
        [followButton addTarget:self action:@selector(touchFollowButton:) forControlEvents:UIControlEventTouchUpInside];
        titleLabel.text = @"";
    }

}

- (IBAction)touchFollowButton:(id)sender
{
    [sender setSelected:![sender isSelected]];


    NSString *httpMethod = [sender isSelected] ? @"POST" : @"DELETE";

    NSString *urlString = @"/my/followings";

    NSURL *url = [RequestHelper createURL:urlString withPrefix:@"/api"];
    NSDictionary *args = @{@"user_id": user_id};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];

        id json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSLog(@"%@", json);
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
    }];

    [request setRequestMethod:httpMethod];
    [request startAsynchronous];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (![self onCurrentUserProfile] && !alreadyLoaded) {
        [self.navigationItem setTitle:@""];
    }

    if (alreadyLoaded)
    {
        [self refresh];
    } else
    {
        [nameLabel setText:@""];
        [checkCountLabel setText:@""];
        [followerCountLabel setText:@""];
        [followingCountLabel setText:@""];
        [self fetchContent];
    }
    forceRefresh = NO;

}

- (void)viewDidAppear:(BOOL)animated
{
}

- (void) fetchContent
{
    NSURL *url;
    if ([self onCurrentUserProfile])
    {
        url = [RequestHelper createURL:@"/my/profile" withPrefix:@"/api"];
    } else
    {
        url = [RequestHelper createURL:[NSString stringWithFormat:@"/users/%@/profile", user_id] withPrefix:@"/api"];
    }

    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:@{}];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *personalInfo = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        [self loadDataWithDictionary:personalInfo Reversed:NO];

        [self loadProfileData:personalInfo];

        // Load badges
        [self loadBadges];

        [loadingView setHidden:YES];
        alreadyLoaded = YES;
        [_tableView reloadData];
    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
        [loadingView setHidden:YES];
    }];
    [request setRequestMethod:@"GET"];
    [request startAsynchronous];

    // Set loading
    int numberOfFrames = 29;
    NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
    for (int i=1; numberOfFrames > i; ++i)
    {
        [imagesArray addObject:[UIImage imageNamed:
                                [NSString stringWithFormat:@"loader@2x-%d (glissées).tiff", i]]];
    }

    loadingView = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 308, 800)];
    [loadingView setBackgroundColor:[UIColor whiteColor]];
    UIImageView *anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(135,240,42,42)];
    UILabel *loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 270, 308, 44)];
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.textAlignment = UITextAlignmentCenter;
    loadingLabel.textColor = [ColorHelper greyColor];
    loadingLabel.text = NSLocalizedString(@"Loading ...", @"");
    loadingLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14.0];

    [loadingView addSubview:loadingLabel];
    [loadingView addSubview:anImageView];

    [self.view insertSubview:loadingView atIndex:1];
    anImageView.animationImages = imagesArray;
    anImageView.animationDuration = 2;
    [anImageView startAnimating];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];

    //Do stuff here...
    [self performSegueWithIdentifier:@"BadgesSegue" sender:self];
}

- (void)viewDidUnload
{
    _tableView = nil;
    [self setView:nil];
    nameLabel = nil;
    checkCountLabel = nil;
    checkTitleLabel = nil;
    followingCountLabel = nil;
    followingTitleLabel = nil;
    followerCountLabel = nil;
    followerTitleLabel = nil;
    checksView = nil;
    followersView = nil;
    followingsView = nil;
    labelsBackgroundView = nil;
    headerBackgroundView = nil;
    spinner = nil;
    userAvatar = nil;
    followButton = nil;
    profileTitle = nil;
    banner = nil;
    viewTitle = nil;
    _badgesView = nil;
    badgesScrollView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)showSettings
{
    [self performSegueWithIdentifier:@"SettingsSegue" sender:self];
}

- (void)checksTrigger:(UITapGestureRecognizer *)recognizer
{
    [self performSegueWithIdentifier:@"ChecksSegue" sender:self];
}

- (void)followersTrigger:(UITapGestureRecognizer *)recognizer
{
    [self performSegueWithIdentifier:@"FollowersSegue" sender:self];
}

- (void)followingsTrigger:(UITapGestureRecognizer *)recognizer
{
    [self performSegueWithIdentifier:@"FollowingsSegue" sender:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.events count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *FeedIdentifier = @"FeedCell2";
    static NSString *BadgeIdentifier = @"BadgeCell2";

    BeechCell *cell;

    UIImage *tappedImage = [UIImage imageNamed:@"bg-list-index-pressed"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:tappedImage];

    Event *event = [self.events objectAtIndex:indexPath.row];

    if ([event isAward]) {
        cell = (BadgeCell*)[tableView dequeueReusableCellWithIdentifier:BadgeIdentifier];

        if (cell == nil) {
            cell = [[BadgeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:BadgeIdentifier];
            [[[cell subviews] objectAtIndex:0] setTag:111];
        }
        ((BadgeCell*)cell).delegate = self;
        ((BadgeCell*)cell).eventId = event.event_id;

        cell.beer.text = NSLocalizedString(@"Just earned a badge", @"");

        NSLog(@"badgeName : %@", ((BadgeCell*)cell).badgeDesc );
        [((BadgeCell*)cell).badgeName setText:event.award.badge.name];
        ((BadgeCell*)cell).badgeDesc.text = event.award.badge.description;
        ((BadgeCell*)cell).badgeCount.text = [NSString stringWithFormat:@"BADGE #%@", event.award.badge.badge_id];

        NSString *imageStringUrl = [BeechHelper validURLStringWithString:event.award.badge.photo_url[@"url"]];
        NSURL *badgeImageUrl = [NSURL URLWithString:imageStringUrl];

        [((BadgeCell*)cell).badgeImageView setImageWithURL:badgeImageUrl placeholderImage:[UIImage imageNamed:@"profile-badge-empty"]];

    } else if ([event isCheck])
    {
        cell = (FeedCell*)[tableView dequeueReusableCellWithIdentifier:FeedIdentifier];

        if (cell == nil) {
            cell = [[FeedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FeedIdentifier];
            [[[cell subviews] objectAtIndex:0] setTag:111];
        }

        ((FeedCell*)cell).delegate = self;
        ((FeedCell*)cell).eventId = event.event_id;
        ((FeedCell*)cell).colorPattern = [NSNumber numberWithInt:[event.check.beer_id intValue] % 3];
        cell.beer.text = event.check.beer.name;

        // Set like button state

        ((FeedCell*)cell).beer.text = event.check.beer.name;
        [((FeedCell*)cell) setDetailsWithBeer:event.check.beer];
        [((FeedCell*)cell).beer setTextColor:[UIColor colorWithHexString:event.check.beer.font_color]];
        [((FeedCell*)cell).cartouche setBackgroundColor:[UIColor colorWithHexString:event.check.beer.background_color]];
    }

    if ([event is_liked]) {
        [cell.likeButton setSelected:YES];
    } else
    {
        [cell.likeButton setSelected:NO];
    }

    // Set comments
    [cell setCommentCount:[[event comments] count]];

    // Set likes
    [cell setLikeCountWithEvent:event];

    NSLog(@"%d", cell.isHidden);

    cell.name.text = event.user.nickname;
    [cell setHidden:NO];

    NSString *imageStringUrl = [BeechHelper validURLStringWithString:[[event.user.avatar_url objectForKey:@"thumb"] objectForKey:@"url"]];

    NSURL *imageUrl = [NSURL URLWithString:imageStringUrl];

    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];

    cell.time.text = [BeechHelper stringForTimestamp:event.created_at serverTime:[NSDate date]];


    if (indexPath.row == [self.events count] - 1)
    {
        NSLog(@"should reload");
        if ([self loadingAnteriorData] == NO){
            [self setLoadingAnteriorData:YES];
            // last item
            NSLog(@"feed : %@", self.events);
            NSLog(@"feed : %d", [[[self.events lastObject] created_at] intValue]);
            [self setLoadingFooter];
            [self loadDataAfter: [[[self.events lastObject] created_at] intValue]];
        }
    }
    
    return cell;

//    static NSString *FeedIdentifier = @"FeedCell";
//    static NSString *BadgeIdentifier = @"BadgeCell";
//
//    BeechCell *cell;
//
//    Event *event = [self.events objectAtIndex:indexPath.row];
//
//    if ([event isAward]) {
//        cell = (BadgeCell*)[tableView dequeueReusableCellWithIdentifier:BadgeIdentifier];
//
//        if (cell == nil)
//        {
//            cell = [[BadgeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:BadgeIdentifier];
//            [[[cell subviews] objectAtIndex:0] setTag:111];
//        }
//
//        ////// TODO : Translate that
//        cell.beer.text = NSLocalizedString(@"Just earned a badge", @"");
//        ((BadgeCell*)cell).badgeName.text = event.award.badge.name;
//        ((BadgeCell*)cell).badgeDesc.text = event.award.badge.description;
//
//        ((BadgeCell*)cell).badgeCount.text = [NSString stringWithFormat:@"BADGE #%@", event.award.badge.badge_id];
//
//        NSURL *badgeImageUrl = [NSURL URLWithString:[event.award.badge.photo_url objectForKey:@"url"]];
//
//        [((BadgeCell*)cell).badgeImageView setImageWithURL:badgeImageUrl placeholderImage:[UIImage imageNamed:@"profile-badge-empty"]];
//
//    } else
//    {
//        cell = (FeedCell*)[tableView dequeueReusableCellWithIdentifier:FeedIdentifier];
//
//        if (cell == nil)
//        {
//            cell = [[FeedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FeedIdentifier];
//            [[[cell subviews] objectAtIndex:0] setTag:111];
//        }
//
//        cell.beer.text = event.check.beer.name;
//        ((FeedCell*)cell).delegate = self;
//        ((FeedCell*)cell).eventId = event.event_id;
//        ((FeedCell*)cell).colorPattern = [NSNumber numberWithInt:[event.check.beer_id intValue] % 3];
//
//        // Set like button state
//        if ([event is_liked]) {
//            [((FeedCell*)cell).likeButton setSelected:YES];
//        } else
//        {
//            [((FeedCell*)cell).likeButton setSelected:NO];
//        }
//
//        // Set comments
//        [((FeedCell*)cell) setCommentCount:[[event comments] count]];
//
//        // Set likes
//        [((FeedCell*)cell) setLikeCountWithEvent:event];
//
//
//    }
//
//    cell.name.text = [myProfile objectForKey:@"nickname"];
//
//    NSString *imageStringUrl = [BeechHelper validURLStringWithString:[[[myProfile objectForKey:@"avatar_url"]objectForKey:@"thumb" ] objectForKey:@"url" ]];
//
//    NSURL *imageUrl = [NSURL URLWithString:imageStringUrl];
//
//
//    [cell.avatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];
//
//    cell.time.text = [BeechHelper stringForTimestamp:event.created_at serverTime:[NSDate date]];
//
//    if (indexPath.row == [self.events count] - 1)
//    {
//        if ([self loadingAnteriorData] == NO){
//            [self setLoadingAnteriorData:YES];
//            // last item
//            [self setLoadingFooter];
//            [self loadDataAfter:[[[self.events lastObject] created_at] intValue]];
//        }
//    }
//
//    return cell;

}

- (void)loadDataAfter: (int)timestamp
{
    // TODO : Stop loading if connection fails
    NSURL *url;
    if ([self onCurrentUserProfile])
    {
        NSString *str = @"/my/feed";
        url = [RequestHelper createURL:str withPrefix:@"/api"];
    } else
    {
        NSString *str = [NSString stringWithFormat:@"/users/%@/feed", user_id];
        url = [RequestHelper createURL:[NSString stringWithFormat:str, user_id] withPrefix:@"/api"];
    }

    NSDictionary *args = @{@"before": [NSNumber numberWithInt:timestamp], @"users": @"me"};
    ASIHTTPRequest *_request = [RequestHelper prepareRequestWithUrl:url withParams:args];
    __weak ASIHTTPRequest *request = _request;
    [request setCompletionBlock:^{
        // Use when fetching binary data
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        [self loadDataWithDictionary:json Reversed:NO];

        NSArray *jsonChecks = [json objectForKey:@"checks"];
        NSArray *jsonAwards = [json objectForKey:@"awards"];

        if ((jsonChecks && [jsonChecks count] > 0) || (jsonAwards && [jsonAwards count] > 0))
        {
            [_tableView reloadData];
            [self setLoadingAnteriorData:NO];
        }
        [loadingView setHidden:YES];
        [self setRegularFooter];

    }];
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"%@", error);
        [BeechHelper redirectToLoginScreenWithRequest:request andTabController:[[self parentViewController] parentViewController]];
        [self setLoadingAnteriorData:NO];
        [self setRegularFooter];
    }];
    [request setRequestMethod:@"GET"];

    [request startAsynchronous];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        int globalOffset = 150;
        float contentOffset = scrollView.contentOffset.y + globalOffset;
        if (contentOffset > 0)
        {
            labelsBackgroundView.transform = CGAffineTransformMakeTranslation(0, -contentOffset);
            if (scrollView.contentOffset.y < -13)
            {
                viewTitle.transform = CGAffineTransformMakeTranslation(0, -contentOffset);
            } else
            {
                viewTitle.transform = CGAffineTransformMakeTranslation(0, -137);
            }
        } else {
            labelsBackgroundView.transform = CGAffineTransformIdentity;
            viewTitle.transform = CGAffineTransformIdentity;
        }
    }

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == badgesScrollView) {
        NSLog(@"here");
//        int currentPage = floor((self.scrollView.contentOffset.x - self.scrollView.frame.size.width / ([slideImages count]+2)) / self.scrollView.frame.size.width) + 1;
//        if (currentPage==0) {
//            //go last but 1 page
//            [self.scrollView scrollRectToVisible:CGRectMake(WIDTH_OF_IMAGE * [slideImages count],0,WIDTH_OF_IMAGE,HEIGHT_OF_IMAGE) animated:NO];
//        } else if (currentPage==([slideImages count]+1)) {
//            [self.scrollView scrollRectToVisible:CGRectMake(WIDTH_OF_IMAGE,0,WIDTH_OF_IMAGE,HEIGHT_OF_IMAGE) animated:NO];
//        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.events count] - 1) {
        return 230.0;
    } else {
        return 210.0;
    }
}

- (void) clear
{
    [super clear];
    [nameLabel setText:[myProfile objectForKey:@""]];
    [_tableView reloadData];
    [self setLoadingAnteriorData:NO];
    for (UIImageView *badgeItem in badgesBanner) {
        badgeItem.image = [UIImage imageNamed:@"profile-badge-empty"];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"SettingsSegue"])
    {
        // Get reference to the destination view controller
        SettingsViewController *vc = [segue destinationViewController];

        NSLog(@"Hei : %@", vc);
        // Pass any objects to the view controller here, like...
        NSError *err = nil;
        User *user = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:myProfile error:&err];
        [vc setUser:user];
    }
}

- (void) loadProfileData:(NSDictionary*) data
{
    myProfile = [data objectForKey:@"profile"];
    [nameLabel setText:[myProfile objectForKey:@"nickname"]];
    if (![self onCurrentUserProfile]) {
        [self.navigationItem setTitle:[myProfile objectForKey:@"nickname"]];
    }
    [checkCountLabel setText: [NSString stringWithFormat:@"%@", [myProfile objectForKey:@"check_count"]]];
    [followerCountLabel setText:[NSString stringWithFormat:@"%@", [myProfile objectForKey:@"follower_count"]]];
    [followingCountLabel setText:[NSString stringWithFormat:@"%@", [myProfile objectForKey:@"following_count"]]];

    //        NSURL *imageUrl = [NSURL URLWithString:[myProfile objectForKey:@"avatar_url"]];
    NSString *imageStringUrl = [BeechHelper validURLStringWithString:[[[myProfile objectForKey:@"avatar_url"]objectForKey:@"thumb" ] objectForKey:@"url" ]];
    NSURL *imageUrl = [NSURL URLWithString:imageStringUrl];

    [userAvatar setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"avatar-4"]];
    userAvatar.layer.masksToBounds = YES;
    userAvatar.layer.cornerRadius = 3.0f;

    if ([self onCurrentUserProfile] == NO)
    {
        if ([[myProfile objectForKey:@"already_following"] intValue] == 1)
        {
            followButton.selected = YES;
        }
        [followButton setHidden:NO];
        titleLabel.text = [myProfile objectForKey:@"nickname"];
    }
}

- (void) loadBadges
{

    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"badge_id" ascending:NO];
    NSArray *sortedBadges = [self.badges sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];

    int i;
    int minCount;
//    sortedBadges = @[];
    minCount = MAX(5, [sortedBadges count]);
    int badgeWidth = 49;
    int margin = 10;


    for (UIImageView *imv in [badgesScrollView subviews]) {
        [imv removeFromSuperview];
    }
    for (i = 0; i < minCount; i++)
    {
        UIImageView *badgeItem = [[UIImageView alloc] initWithFrame:CGRectMake(margin + i * (badgeWidth + margin), (badgesScrollView.frame.size.height / 2) - (badgeWidth / 2), badgeWidth, badgeWidth)];
        if ([sortedBadges count] > i) {
            Badge *badge = [sortedBadges objectAtIndex:i];

            NSString *imageName = [[badge.photo_url objectForKey:@"thumb"] objectForKey:@"url"];
            NSString *urlImageName = [BeechHelper validURLStringWithString:imageName];
            NSURL *imageUrl = [NSURL URLWithString:urlImageName];

            [badgeItem setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"profile-badge-empty"]];

        } else {
            [badgeItem setImageWithURL:[NSURL URLWithString:@"profile-badge-empty" ] placeholderImage:[UIImage imageNamed:@"profile-badge-empty"]];
        }
        [badgesBanner addObject:badgeItem];

        [badgesScrollView addSubview:badgeItem];
    }
    [badgesScrollView setContentSize:CGSizeMake(20 + margin + [self.badges count] * (badgeWidth + margin), badgesScrollView.frame.size.height)];

}


@end
