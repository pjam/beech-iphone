//
//  Beer.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "BeerColor.h"
#import "BeechModel.h"

@interface Beer : BeechModel
@property(nonatomic) NSNumber *beer_id;
@property(nonatomic) NSNumber *color_pattern;
@property(nonatomic) NSString *name;
@property(nonatomic) NSString *country;
@property(nonatomic) NSString *font_color;
@property(nonatomic) NSString *background_color;
@property(nonatomic) BeerColor* beer_color;
- (BOOL)saveWithDB:(FMDatabase *)db forceInsert:(BOOL) isForce;
+ (Beer*)find:(NSNumber*)beer_id withDB:(FMDatabase*)db;
@end
