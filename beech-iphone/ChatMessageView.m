//
//  MessageView.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/14/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "ChatMessageView.h"
#import "ColorHelper.h"
#import <QuartzCore/QuartzCore.h>
#import "CommentsViewController.h"

#define TABBAR_HEIGHT 49

@implementation ChatMessageView

@synthesize keyboardHeight;

- (void) composeView {

    CGSize size = self.frame.size;

    // Input
	_inputBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
	_inputBackgroundView.autoresizingMask = UIViewAutoresizingNone;
    _inputBackgroundView.contentMode = UIViewContentModeScaleToFill;
	//_inputBackgroundView.userInteractionEnabled = YES;
    //_inputBackgroundView.alpha = .5;
    _inputBackgroundView.backgroundColor = [UIColor clearColor];
    //_inputBackgroundView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:.5];
	[self addSubview:_inputBackgroundView];

	// Text field
	_textView = [[UITextView alloc] initWithFrame:CGRectMake(10.0f, 0, 230, 2)];
    _textView.backgroundColor = [UIColor whiteColor];
    //_textView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:.3];
	_textView.delegate = self;
    _textView.contentInset = UIEdgeInsetsMake(-4, -2, -4, 0);
    _textView.showsVerticalScrollIndicator = NO;
    _textView.showsHorizontalScrollIndicator = NO;
//	_textView.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14];
    [_textView setTextColor:[UIColor colorWithRed:0.40f green:0.40f blue:0.40f alpha:1.00f]];
    _textView.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16.0];

//    _textView.textColor = [ColorHelper blackColor];
    _textView.layer.cornerRadius = 3.0f;
    _textView.layer.borderColor = [[UIColor colorWithRed:0.64f green:0.64f blue:0.64f alpha:1.00f] CGColor];
    _textView.layer.borderWidth = 1.0f;
	[self addSubview:_textView];

    [self adjustTextInputHeightForText:@"" animated:NO];

    _lblPlaceholder = [[UILabel alloc] initWithFrame:CGRectMake(18.0f, 14, 160, 20)];
    _lblPlaceholder.font = [UIFont fontWithName:@"ProximaNova-Regular" size:14];
    _lblPlaceholder.text = NSLocalizedString(@"Type here...", nil);
    _lblPlaceholder.textColor = [UIColor lightGrayColor];
    _lblPlaceholder.backgroundColor = [UIColor clearColor];
	[self addSubview:_lblPlaceholder];

	// Send button
	_sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
	_sendButton.frame = CGRectMake(size.width - 65.0f, 11.0f, 58.0f, 27.0f);
	_sendButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	_sendButton.titleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:14];
    [_sendButton setBackgroundColor:[UIColor colorWithRed:0.00f green:0.74f blue:0.53f alpha:1.00f]];
    _sendButton.layer.cornerRadius = 2.0f;
    _sendButton.layer.borderWidth = 1.0f;
    _sendButton.layer.borderColor = [[UIColor colorWithRed:0.00f green:0.49f blue:0.33f alpha:1.00f] CGColor];
	[_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sendButton addTarget:self action:@selector(sendButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:_sendButton];

    _sendingComment = [[UIView alloc] initWithFrame:CGRectMake(size.width - 65.0f, 11.0f, 58.0f, 27.0f)];
    [_sendingComment setBackgroundColor:[UIColor colorWithRed:0.00f green:0.74f blue:0.53f alpha:0.60f]];
    _sendingComment.layer.cornerRadius = 2.0f;
    _sendingComment.layer.borderWidth = 1.0f;
    _sendingComment.layer.borderColor = [[UIColor colorWithRed:0.00f green:0.49f blue:0.33f alpha:1.00f] CGColor];

    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityView.frame = CGRectMake(0, 0.0, _sendingComment.frame.size.width, _sendingComment.frame.size.height);
    [activityView setColor:[UIColor whiteColor]];
    [activityView startAnimating];
    [_sendingComment addSubview:activityView];
    [_sendingComment setHidden:YES];
    [self addSubview:_sendingComment];

    [self sendSubviewToBack:_inputBackgroundView];
}

- (void) awakeFromNib {

    _inputHeight = 38.0f;
    _inputHeightWithShadow = 44.0f;
    _autoResizeOnKeyboardVisibilityChanged = YES;

    [self composeView];
}

- (void) adjustTextInputHeightForText:(NSString*)text animated:(BOOL)animated {

    int h1 = [text sizeWithFont:_textView.font].height;
    int h2 = [text sizeWithFont:_textView.font constrainedToSize:CGSizeMake(_textView.frame.size.width - 16, 170.0f) lineBreakMode:UILineBreakModeWordWrap].height;

    __block int delta;
    [UIView animateWithDuration:(animated ? .1f : 0) animations:^
     {
         int h = h2 == h1 ? _inputHeightWithShadow : h2 + 24;
         delta = h - self.frame.size.height;
         CGRect r2 = CGRectMake(0, self.frame.origin.y - delta, self.frame.size.width, h);
         self.frame = r2; //CGRectMake(0, self.frame.origin.y - delta, self.superview.frame.size.width, h);
         _inputBackgroundView.frame = CGRectMake(0, 0, self.frame.size.width, h);

         CGRect r = _textView.frame;
         r.origin.y = 12;
         r.size.height = h - 18;
         _textView.frame = r;
         if (_delegate != nil && [_delegate respondsToSelector:@selector(textViewDidChangeFrameSize:)]) {
             [_delegate performSelector:@selector(textViewDidChangeFrameSize:) withObject:[NSNumber numberWithInt:delta]];
         }


     } completion:^(BOOL finished)
     {

     }];
}

- (id) initWithFrame:(CGRect)frame {

    self = [super initWithFrame:frame];

    if (self)
    {
        _inputHeight = 38.0f;
        _inputHeightWithShadow = 44.0f;
        _autoResizeOnKeyboardVisibilityChanged = YES;

        [self composeView];
    }
    return self;
}

- (void) fitText {

    [self adjustTextInputHeightForText:_textView.text animated:YES];
}

- (void) setText:(NSString*)text {

    _textView.text = text;
    _lblPlaceholder.hidden = text.length > 0;
    [self fitText];
}


#pragma mark UITextFieldDelegate Delegate

- (void) textViewDidBeginEditing:(UITextView*)textView {

    if (_delegate != nil && [_delegate respondsToSelector:@selector(textViewDidBeginEditing:)])
        [_delegate performSelector:@selector(textViewDidBeginEditing:) withObject:textView];

    if (_autoResizeOnKeyboardVisibilityChanged)
    {
        [UIView animateWithDuration:.25f animations:^{
            CGRect r = self.frame;
            r.origin.y -= keyboardHeight - TABBAR_HEIGHT; // Should be heyboard height
            [self setFrame:r];
        }
        completion:^(BOOL finished) {
            if (finished) {
                if (_delegate != nil && [_delegate respondsToSelector:@selector(textViewDidEndSlindingUp:)])
                    [_delegate performSelector:@selector(textViewDidEndSlindingUp:) withObject:textView];
            }
        }];
        [self fitText];
    }
}

- (void) textViewDidEndEditing:(UITextView*)textView {

    if (_delegate != nil && [_delegate respondsToSelector:@selector(textViewDidEndEditing:)])
        [_delegate performSelector:@selector(textViewDidEndEditing:) withObject:textView];


    if (_autoResizeOnKeyboardVisibilityChanged)
    {
        [UIView animateWithDuration:.25f animations:^{
            CGRect r = self.frame;
            r.origin.y += keyboardHeight - TABBAR_HEIGHT; // Should be heyboard height
            [self setFrame:r];
        }];

        [self fitText];
    }
    _lblPlaceholder.hidden = _textView.text.length > 0;

}

- (BOOL) textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text {

    if ([text isEqualToString:@"\n"])
    {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(returnButtonPressed:)])
            [_delegate performSelector:@selector(returnButtonPressed:) withObject:_textView afterDelay:.1];
        return NO;
    }
    else if (text.length > 0)
    {
        [self adjustTextInputHeightForText:[NSString stringWithFormat:@"%@%@", _textView.text, text] animated:YES];
    }
    return YES;
}

- (void) textViewDidChange:(UITextView*)textView {

    _lblPlaceholder.hidden = _textView.text.length > 0;

    [self fitText];

    if (_delegate != nil && [_delegate respondsToSelector:@selector(textViewDidChange:)])
        [_delegate performSelector:@selector(textViewDidChange:) withObject:textView];
}


#pragma mark THChatInput Delegate

- (void) sendButtonPressed:(id)sender {

    if (_delegate != nil && [_delegate respondsToSelector:@selector(sendButtonPressed:)]) {
        NSDictionary *args = @{
                               @"sender": sender,
                               @"text": _textView.text
                               };
        [_delegate performSelector:@selector(sendButtonPressed:) withObject:args];
    }


}


@end
