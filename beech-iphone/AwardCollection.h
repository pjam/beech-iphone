//
//  AwardCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Award.h"

@interface AwardCollection : Collection
-(Award*) awardForId:(NSNumber*) award_id;
@end
