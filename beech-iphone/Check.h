//
//  Check.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "Beer.h"

@interface Check : MTLModel <MTLJSONSerializing>

@property(nonatomic) NSNumber *check_id;
@property(nonatomic) NSNumber *beer_id;
@property(nonatomic) NSNumber *user_id;
@property(nonatomic) NSNumber *created_at;
@property(nonatomic) Beer *beer;

- (BOOL) isEqualToCheck:(Check*) check;
@end
