//
//  TopBeerViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 20/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "TopBeerViewController.h"

@interface TopBeerViewController ()
{
    __weak IBOutlet UILabel *topBeechCount;
    __weak IBOutlet UILabel *topBeechName;
    __weak IBOutlet UILabel *topBeechIndex;
}
@end

@implementation TopBeerViewController

@synthesize beerCount, beerIndex, beerName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithBeerCount:(int)abeerCount beerName:(NSString*) abeerName beerIndex:(int) abeerIndex
{
    if (self = [super initWithNibName:@"TopBeerViewController" bundle:nil])
    {
        [self setBeerCount:abeerCount];
        [self setBeerIndex:abeerIndex];
        [self  setBeerName:abeerName];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    topBeechCount.text = [NSString stringWithFormat:@"%d", beerCount ];
    topBeechCount.font = [UIFont fontWithName:@"ProximaNova-Light" size:80.0 ];
    topBeechCount.textColor = [UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.2];
    
    topBeechName.text = [NSString stringWithFormat:@"%@", beerName ];
    topBeechName.font = [UIFont fontWithName:@"ProximaNova-Light" size:30.0];
    topBeechName.textColor = [UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:1.0];
    
    topBeechIndex.text = [NSString stringWithFormat:@"BEECH #%d",beerIndex];
    topBeechIndex.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:12.0];
    topBeechIndex.textColor = [UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
