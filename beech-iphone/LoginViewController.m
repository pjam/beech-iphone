    //
//  LoginViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 11/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import "LoginViewController.h"
#import "ASIFormDataRequest.h"
#import "RequestHelper.h"
#import "WelcomeViewController.h"
#import "ColorHelper.h"
#import "UIBarButtonItem+StyledButton.h"

@interface LoginViewController ()
{
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UIBarButtonItem *submitButton;
    __weak IBOutlet UIButton *forgottenButton;
}
@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    [emailTextField becomeFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [forgottenButton setTitle:NSLocalizedString(@"Forgot your password ?", @"") forState:UIControlStateNormal];
    [forgottenButton setTitleColor:[ColorHelper greyColor] forState:UIControlStateNormal];
    [forgottenButton setTitleColor:[ColorHelper greyColor] forState:UIControlStateHighlighted];
    [forgottenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [forgottenButton.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:14.0]];
    [forgottenButton.titleLabel setTextColor:[ColorHelper greyColor]];
    // TODO : Change that for next release
//    [forgottenButton setHidden:YES];

    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    self.navigationItem.title = NSLocalizedString(@"Signing in", @"");
    
    emailTextField.delegate = self;
    passwordTextField.delegate = self;
    [emailTextField setPlaceholder:NSLocalizedString(@"Nickname or email", @"")];
    [passwordTextField setPlaceholder:NSLocalizedString(@"Password", @"")];

    self.navigationItem.rightBarButtonItem = [UIBarButtonItem styledSubmitBarButtonItemWithTitle:NSLocalizedString(@"Done", @"") target:self selector:@selector(submitLogin:)];

//    [submitButton setEnabled:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *) getEmail
{
    return [emailTextField text];
}

- (NSString *) getPassword
{
    return [passwordTextField text];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [submitButton setEnabled:YES];    
    if ([request responseStatusCode] != 200 && [request responseStatusCode] != 201) {
        [self requestFailed:request];
        NSData *data = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        NSMutableString *messageContent = [NSMutableString stringWithCapacity:1000];
        NSDictionary *errors = [json objectForKey:@"errors"];
        for(id key in errors)
        {
            [messageContent appendString:[NSString stringWithFormat:@"%@ : ", key]];
            [messageContent appendString:[NSString stringWithFormat:@"%@", [((NSArray*)[errors objectForKey:key]) componentsJoinedByString:@"," ]]];
            [messageContent appendString:@"\n"];
        }
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errors !", @"")
                                                          message:messageContent
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }
    else {
        // authentication successful, store credentials
        NSData *responseData = [request responseData];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
        NSString *auth_token = [[json objectForKey:@"user"] objectForKey:@"authentication_token"];
        NSNumber *userId = [[json objectForKey:@"user"] objectForKey:@"id"];
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:[self getEmail] forKey:@"username"];
        [defaults setValue:[self getPassword] forKey:@"password"];
        [defaults setValue:auth_token forKey:@"authentication_token"];
        [defaults setValue:userId forKey:@"user_id"];

//        [self performSegueWithIdentifier:@"LoginToWelcomeSegue" sender:self];
        [self dismissModalViewControllerAnimated:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"login-success" object:nil];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"failed with error: %d", [request responseStatusCode]);
    // tell user incorrect username/password
    NSData *data = [request responseData];
    if (data != nil) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        NSMutableString *messageContent = [NSString stringWithFormat:@"%@ : %@", NSLocalizedString(@"Error", @""), [json objectForKey:@"error"]];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errors !", @"")
                                                          message:messageContent
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        [submitButton setEnabled:YES];

    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // TODO
}

- (void)viewDidUnload
{
    emailTextField = nil;
    passwordTextField = nil;
    submitButton = nil;
    forgottenButton = nil;
    [super viewDidUnload];
}

- (IBAction)submitLogin:(id)sender
{
    if ([[self getEmail] length] > 0 && [[self getPassword] length] > 0) {
        NSURL *login_url = [RequestHelper createURL:@"/users/sign_in" withPrefix:@""];
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:login_url];
        [request setDelegate:self];
        [request addPostValue:[self getEmail] forKey:@"user[login]"];
        [request addPostValue:[self getPassword] forKey:@"user[password]"];
        [request addRequestHeader:@"Accept" value:@"application/json"];
        [request addRequestHeader:@"Content-Type" value:@"application/json"];
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if ([language isEqualToString:@"fr"]) {
            language = @"fr";
        } else
        {
            language = @"en";
        }
        [request addRequestHeader:@"Accept-Language" value:language];
        [request setRequestMethod:@"POST"];
        [request startAsynchronous];
        [submitButton setEnabled:NO];
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"LoginToWelcomeSegue"]){
        
        WelcomeViewController *vc = [segue destinationViewController];
        [vc setDelegate:self];
    }
}
- (IBAction)forgottenButtonTapped:(id)sender {
}

@end
