//
//  BadgeCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "BadgeCollection.h"

@implementation BadgeCollection
- (Badge *)badgeForId:(NSNumber *)badge_id
{
    NSUInteger badgeIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id badge, NSUInteger idx,BOOL *stop)
     {
         return [((Badge*)badge).badge_id intValue] == [badge_id intValue];
     }];
    return [self.collection objectAtIndex:badgeIndex];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Badge *newBadge = [MTLJSONAdapter modelOfClass:Badge.class fromJSONDictionary:item error:nil];
        [self addObject:newBadge];
    }
}

- (BOOL)addObject:(id)anObject
{
    NSUInteger eventIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id badge, NSUInteger idx,BOOL *stop)
     {
         return [((Badge*)badge).badge_id intValue] == [((Badge*)anObject).badge_id intValue];
     }];

    if (eventIndex == NSNotFound) {
        NSUInteger newIndex = [self.collection indexOfObject:anObject
                                               inSortedRange:(NSRange){0, [self.collection count]}
                                                     options:NSBinarySearchingInsertionIndex
                                             usingComparator:^NSComparisonResult(id a, id b) {
                                                 NSNumber *first = [(Badge*)a badge_id];
                                                 NSNumber *second = [(Badge*)b badge_id];
                                                 return [second compare:first];
                                             }];
        [self.collection insertObject:anObject atIndex:newIndex];
        return YES;
    } else {
        return NO;
    }
}
@end
