//
//  FollowButton.m
//  beech-iphone
//
//  Created by Pierre Jambet on 26/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "FollowButton.h"

@implementation FollowButton
{
    UIImageView *iconView;
}
@synthesize offsetX, offsetY;

- (id) initWithOffsetX:(int)offset
{
//- (id)initWithFrame:(CGRect)frame
//{
    self = [super init];
    if (self) {
        [self setOffsetX:offset];
        NSString *followButtonLabel = @"Follow";
        [self.titleLabel setFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:14.0]];
        [self setTitle:followButtonLabel forState:UIControlStateNormal];
        self.titleLabel.adjustsFontSizeToFitWidth = NO;
        [self.titleLabel setTextColor:[UIColor blackColor]];
        [self setTitleColor:[UIColor colorWithRed:.80/255.0 green:.70/255.0 blue:.65/255.0 alpha:1] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        CGSize stringsize = [followButtonLabel sizeWithFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:14]];
        int width = 40;
        [self setFrame:CGRectMake(308 - stringsize.width - [self offsetX] - width, 12.0, stringsize.width - width, 33.0)];
        iconView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 18, 18)];
        iconView.image = [UIImage imageNamed:@"user-button-follow-icon"];
        [self addSubview:iconView];
        
        //
        //make the buttons content appear in the top-left
//        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
//        [button setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
        
        //move text 10 pixels down and right
        [self setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 20.0f, 0.0f, 0.0f)];

        [self setBackgroundImage:[[UIImage imageNamed:@"user-button-follow"]resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 2, 5)]forState:UIControlStateNormal];
        [self setBackgroundImage:[[UIImage imageNamed:@"user-button-unfollow"]resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 2, 5)]forState:UIControlStateSelected];
        
        [self setTitle:NSLocalizedString(@"Follow", @"") forState:UIControlStateNormal];
        [self setTitle:NSLocalizedString(@"Unfollow", @"") forState:UIControlStateSelected];

        [self setSelected:NO];
    }
    return self;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (selected)
    {
        iconView.image = [UIImage imageNamed:@"user-button-unfollow-icon"];
        [self.titleLabel setText:[self titleForState:UIControlStateSelected]];
    } else
    {
        iconView.image = [UIImage imageNamed:@"user-button-follow-icon"];
        [self.titleLabel setText:[self titleForState:UIControlStateNormal]];
    }
    CGSize stringsize = [[[self titleLabel] text] sizeWithFont:[UIFont fontWithName:@"ProximaNova-Semibold" size:14]];
    int width = 40;
    [self setFrame:CGRectMake(308 - stringsize.width - [self offsetX] - width, 12.0, stringsize.width + width, 33.0)];
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, 20.0f, 0.0f, 0.0f)];
}

@end
