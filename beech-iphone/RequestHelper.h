//
//  RequestHelper.h
//  beech-iphone
//
//  Created by Pierre Jambet on 27/12/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface RequestHelper : NSObject
+ (NSURL *) createURL:(NSString *)path withPrefix:(NSString *) prefix;
+ (ASIHTTPRequest *) prepareRequestWithUrl:(NSURL *) url withParams:(NSDictionary*) queryArgs;
+ (ASIFormDataRequest *) prepareFormRequestWithUrl:(NSURL *) url withParams:(NSDictionary*) queryArgs;
@end
