//
//  User.h
//  beech-iphone
//
//  Created by Pierre Jambet on 23/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "BeechModel.h"

@interface User : BeechModel

@property(nonatomic) NSString* nickname;
@property(nonatomic) NSString* email;
@property(nonatomic) UIImage* avatar;
@property(nonatomic) NSNumber *already_following;
@property(nonatomic) NSNumber *user_id;
@property(nonatomic) NSNumber *created_at;
@property(nonatomic) NSDictionary *avatar_url;

- (BOOL) isEqualToUser:(User*) user;
- (NSString*) thumbUrl;
@end
