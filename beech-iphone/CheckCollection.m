//
//  CheckCollection.m
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "CheckCollection.h"

@implementation CheckCollection
- (Check*)checkForId:(NSNumber *)check_id
{
    NSUInteger checkIndex =
    [self.collection indexOfObjectPassingTest:^BOOL(id check, NSUInteger idx,BOOL *stop)
     {
         return [((Check*)check).check_id intValue] == [check_id intValue];
     }];
    return [self.collection objectAtIndex:checkIndex];
}

- (void)addItems:(NSArray *)items
{
    for (id item in items) {
        Check *newCheck = [MTLJSONAdapter modelOfClass:Check.class fromJSONDictionary:item error:nil];
        
        NSUInteger checkIndex =
        [self.collection indexOfObjectPassingTest:^BOOL(id check, NSUInteger idx,BOOL *stop)
         {
             return [((Check*)check).check_id intValue] == [((Check*)newCheck).check_id intValue];
         }];
        if (checkIndex == NSNotFound) {
            [self.collection addObject:newCheck];
        }
    }
}
@end
