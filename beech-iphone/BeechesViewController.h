//
//  BeechesViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeechesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
{
    BOOL pageControlUsed;
}
@property(nonatomic) BOOL forceRefresh;
@property (nonatomic, retain) NSMutableArray *viewControllers;
@end
