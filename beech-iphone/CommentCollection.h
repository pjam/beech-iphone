//
//  CommentCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Comment.h"

@interface CommentCollection : Collection
-(Comment*) commentForId:(NSNumber*) comment_id;
-(NSMutableArray*) commentsForEventId:(NSNumber*) event_id;
@end
