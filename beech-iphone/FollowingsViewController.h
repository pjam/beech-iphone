//
//  SecondViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 10/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
