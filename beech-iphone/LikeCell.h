//
//  LikeCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeCell : UITableViewCell
@property (nonatomic, retain) UIImageView *avatar;
@property (nonatomic, retain) UILabel *name;
@property (nonatomic, retain) UILabel *desc;
@property (nonatomic, retain) UILabel *time;
@end
