//
//  SettingsViewController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 09/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "User.h"

@interface SettingsViewController : UIViewController<UINavigationControllerDelegate, MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

- (IBAction)openMail:(id)sender;
@property (nonatomic) User *user;
@end
