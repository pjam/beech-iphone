//
//  BeechNavigationController.h
//  beech-iphone
//
//  Created by Pierre Jambet on 24/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeechNavigationController : UINavigationController

@end
