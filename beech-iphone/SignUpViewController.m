//
//  SignUpViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 19/01/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SignUpViewController.h"
#import "ASIFormDataRequest.h"
#import "RequestHelper.h"
#import "WelcomeViewController.h"
#import "UIBarButtonItem+StyledButton.h"

@interface SignUpViewController ()
{
    __weak IBOutlet UIBarButtonItem *submitButton;
    __weak IBOutlet UINavigationItem *_navigationTitle;
    __weak IBOutlet UITextField *emailTextField;
    __weak IBOutlet UITextField *passwordTextField;
    __weak IBOutlet UITextField *nicknameTextField;
}
@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }

    self.navigationItem.title = NSLocalizedString(@"Signing up", @"");
    
    emailTextField.delegate = self;
    passwordTextField.delegate = self;
    
    [emailTextField setPlaceholder:NSLocalizedString(@"Email", @"")];
    [nicknameTextField setPlaceholder:NSLocalizedString(@"Nickname", @"")];
    [passwordTextField setPlaceholder:NSLocalizedString(@"Password", @"")];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem styledSubmitBarButtonItemWithTitle:NSLocalizedString(@"Done", @"") target:self selector:@selector(submitButtonTapped:)];


}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    [emailTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *) getEmail
{
    return [emailTextField text];
}

- (NSString *) getPassword
{
    return [passwordTextField text];
}

- (NSString *) getNickname
{
    return [nicknameTextField text];
}

-(IBAction)submitButtonTapped:(id)sender
{
    NSURL *login_url = [RequestHelper createURL:@"/users" withPrefix:@""];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:login_url];
    [request setDelegate:self];
    [request addPostValue:[self getEmail] forKey:@"user[email]"];
    [request addPostValue:[self getPassword] forKey:@"user[password]"];
    [request addPostValue:[self getNickname] forKey:@"user[nickname]"];
    [request addRequestHeader:@"Accept" value:@"application/json"];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if ([language isEqualToString:@"fr"]) {
        language = @"fr";
    } else
    {
        language = @"en";
    }
    [request addRequestHeader:@"Accept-Language" value:language];
    [request setRequestMethod:@"POST"];
    
    [request setCompletionBlock:^{
        [submitButton setEnabled:YES];        
        // Use when fetching binary data
        NSData *data = [request responseData];
        if ([request responseStatusCode] != 200 && [request responseStatusCode] != 201) {
            NSError *error = [request error];
            NSLog(@"success with error : %@", error);
            NSLog(@"%@", data);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSMutableString *messageContent = [NSMutableString stringWithCapacity:1000];
            NSDictionary *errors = [json objectForKey:@"errors"];
            for(id key in errors)
            {
                [messageContent appendString:[NSString stringWithFormat:@"%@ : ", key]];
                [messageContent appendString:[NSString stringWithFormat:@"%@", [((NSArray*)[errors objectForKey:key]) componentsJoinedByString:@"," ]]];
                [messageContent appendString:@"\n"];
            }
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Errors !", @"")
                                                              message:messageContent
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
        }
        else {
            // authentication successful, store credentials
            NSData *responseData = [request responseData];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
            NSString *auth_token = [[json objectForKey:@"user"] objectForKey:@"authentication_token"];
            NSNumber *userId = [[json objectForKey:@"user"] objectForKey:@"id"];
            NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:[self getEmail] forKey:@"username"];
            [defaults setValue:[self getPassword] forKey:@"password"];
            [defaults setValue:auth_token forKey:@"auth_token"];
            [defaults setValue:userId forKey:@"user_id"];
//            [self dismissModalViewControllerAnimated:NO];
            [self performSegueWithIdentifier:@"SignUpToWelcomeSegue" sender:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"login-success" object:nil];
        };

    }];
    [request setFailedBlock:^{
        [submitButton setEnabled:YES];
        NSError *error = [request error];
        NSLog(@"failed with error : %@", error);
        NSData *data = [request responseData];
        if (data) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSLog(@"json : %@", json);
        }
    }];
    
    [request startAsynchronous];
    [submitButton setEnabled:NO];
}

- (void)viewDidUnload {
    submitButton = nil;
    _navigationTitle = nil;
    emailTextField = nil;
    passwordTextField = nil;
    nicknameTextField = nil;
    [super viewDidUnload];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"SignUpToWelcomeSegue"]){
        
        WelcomeViewController *vc = [segue destinationViewController];
        [vc setDelegate:self];
    }
}
@end
