//
//  SlideItemViewController.m
//  beech-iphone
//
//  Created by Pierre Jambet on 09/02/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "SlideItemViewController.h"

@interface SlideItemViewController ()
{
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *contentLabel;
}
@end

@implementation SlideItemViewController

@synthesize stringContent, stringTitle, bottomText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) initWithTitle:(NSString *)ptitle andContent:(NSString *)pcontent
{
    if (self = [super initWithNibName:@"SlideItemViewController" bundle:nil])
    {
        [self setStringTitle:ptitle];
        [self setStringContent:pcontent];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    contentLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16.0];
    contentLabel.textColor = [UIColor colorWithRed:83/255.0 green:83/255.0 blue:83/255.0 alpha:1.0];
    contentLabel.text = stringContent;
    contentLabel.textAlignment = UITextAlignmentCenter;
    [contentLabel sizeToFit];
    
    titleLabel.font = [UIFont fontWithName:@"ProximaNova-Semibold" size:20.0];
    titleLabel.textColor = [UIColor colorWithRed:19/255.0 green:19/255.0 blue:19/255.0 alpha:1.0];
    titleLabel.text = stringTitle;
    titleLabel.textAlignment = UITextAlignmentCenter;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTitle:nil];
    contentLabel = nil;
    titleLabel = nil;
    [self setSlideImage:nil];
    [self setBottomText:nil];
    [super viewDidUnload];
}
@end
