//
//  Like.m
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Like.h"

@implementation Like

@synthesize like_id, event, user, event_id, user_id;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"like_id": @"id",
             @"event_id": @"event_id",
             @"user_id": @"user_id",
             @"created_at": @"created_at",
             };
}

@end
