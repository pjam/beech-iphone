//
//  FeedCell.h
//  beech-iphone
//
//  Created by Pierre Jambet on 18/11/12.
//  Copyright (c) 2012 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeechCell.h"


@class FeedCell;

@interface FeedCell : BeechCell
@property(nonatomic) UILabel *beerDetails;
- (void) setDetailsWithBeer:(Beer*)beer;
@end
