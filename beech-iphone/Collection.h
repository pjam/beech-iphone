//
//  Collection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SqliteDB.h"

@interface Collection : NSObject
@property(nonatomic) NSMutableArray *collection;

-(id) initWithCollection:(NSMutableArray*) col;
-(NSUInteger) count;
-(id) lastObject;
-(id) objectAtIndex:(NSUInteger)index;
-(BOOL) addObject:(id)anObject;
-(void) insertObject:(id)anObject atIndex:(NSUInteger)index;
-(void) removeAllObjects;
-(void) addItems:(NSArray*)items;
- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])buffer count:(NSUInteger)len;
- (NSArray *)sortedArrayUsingDescriptors:(NSArray *)sortDescriptors;
@end
