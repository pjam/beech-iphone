//
//  Like.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/13/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "User.h"
#import "Event.h"

@interface Like : MTLModel <MTLJSONSerializing>
@property(nonatomic) NSNumber *like_id;
@property(nonatomic) NSNumber *user_id;
@property(nonatomic) NSNumber *event_id;
@property(nonatomic) NSNumber *created_at;
@property (nonatomic) User *user;
@property (nonatomic) Event *event;
@end
