//
//  CheckCollection.h
//  beech-iphone
//
//  Created by Pierre Jambet on 30/03/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import "Collection.h"
#import "Check.h"

@interface CheckCollection : Collection
-(Check*) checkForId:(NSNumber*) check_id;
@end
