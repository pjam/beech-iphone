//
//  MessageView.h
//  beech-iphone
//
//  Created by Pierre Jambet on 4/14/13.
//  Copyright (c) 2013 Pierre Jambet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatMessageView : UIView <UITextViewDelegate> {

}
@property (assign) IBOutlet id delegate;

@property (assign) int inputHeight;
@property (assign) int inputHeightWithShadow;
@property (assign) BOOL autoResizeOnKeyboardVisibilityChanged;

@property (strong, nonatomic) UIButton* sendButton;
@property (strong, nonatomic) UIView* sendingComment;
@property (strong, nonatomic) UITextView* textView;
@property (strong, nonatomic) UILabel* lblPlaceholder;
@property (strong, nonatomic) UIImageView* inputBackgroundView;
@property int keyboardHeight;

- (void) fitText;

- (void) setText:(NSString*)text;

@end
